# Testing

Build container with testing tools:

```bash
docker build -t testing_jaco_bnk - < tests/Containerfile

docker run --network host --rm \
  --volume `pwd`/:/Benchmark-Jaco/ \
  -it testing_jaco_bnk
```

For syntax tests, check out the steps in the [gitlab-ci](../.gitlab-ci.yml#L68) file.
