import argparse
import copy
import json
import os
import sys
import time

import alexa_client
import hyper
import requests
import tqdm

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
rhino_bm_path = "/speech-to-intent-benchmark/"
bm_data_path = rhino_bm_path + "data/speech/"

snr_dbs = [6, 9, 12, 15, 18, 21, 24]
noises = ["cafe", "kitchen"]

with open(file_path + "alexa-credentials.json", encoding="utf-8") as cfile:
    credentials = json.load(cfile)

dialog_request_id = None
alexa = None
avs_language = "en-US"

labels = None
reduced_dataset = 0


# ==================================================================================================


def connect_alexa():
    """Create and connect a new alexa client"""

    client = alexa_client.AlexaClient(
        client_id=credentials["client_id"],
        secret=credentials["secret"],
        refresh_token=credentials["refresh_token"],
        language=avs_language,
    )
    client.connect()
    print(" Established connection to Alexa")
    return client


# ==================================================================================================


def start_new_dialog():
    """Start dialog by sending an audio file saying 'Alexa, start benchmark experiment'"""

    request_id = alexa_client.alexa_client.helpers.generate_unique_id()
    with open(file_path + "alexa_launch_en.wav", "rb") as f:
        alexa.send_audio_file(f, dialog_request_id=dialog_request_id)
    return request_id


# ==================================================================================================


def seconds_to_hours(secs):
    secs = int(secs)
    m, s = divmod(secs, 60)
    h, m = divmod(m, 60)
    t = "{:d}:{:02d}:{:02d}".format(h, m, s)
    return t


# ==================================================================================================


def get_intent(audio_path):
    """Send audio file to Alexa and extract the intent from the returned SimpleCard
    If Alexa doesn't respond, the request is send two times again"""

    response = None
    intent = None

    # Wait some time that the requests are not sent too fast
    time.sleep(1)

    for _ in range(3):
        with open(audio_path, "rb") as f:
            response = alexa.send_audio_file(f, dialog_request_id=dialog_request_id)

        if response is not None:
            for res in response:
                if res.name == "RenderTemplate":
                    if (
                        "payload" in res.directive
                        and "textField" in res.directive["payload"]
                    ):
                        intent = res.directive["payload"]["textField"]
            break

        # Wait some time before sending the unanswered request again
        time.sleep(3)

    if response is None:
        print(" No response for file:", audio_path)
        return None

    if intent is None:
        print(" No intent for file:", audio_path)
        return None

    try:
        intent = json.loads(intent)
        return intent
    except json.decoder.JSONDecodeError:
        print(" Invalid json string:", intent, audio_path)
        return None


# ==================================================================================================


def convert_intent(intent):
    """Convert intent to rhino format"""

    if intent is None or "slots" not in intent:
        return None

    rh_intent = copy.deepcopy(intent)

    # Remove empty slots
    for s in intent["slots"]:
        if intent["slots"][s] is None:
            del rh_intent["slots"][s]

    # Convert weights
    if "size" in intent["slots"]:
        if intent["slots"]["size"] == "8 oz":
            rh_intent["slots"]["size"] = "eight ounce"
        elif intent["slots"]["size"] == "12 oz":
            rh_intent["slots"]["size"] = "twelve ounce"
        elif intent["slots"]["size"] == "16 oz":
            rh_intent["slots"]["size"] = "sixteen ounce"
        elif intent["slots"]["size"] == "20 oz":
            rh_intent["slots"]["size"] = "twenty ounce"

    return rh_intent


# ==================================================================================================


def compare_intents(intent, label):
    if intent is None:
        return False, "intent missing"

    if label["intent"].lower() != intent["intent"].lower():
        return False, "intent wrong"

    intent_slots = [s.lower() for s in intent["slots"].keys()]
    for slot in label["slots"].keys():
        if slot.lower() not in intent_slots:
            return False, "slot missing: {}".format(slot)

        detection_value = intent["slots"][slot].strip().lower()
        label_value = label["slots"][slot].strip().lower()
        if detection_value != label_value:
            return False, "value wrong: {} - {}".format(detection_value, label_value)

    return True, ""


# ==================================================================================================


def process_dir(data_path):
    global dialog_request_id, alexa

    files = sorted(os.listdir(data_path))
    if 0 < reduced_dataset <= len(files):
        files = files[0:reduced_dataset]

    results = []
    for file_name in tqdm.tqdm(files):
        try:
            intent = get_intent(data_path + file_name)
            if intent is None:
                # If no response was returned, restart the dialog, because Alexa seems to drop it
                # This may be the same case when an invalid json file is returned
                dialog_request_id = start_new_dialog()

        except (
            requests.exceptions.HTTPError,
            hyper.http20.exceptions.StreamResetError,
            hyper.http20.exceptions.ConnectionError,
            ConnectionResetError,
        ):
            # The connection was lost, try to create a new client
            # This may result in another connection error in the avs-client thread, ignore this one
            alexa = connect_alexa()
            intent = get_intent(data_path + file_name)

        rh_intent = convert_intent(intent)
        label = labels[file_name]

        same, error_reason = compare_intents(rh_intent, label)
        results.append(same)

        if not same:
            # Print wrong detections for debugging
            print("\n")
            print("ERROR", error_reason)
            print(file_name)
            print("Detected intent:", rh_intent)
            print(label)

        if len(results) % 20 == 0:
            # Start new dialog every 20 requests,
            # attempt to fix empty intent responses after some time
            _ = start_new_dialog()
            dialog_request_id = start_new_dialog()

        if len(results) % 50 == 0:
            status = " Numbers of correct files: {}/{}"
            print(status.format(sum((1 for r in results if r)), len(results)))

    return results


# ==================================================================================================


def main():
    global alexa, dialog_request_id, labels, reduced_dataset

    parser = argparse.ArgumentParser(description="Run benchmark with Alexa")
    parser.add_argument(
        "--reduced_dataset",
        default=0,
        type=int,
        help="Test with fewer files in each step",
    )
    args = parser.parse_args()

    with open(rhino_bm_path + "data/label/label.json", encoding="utf-8") as file:
        labels = json.load(file)

    reduced_dataset = args.reduced_dataset
    accuracies = {}
    alexa = connect_alexa()
    dialog_request_id = start_new_dialog()

    # Test clean speech
    print("\nProcessing the clean audio directory ...")
    data_path = bm_data_path + "clean/"
    start_time = time.time()
    results = process_dir(data_path)
    conv_dur = time.time() - start_time

    # Count wrong and correct instead of using list length for debugging purposes
    # (allows to end the loop midways)
    num_correct = sum((1 for r in results if r))
    acc = num_correct / len(results)
    accuracies["clean"] = round(acc, 4)

    print("\n====================")
    print("The clean audio benchmark took {}".format(seconds_to_hours(conv_dur)))
    msg = "Correctly converted {}/{}. The accuracy is {:.4f}"
    print(msg.format(num_correct, len(results), acc))
    print("====================\n")

    for noise_type in noises:
        print("\nProcessing the {} noise audio directory ...".format(noise_type))
        accs = []
        for sd in tqdm.tqdm(snr_dbs):
            data_path = bm_data_path + "{}_{}db/".format(noise_type, sd)
            results = process_dir(data_path)
            num_correct = sum((1 for r in results if r))
            acc = num_correct / len(results)
            accs.append(round(acc, 4))

            print("\n====================")
            msg = "Correctly converted {}/{} in {} with {} snr. The accuracy is {:.4f}"
            print(msg.format(num_correct, len(results), noise_type, sd, acc))
            print("====================\n")
        accuracies[noise_type] = accs

    msg = "\nBenchmark completed after {}, with results:"
    print(msg.format(seconds_to_hours(time.time() - start_time)))
    print(accuracies)

    # There is still some thread running, maybe from avs-client library
    sys.exit()


# ==================================================================================================

if __name__ == "__main__":
    main()
