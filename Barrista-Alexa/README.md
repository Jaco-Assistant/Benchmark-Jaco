# Benchmark Barrista Alexa

Tools to benchmark [Alexa](https://developer.amazon.com/en-US/alexa), a widely used voice assistant from Amazon. \
NOT WORKING ANYMORE

<br>

### Test Alexa

Steps to rebuild everything:

- Generate the SLU dataset for Jaco

- Convert the intents:

  ```bash
  sed 's/- //g' Barrista-Jaco/dialog/nlu432/en/nlu.md > Barrista-Alexa/tmp1.txt
  sed -r 's/\[[a-z A-Z]*\]\(([a-zA-Z_]*)\.txt\)/{\1}/g' Barrista-Alexa/tmp1.txt > Barrista-Alexa/tmp2.txt
  ```

- Change the localization of your _Amazon_ account:

  - Set your address to somewhere in the USA
  - Change your shop to _"amazon.com"_ by following this [link](https://www.amazon.com/gp/help/customer/display.html?nodeId=201248840). \
    (there was a text _"You are currently linked to shop on Amazon.de"_ (it was no link) below, click on it, then will show another link where you can transfer to the new region)

- Create a new skill for Alexa: [Tutorial](https://developer.amazon.com/en-US/docs/alexa/custom-skills/create-custom-skill-from-quick-start-template.html) \
  Select the `custom` model, `python` skill hosting and `start from scratch` template.

- Create intents and slot types using bulk edit option, then match slots to slot types. Save and build the skill.

- Copy the exported lambda function script into your skill and deploy it.

- Test the skill with textual inputs in the console \
  (don't forget to activate the skill with its invocation name)

- Build the container:

  ```bash
  docker build --progress=plain -t barrista_alexa - < Barrista-Alexa/Containerfile
  ```

- Create a new virtual Alexa device: \
  (Following `aws-client` setup [here](https://github.com/richtier/alexa-voice-service-client#authentication))

  - Follow the link to AWS product page (https://developer.amazon.com/alexa/console/avs/products), optionally click _get-started_, add a new product
  - Fill forms and setup security profile. Activate display cards with text (Product-Details -> Capabilities). Click Update.

  - Copy `alexa-credentials.template.json` to `alexa-credentials.json` and fill in your credentials
  - Get `client-id` and `secret` from the product's detail page

  - Get the `request_token`. Use your product-id (Product-Details -> Information) for the `device-type-id` flag.

    ```bash
    docker run --network host --rm \
      -it barrista_alexa

    python3 -m alexa_client.refreshtoken.serve \
      --device-type-id="enter-device-type-id-here" \
      --client-id="enter-client-id-here" \
      --client-secret="enter-client-secret-here"

    # Open the printed link to register this device
    ```

- Create a launch sentence for Alexa (you can use Jaco's _text-to-speech_ module for this),
  then convert it to 16kHz `alexa_launch_en.wav`

- You can view the transcripts and returned cards at: https://alexa.amazon.de/spa/index.html#cards \
   History of speech inputs: https://alexa.amazon.de/spa/index.html#cards -> Settings -> History -> Manage Your Content and Devices -> Privacy Settings -> Alexa -> Review Voice History

- Run the benchmark:

  ```bash
  docker run --network host --rm --device /dev/snd --env ALSA_CARD=PCH \
    --volume `pwd`/Barrista-Alexa/:/Benchmark-Jaco/Barrista-Alexa/ \
    --volume `pwd`/datasets/:/Benchmark-Jaco/datasets/ \
    -it barrista_alexa

  python3 /Benchmark-Jaco/Barrista-Alexa/run_benchmark.py --reduced_dataset 3
  ```

Clean dataset accuracy: 0.8837 \
Benchmark Runtime: 11:33h

_Developer's Note: It took me seven days, one phone call and three support requests to find out
how to change the language of my virtual alexa device from German to English.
In the end I had to extend the `aws-client` library to send an api-request to Alexa.
With Jaco this is much easier, and you can change everything you like because it's open source:)_
