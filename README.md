# Benchmarks

[![pipeline status](https://gitlab.com/Jaco-Assistant/Benchmark-Jaco/badges/master/pipeline.svg)](https://gitlab.com/Jaco-Assistant/Benchmark-Jaco/-/commits/master)
[![code style black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![code complexity](https://gitlab.com/Jaco-Assistant/Benchmark-Jaco/-/jobs/artifacts/master/raw/badges/rcc.svg?job=analysis)](https://gitlab.com/Jaco-Assistant/Benchmark-Jaco/-/commits/master)

<br/>

## Benchmark Barrista

The benchmark from [Picovoice Rhino](https://github.com/Picovoice/speech-to-intent-benchmark) is used to compare the performance with some commercial solutions.
The benchmark consists of 620 commands of different people ordering coffee in english. The audio (~1:29h) is mixed with
different volume levels of background noise from cafe and kitchen environments.

<div align="center">
    <img src="media/compare_barrista_432.png" alt="compare barrista dataset" width="75%"/>
</div>

<br/>

## Benchmark SmartLights and SmartSpeaker

_SmartLights_ and _SmartSpeakers_ are SLU benchmark dataset from [Snips](https://snips.ai/).
The corresponding paper can be found [here](https://arxiv.org/pdf/1810.12735.pdf). \
_SmartLights_ allows to control lights in different rooms.
_SmartSpeakers_ allows to control a smart speaker through playback control but also to play music from large libraries of artists, tracks and album.

<div align="center">
    <img src="media/compare_smart_lights.png" alt="compare smart lights dataset" width="35%"/>
    <img src="media/compare_smart_speaker_en.png" alt="compare smart speaker dataset english" width="30%"/>
    <img src="media/compare_smart_speaker_fr.png" alt="compare smart speaker dataset french" width="30%"/>
</div>

<br/>

## Benchmark FluentSpeechCommands

This benchmark is about predicting simple voice assistant commands.
The corresponding paper can be found [here](https://arxiv.org/pdf/1904.03670.pdf). \
It can be seen that finetuning the acoustic model of _Scribosermo_ to the dataset (+AMT) can further improve the results.

<div align="center">
    <img src="media/compare_fluent_speech_commands.png" alt="compare fluent speech commands" width="65%"/>
</div>

<br/>

## More tests and Benchmarks

#### TimersAndSuch

The benchmark tests common use-cases involving numbers.
The corresponding paper can be found [here](https://arxiv.org/pdf/2104.01604.pdf).

|              | Accuracy |
| ------------ | -------- |
| TaS-baseline | 0.816    |
| SpeechBrain  | 0.940    |
| Finstreder   | 0.954    |
| Slungt       | 0.925    |

#### SLURP

SLURP is a large SLU dataset which includes multiple different domains.
The corresponding paper can be found [here](https://arxiv.org/pdf/2011.13205.pdf). \
The benchmarks shows limitations of _finstreder_ when using very large datasets.

|             | ScenAct-F1 | Entity-F1 | SLU-F1 |
| ----------- | ---------- | --------- | ------ |
| Multi-SLURP | 0.783      | 0.642     | 0.708  |
| Finstreder  | 0.541      | 0.399     | 0.460  |
| Slungt      | 0.547      | 0.394     | 0.460  |

#### Natural Language Understanding

The tests with textual inputs follow the setup for full SLU parsing, they are not optimized for the NLU task, so performance should get better with some modifications.

|            | SmartLights:&nbsp;Accuracy | SmartSpeaker:&nbsp;Accuracy |
| ---------- | -------------------------- | --------------------------- |
| Jaco       | 0.960                      | 0.977                       |
| Finstreder | 0.909                      | 0.991                       |
| Slungt     | 0.902                      | 0.998                       |

#### Token to Text Decoding

The tests with textual outputs follow the setup for full SLU parsing, they are not optimized for the STT task, so performance should get better with some modifications.

|              | SmartLights:&nbsp;WER | SmartLights:&nbsp;RTF |
| ------------ | --------------------- | --------------------- |
| dsctcdecoder | 0.054                 | 0.002                 |
| pyctcdecode  | 0.072                 | 0.001                 |
| slungt       | 0.052                 | 0.002                 |
