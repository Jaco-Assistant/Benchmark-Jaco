import matplotlib.pyplot as plt
import numpy as np

# ==================================================================================================

alexa = 0.987
fsc_baseline = 0.988
cao_etal = 0.990
fans = 0.990
reptile = 0.992
slungt = 0.992
saxon_etal = 0.994
atat = 0.995
finstreder_cf = 0.995
borgholt_etal = 0.996
seo_etal = 0.997
qian_etal = 0.997
kim_etal = 0.997
slungt_amt = 0.997
universlu = 0.998
finstreder_cfcb_amt = 0.998


# ==================================================================================================


width = 0.7
labels = [
    "Alexa",
    "FSC-baseline",
    "Cao et al.",
    "FANS",
    "Reptile",
    "Slungt",
    "Saxon et al.",
    "AT-AT",
    "Finstreder",
    "Borgholt et al.",
    "Seo et al.",
    "Qian et al.",
    "Kim et al.",
    "Slungt + AMT",
    "UniverSLU",
    "Finstreder + AMT",
]

fig, ax = plt.subplots(figsize=(5.5, 4))
plt.title("Benchmark FluentSpeechCommands Dataset\n")
plt.bar(1, alexa, width=width, color="darkorange", label="")
plt.bar(2, fsc_baseline, width=width, color="gray", label="")
plt.bar(3, cao_etal, width=width, color="darkred", label="")
plt.bar(4, fans, width=width, color="moccasin", label="")
plt.bar(5, reptile, width=width, color="magenta", label="")
plt.bar(6, slungt, width=width, color="turquoise", label="")
plt.bar(7, saxon_etal, width=width, color="lightpink", label="")
plt.bar(8, atat, width=width, color="brown", label="")
plt.bar(9, finstreder_cf, width=width, color="lightgreen", label="")
plt.bar(10, borgholt_etal, width=width, color="hotpink", label="")
plt.bar(11, seo_etal, width=width, color="khaki", label="")
plt.bar(12, qian_etal, width=width, color="palevioletred", label="")
plt.bar(13, kim_etal, width=width, color="tan", label="")
plt.bar(14, slungt_amt, width=width, color="turquoise", label="")
plt.bar(15, universlu, width=width, color="darkkhaki", label="")
plt.bar(16, finstreder_cfcb_amt, width=width, color="lightgreen", label="")

plt.ylabel("Accuracy perfect classification")
plt.xticks(range(1, len(labels) + 1), labels, rotation=45, horizontalalignment="right")
plt.yticks(np.arange(0.98, 1.00, 0.005))
plt.ylim(0.98, 1.0)
plt.tight_layout()
plt.savefig("/Benchmark-Jaco/media/compare_fluent_speech_commands.png", dpi=300)
plt.clf()
