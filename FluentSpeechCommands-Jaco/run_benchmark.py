import argparse
import json
import os
import sys
import time

import numpy as np
import pandas as pd
import soundfile as sf
import tensorflow as tf
import tqdm

sys.path.insert(1, "/slungt/swig/")
import slungt  # noqa: E402 pylint: disable=wrong-import-position

sys.path.insert(1, "/slungt/tests/")
import test_interface  # noqa: E402 pylint: disable=wrong-import-position

# ==================================================================================================

filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
data_dir = filepath + "../datasets/fluent_speech_commands/"
label_csv = data_dir + "data/test_data.csv"

stt_model_dir = filepath + "../sttmodels/en-conformerctc-large-cb4/"
# stt_model_dir = filepath + "../sttmodels/en-conformerctc-large-new/"
# stt_model_dir = filepath + "../sttmodels/en-conformerctc-large-fsc/"
tf_model: tf.keras.Model

alphabet_path = stt_model_dir + "alphabet.json"
with open(alphabet_path, "r", encoding="utf-8") as afile:
    alphabet = json.load(afile)

datapath = filepath + "moduldata/sludata/"
vocabs, intents, lms = test_interface.load_data(datapath)

slu_decoder = slungt.Decoder(
    list(alphabet),
    256,
    vocabs,
    intents,
    lms,
    lm_alpha=1.5,
    token_min_logprob=-10.0,
)


# ==================================================================================================


def seconds_to_hours(secs):
    secs = int(secs)
    m, s = divmod(secs, 60)
    h, m = divmod(m, 60)
    t = "{:02d}:{:02d}:{:02d}".format(h, m, s)
    return t


# ==================================================================================================


def get_duration(filename):
    """Get duration of the wav file"""
    length = sf.info(filename).duration
    return length


# ==================================================================================================


def load_labels(path: str) -> list:
    dataset = pd.read_csv(path, sep=",", keep_default_na=False)
    dataset = dataset[["path", "transcription", "action", "object", "location"]]
    dataset = dataset.rename(columns={"transcription": "text"})
    dataset = dataset.to_dict(orient="records")
    for entry in dataset:
        entry["path"] = data_dir + entry["path"]
    return dataset


# ==================================================================================================


def load_audio(wav_path):
    audio, _ = sf.read(wav_path, dtype="int16")
    audio = audio / (np.iinfo(np.int16).max + 1)
    audio = np.expand_dims(audio, axis=0)
    audio = audio.astype(np.float32)
    return audio


# ==================================================================================================


def predict_signal_tf(audio):
    global tf_model

    output_data = tf_model.predict(audio, steps=1)
    output_data = output_data[0]
    return output_data


# ==================================================================================================


def get_intent_from_audio(audio_path):
    """Get intent from audio file"""

    audio = load_audio(audio_path)
    prediction = predict_signal_tf(audio)
    probs = prediction.tolist()

    results = slu_decoder.decode(probs)
    result = results[0]
    slungt_intent = slu_decoder.extract_text2intent(result)
    intent = json.loads(str(slungt_intent))

    intent["result"] = str(result)
    intent["greedy"] = slu_decoder.decode_greedy_text(probs)

    return intent


# ==================================================================================================


def convert_intent(intent):
    """Convert intent format"""

    if intent is None or intent["name"] is None:
        return None

    convint = {
        "intent": intent["name"].replace("fsc-", ""),
        "entities": {},
        "text": intent["text"],
        "greedy": intent["greedy"],
        "result": intent["result"],
    }

    if "entities" in intent:
        for e in intent["entities"]:
            entity = e["name"].replace("fsc-", "")
            convint["entities"][entity] = e["value"]

    return convint


# ==================================================================================================


def compare_intents(intent, label):
    intentname = intent["intent"]
    obj, loc = "none", "none"

    if "_" in intentname:
        if intentname in ["bring", "change_language"]:
            action = intentname.replace("_", " ")
        else:
            action, obj = intentname.split("_")
    else:
        action = intentname

    if action == "bring" and "object" in intent["entities"]:
        obj = intent["entities"]["object"]
    elif action == "change language" and "language" in intent["entities"]:
        obj = intent["entities"]["language"]

    if "location" in intent["entities"]:
        loc = intent["entities"]["location"]

    if action != label["action"].lower():
        return False, "action wrong"

    if obj != label["object"].lower():
        return False, "object wrong"

    if loc != label["location"].lower():
        return False, "location wrong"

    return True, ""


# ==================================================================================================


def run_benchmark(labels: dict):
    print("\nRunning benchmark ...")
    start_time = time.time()

    results = []
    for label in tqdm.tqdm(labels):
        intent = get_intent_from_audio(label["path"])
        convint = convert_intent(intent)
        same, error_reason = compare_intents(convint, label)
        results.append(same)

        if not same:
            # Print wrong predictions for debugging
            print("\n")
            print("ERROR", error_reason)
            print("Detected intent:", intent)
            print("Converted intent:", convint)
            print("Label:", label)

    conv_dur = time.time() - start_time
    msg = " The benchmark took {}"
    print(msg.format(seconds_to_hours(conv_dur)))

    # Count wrong and correct instead of using list length for debugging purposes
    # (allows to end the loop midways)
    num_correct = sum((1 for r in results if r))
    acc = num_correct / len(results)
    msg = " Correctly converted {}/{}. The accuracy is {:.4f}"
    print(msg.format(num_correct, len(results), acc))


# ==================================================================================================


def main():
    global tf_model

    parser = argparse.ArgumentParser(description="Run benchmark with Jaco")
    parser.add_argument(
        "--reduced_dataset",
        default=0,
        type=int,
        help="Test with fewer files",
    )
    args = parser.parse_args()

    # Load stt model and run some warmup predictions
    stime = time.time()
    tf_model = tf.keras.models.load_model(stt_model_dir + "pb/")
    tf_model.predict(np.random.rand(1, 16000).astype(np.float32))
    tf_model.predict(np.random.rand(1, 48000).astype(np.float32))
    tf_model.predict(np.random.rand(1, 32000).astype(np.float32))
    tf_model.summary()
    print("Time to load stt model:", time.time() - stime)

    # Load labels
    labels = load_labels(label_csv)
    if 0 < args.reduced_dataset <= len(labels):
        # Reduce dataset size if flag given
        labels = labels[0 : args.reduced_dataset]

    # Get duration of sample files, extra because this needs some time too
    audio_dur = 0
    wav_files = [lb["path"] for lb in labels]
    for file_name in tqdm.tqdm(wav_files):
        audio_dur += get_duration(os.path.join(file_name))
    msg = "The benchmark contains {} files with {} hours of audio"
    print(msg.format(len(wav_files), seconds_to_hours(audio_dur)))

    # Now run the benchmark
    run_benchmark(labels)


# ==================================================================================================

if __name__ == "__main__":
    main()
