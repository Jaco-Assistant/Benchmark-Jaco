import json
import os

import pandas as pd

# ==================================================================================================


filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
skillpath = filepath + "skills/fsc/dialog/nlu/en/"
datapath = filepath + "../datasets/fluent_speech_commands/"

path_input_train = datapath + "data/train_data.csv"
path_input_dev = datapath + "data/valid_data.csv"
path_nlu = skillpath + "nlu.md"

# ==================================================================================================


def main():
    if not os.path.isdir(skillpath):
        os.makedirs(skillpath)

    # Load dataset
    data_train = pd.read_csv(path_input_train, sep=",", keep_default_na=False)
    data_dev = pd.read_csv(path_input_dev, sep=",", keep_default_na=False)
    dataset = pd.concat([data_train, data_dev], axis=0, join="inner")
    dataset = dataset[["transcription", "action", "object", "location"]]
    dataset = dataset.rename(columns={"transcription": "text"})
    dataset = dataset.to_dict(orient="records")

    for entry in dataset:
        entry["text"] = entry["text"].lower()
        entry["action"] = entry["action"].lower()
        entry["object"] = entry["object"].lower()
        entry["location"] = entry["location"].lower()

    new_ds = {"intents": {}, "lookups": {}}
    for entry in dataset:
        slottype = ""

        if entry["location"] != "none":
            intent = "{}_{}".format(entry["action"], entry["object"])
            slottype = "location"
            slotval = entry["location"]

            if slotval == "washroom" and "bathroom" in entry["text"]:
                # Handle synonym
                slotval = "bathroom"

            slottag = "[{}]({})".format(slotval, slottype)
            sample = entry["text"].replace(slotval, slottag)

        elif entry["action"] == "change language":
            intent = entry["action"]
            slottype = "language"
            slotval = entry["object"]
            if slotval != "none":
                slottag = "[{}]({})".format(slotval, slottype)
                sample = entry["text"].replace(slotval, slottag)
            else:
                slottype = ""
                sample = entry["text"]

        elif entry["action"] == "bring":
            intent = entry["action"]
            slottype = "object"
            slotval = entry["object"]
            slottag = "[{}]({})".format(slotval, slottype)
            sample = entry["text"].replace(slotval, slottag)

        else:
            intent = "{}_{}".format(entry["action"], entry["object"])
            sample = entry["text"]

        if slottype != "":
            if slotval in ["bathroom", "washroom"]:
                # Handle synonym
                slotval = "(bathroom|washroom)->washroom"

            if slottype not in new_ds["lookups"]:
                new_ds["lookups"][slottype] = [slotval]
            else:
                new_ds["lookups"][slottype].append(slotval)

        # Some formatting
        sample = sample.replace(".", " ")
        sample = " ".join(sample.split())
        intent = intent.replace(" ", "_")

        if intent not in new_ds["intents"]:
            new_ds["intents"][intent] = [sample]
        else:
            new_ds["intents"][intent].append(sample)

    # Write nlu file
    dataset = new_ds
    slu_md = ""
    for intent in dataset["intents"]:
        slu_md += "\n## intent:{}\n".format(intent)
        for sample in set(dataset["intents"][intent]):
            slu_md += "- {}\n".format(sample)
    with open(path_nlu, "w+", encoding="utf-8") as file:
        file.write(slu_md)

    # Write lookups
    for lookup in dataset["lookups"]:
        with open(skillpath + "{}.txt".format(lookup), "w+", encoding="utf-8") as file:
            file.write("\n".join(set(dataset["lookups"][lookup])) + "\n")

    # Write topic keys file
    topkeys = {}
    for intent in dataset["intents"]:
        topname = "".join([i.title() for i in intent.split("_")])
        topkeys["Jaco/Intents/Fsc/" + topname] = ""
    path = filepath + "userdata/skill_topic_keys.json"
    with open(path, "w+", encoding="utf-8") as file:
        file.write(json.dumps(topkeys, indent=2) + "\n")


# ==================================================================================================

if __name__ == "__main__":
    main()
