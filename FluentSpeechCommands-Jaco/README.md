# Benchmark FluentSpeechCommands Jaco

FluentSpeechCommands tests simple voice assistant requests.
The corresponding paper can be found [here](https://arxiv.org/pdf/1904.03670.pdf).

<br>

### Test Jaco

Steps to rebuild everything:

- Download dataset from [here](https://groups.google.com/a/fluent.ai/g/fluent-speech-commands), requires login with your Google account.

- Extract dataset to `datasets/fluent_speech_commands/`.

- Build container:

  ```bash
  docker build --progress=plain -t fluentspeechcommands_jaco - < FluentSpeechCommands-Jaco/Containerfile
  ```

- Build number text files and convert intents

  ```bash
  docker run --rm \
    --volume `pwd`/FluentSpeechCommands-Jaco/:/Benchmark-Jaco/FluentSpeechCommands-Jaco/ \
    --volume `pwd`/datasets/:/Benchmark-Jaco/datasets/ \
    -it fluentspeechcommands_jaco \
      python3 /Benchmark-Jaco/FluentSpeechCommands-Jaco/convert_intents.py
  ```

- Set language in Jaco-Master's `global_config` file to english

- Generate dialog dataset:

  ```bash
  # Run from parent directory
  docker run --network host --rm \
    --volume `pwd`/Jaco-Master/skills/collect_data.py:/Jaco-Master/skills/collect_data.py:ro \
    --volume `pwd`/Jaco-Master/skills/generate_data.py:/Jaco-Master/skills/generate_data.py:ro \
    --volume `pwd`/Jaco-Master/skills/map_intents_slots.py:/Jaco-Master/skills/map_intents_slots.py:ro \
    --volume `pwd`/Jaco-Master/skills/sentence_tools.py:/Jaco-Master/skills/sentence_tools.py:ro \
    --volume `pwd`/Jaco-Master/skills/text_tools.py:/Jaco-Master/skills/text_tools.py:ro \
    --volume `pwd`/Jaco-Master/skills/basic_normalizer.py:/Jaco-Master/skills/basic_normalizer.py:ro \
    --volume `pwd`/Jaco-Master/slu-parser/moduldata/langdicts.json:/Jaco-Master/slu-parser/moduldata/langdicts.json:ro \
    --volume `pwd`/Jaco-Master/jacolib/:/jacolib/:ro \
    --volume `pwd`/Benchmark-Jaco/FluentSpeechCommands-Jaco/skills/:/Jaco-Master/skills/skills/:ro \
    --volume `pwd`/Jaco-Master/userdata/config/:/Jaco-Master/userdata/config/:ro \
    --volume `pwd`/Benchmark-Jaco/FluentSpeechCommands-Jaco/userdata/skill_topic_keys.json:/Jaco-Master/userdata/skill_topic_keys.json:ro \
    --volume `pwd`/Benchmark-Jaco/FluentSpeechCommands-Jaco/moduldata/collected/:/Jaco-Master/skills/collected/ \
    --volume `pwd`/Benchmark-Jaco/FluentSpeechCommands-Jaco/moduldata/generated/:/Jaco-Master/skills/generated/ \
    -it master_base_image_amd64 /bin/bash -c ' \
      python3 /Jaco-Master/skills/collect_data.py && \
      python3 /Jaco-Master/skills/map_intents_slots.py && \
      python3 /Jaco-Master/skills/generate_data.py'
  ```

- Create language model:

  ```bash
  docker run --network host --rm \
    --volume `pwd`/FluentSpeechCommands-Jaco/:/Benchmark-Jaco/FluentSpeechCommands-Jaco/ \
    --volume `pwd`/../slungt/:/slungt/ \
    --volume `pwd`/sttmodels/:/Benchmark-Jaco/sttmodels/ \
    -it fluentspeechcommands_jaco \
      python3 /slungt/preprocess/fullbuild_slu.py \
        --workdir /Benchmark-Jaco/FluentSpeechCommands-Jaco/moduldata/sludata/ \
        --nlufile_path /Benchmark-Jaco/FluentSpeechCommands-Jaco/moduldata/generated/nlu/nlu.json \
        --alphabet_path /Benchmark-Jaco/sttmodels/en-conformerctc-large-cb4/alphabet.json
  ```

- Run benchmark:

  ```bash
  docker run --rm --network host \
    --volume `pwd`/FluentSpeechCommands-Jaco/:/Benchmark-Jaco/FluentSpeechCommands-Jaco/ \
    --volume `pwd`/../slungt/:/slungt/ \
    --volume `pwd`/datasets/:/Benchmark-Jaco/datasets/ \
    --volume `pwd`/sttmodels/:/Benchmark-Jaco/sttmodels/ \
    --volume `pwd`/media/:/Benchmark-Jaco/media/ \
    -it fluentspeechcommands_jaco

  # Build slungt
  cd /slungt/swig/; make all; cd /

  # Run the benchmark
  python3 /Benchmark-Jaco/FluentSpeechCommands-Jaco/run_benchmark.py --reduced_dataset 300

  # Rebuild plot in media directory, update values with above outputs before
  python3 /Benchmark-Jaco/FluentSpeechCommands-Jaco/plot_comparison.py
  ```

Accuracy: 0.9921 \
Benchmark Runtime: 00:00:16 + 00:12:54

<br>

### Pretraining with Scribosermo

- Convert dataset format:

  ```bash
  sed 's/,/\t/g' datasets/fluent_speech_commands/data/train_data.csv > FluentSpeechCommands-Jaco/pretraining/tmp1.txt
  sed -e 's/wavs\//\/datasets\/fluent_speech_commands\/wavs\//g' FluentSpeechCommands-Jaco/pretraining/tmp1.txt > FluentSpeechCommands-Jaco/pretraining/tmp2.txt
  sed -e 's/^[0-9]*\t//g' FluentSpeechCommands-Jaco/pretraining/tmp2.txt > FluentSpeechCommands-Jaco/pretraining/tmp3.txt
  sed -e '1s/path\tspeakerId\ttranscription/filepath\tspeaker\ttext/g' FluentSpeechCommands-Jaco/pretraining/tmp3.txt > FluentSpeechCommands-Jaco/pretraining/train.csv
  cd FluentSpeechCommands-Jaco/pretraining/; rm tmp1.txt tmp2.txt tmp3.txt; cd ../../

  sed 's/,/\t/g' datasets/fluent_speech_commands/data/valid_data.csv > FluentSpeechCommands-Jaco/pretraining/tmp1.txt
  sed -e 's/wavs\//\/datasets\/fluent_speech_commands\/wavs\//g' FluentSpeechCommands-Jaco/pretraining/tmp1.txt > FluentSpeechCommands-Jaco/pretraining/tmp2.txt
  sed -e 's/^[0-9]*\t//g' FluentSpeechCommands-Jaco/pretraining/tmp2.txt > FluentSpeechCommands-Jaco/pretraining/tmp3.txt
  sed -e '1s/path\tspeakerId\ttranscription/filepath\tspeaker\ttext/g' FluentSpeechCommands-Jaco/pretraining/tmp3.txt > FluentSpeechCommands-Jaco/pretraining/eval.csv
  cd FluentSpeechCommands-Jaco/pretraining/; rm tmp1.txt tmp2.txt tmp3.txt; cd ../../
  ```

- Start Scribosermo's container:

  ```bash
  # Run from parent directory
  docker run --privileged --rm --network host -it \
  --gpus all --shm-size=1g --ulimit memlock=-1 --ulimit stack=67108864 \
  --volume "$(pwd)"/Scribosermo/:/Scribosermo/ \
  --volume "$(pwd)"/corcua/:/corcua/ \
  --volume "$(pwd)"/checkpoints/:/checkpoints/ \
  --volume "$(pwd)"/Benchmark-Jaco/datasets/:/datasets/ \
  --volume "$(pwd)"/Benchmark-Jaco/FluentSpeechCommands-Jaco/:/FluentSpeechCommands-Jaco/ \
  scribosermo
  ```

- Prepare dataset:

  ```bash
  python3 -c 'from corcua import readers, writers, stats; \
    ds = readers.base_reader.Reader().load_dataset({"path": "/FluentSpeechCommands-Jaco/pretraining/train.csv"}); \
    ds = stats.get_duration(ds); \
    writers.base_writer.Writer().save_dataset(ds, path="/FluentSpeechCommands-Jaco/pretraining/", sample_rate=0, overwrite=False);'
  cd /FluentSpeechCommands-Jaco/pretraining/ && mv all.csv train.csv && cd /

  python3 -c 'from corcua import readers, writers, stats; \
    ds = readers.base_reader.Reader().load_dataset({"path": "/FluentSpeechCommands-Jaco/pretraining/eval.csv"}); \
    ds = stats.get_duration(ds); \
    writers.base_writer.Writer().save_dataset(ds, path="/FluentSpeechCommands-Jaco/pretraining/", sample_rate=0, overwrite=False);'
  cd /FluentSpeechCommands-Jaco/pretraining/ && mv all.csv eval.csv && cd /

  export LANGUAGE="en"
  python3 /Scribosermo/preprocessing/dataset_operations.py "/FluentSpeechCommands-Jaco/pretraining/train.csv" \
    "/FluentSpeechCommands-Jaco/pretraining/train_azce.csv" --replace --exclude
  python3 /Scribosermo/preprocessing/dataset_operations.py "/FluentSpeechCommands-Jaco/pretraining/eval.csv" \
    "/FluentSpeechCommands-Jaco/pretraining/eval_azce.csv" --replace --exclude
  ```

- Update Scribosermo's config file, optionally activate more augmentation options, run training and export the model.

- Use the newly trained model to run the benchmark

Accuracy: 0.9974 \
Training + Benchmark Runtime (Gaming PC): 3:02h + 0:11h
