import argparse
import json
import os
import re
import sys
import time
import wave

import houndify
import num2words
import tqdm

import utils

sys.path.insert(1, "/spoken-language-understanding-research-datasets/")
from dataset_handler import (  # noqa: E402 pylint: disable=wrong-import-position
    CrossValDataset,
)

# ==================================================================================================

synonym_replacers = {
    "pink": "red",
    "lounge": "living room",
    "bed room": "bedroom",
    "toilet": "toilets",
    "loo": "toilets",
    "facilities": "toilets",
    "kids room": "kids bedroom",
    "cella": "cellar",
    "entire house": "house",
    "entire flat": "flat",
    "entire appartment": "appartment",
    "child's bedroom": "child bedroom",
    "children bedroom": "child bedroom",
    "children's bedroom": "child bedroom",
}

extra_synonym_replacers = {
    "toilets room": "toilets",
    "toilet room": "toilets",
    "cublicle": "cubicle",
}

intent_maps = {
    "SetLightBrightness": "SetBrightness",
    "SetLightColor": "SetColor",
    "SwitchLightOn": "TurnOn",
    "SwitchLightOff": "TurnOff",
    "IncreaseBrightness": "SetBrightnessDelta",
    "DecreaseBrightness": "SetBrightnessDelta",
}

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
data_dir = file_path + "../datasets/snips_slu/smart-lights-en-close-field/"

with open(file_path + "houndify-credentials.json", "r", encoding="utf-8") as file:
    credentials = json.load(file)

client = None
buffer_size = 256

rq_path = file_path + "request_info.json"
with open(rq_path, "r", encoding="utf-8") as file:
    request_info = json.load(file)

# ==================================================================================================


class MyListener(houndify.HoundListener):
    """Ignore results via callbacks"""

    def onPartialTranscript(self, transcript):
        pass

    def onFinalResponse(self, response):
        pass

    def onError(self, err):
        pass


# ==================================================================================================


def init_houndify(credents):
    """Initialize the houndify client that it can handle devices and groups"""

    init_clt = houndify.TextHoundClient(
        credents["client_id"], credents["client_key"], "test_user", request_info
    )
    response = init_clt.query("index_user_devices_from_request_info")
    print(
        "Init devices: ",
        response["AllResults"][0]["RequiredFeaturesSupportedResult"]["WrittenResponse"],
    )
    response = init_clt.query("index_user_groups_from_request_info")
    print(
        "Init groups: ",
        response["AllResults"][0]["RequiredFeaturesSupportedResult"]["WrittenResponse"],
    )


# ==================================================================================================


def connect_houndify(credents):
    """Create and connect a new houndify audio client"""

    clt = houndify.StreamingHoundClient(
        credents["client_id"], credents["client_key"], "test_user", request_info
    )
    clt.setLocation(48.333967, 10.898489)
    clt.setSampleRate(16000)
    clt.start(MyListener())
    return clt


# ==================================================================================================


def get_intent(audio_path):
    """Send audio file to Houndify and extract the intent from the response"""
    global client

    # Wait some time that the requests are not sent too fast
    time.sleep(1)

    audio = wave.open(audio_path)
    while True:
        samples = audio.readframes(buffer_size)
        if len(samples) == 0:
            break
        if client.fill(samples):
            break
    result = client.finish()

    intent = {
        "name": "",
        "text": "",
        "slots": {},
    }

    # Use try-catch blocks that we don't need to check existence for each key
    try:
        intent["name"] = result["AllResults"][0]["Operation"]
    except KeyError:
        pass

    try:
        r1 = result["AllResults"][0]["RequiredFeaturesSupportedResult"]
        r2 = r1["ConversationState"]["QueryEntities"]["HowMuch"][0]
        intent["slots"]["brightness"] = r2["Quantity"]
    except KeyError:
        pass
    try:
        intent["slots"]["brightness"] = result["AllResults"][0]["Brightness"]
    except KeyError:
        pass

    try:
        r1 = result["AllResults"][0]["DeviceSpecifier"]["Groups"][0]
        intent["slots"]["room"] = r1["Name"]
    except KeyError:
        pass

    try:
        r1 = result["AllResults"][0]["RequiredFeaturesSupportedResult"]
        r2 = r1["ConversationState"]["HomeAutomation"][0]["Color"]
        intent["slots"]["color"] = r2["Name"]
    except KeyError:
        pass
    try:
        intent["slots"]["color"] = result["AllResults"][0]["Color"]["Name"]
    except KeyError:
        pass

    try:
        intent["text"] = result["Disambiguation"]["ChoiceData"][0]["Transcription"]
    except KeyError:
        pass

    return intent, result


# ==================================================================================================


def compare_intents(intent, label):
    if intent is None or intent["name"] == "":
        return False, "intent missing"

    label_name = intent_maps[label["intent"]]
    if label_name.lower() != intent["name"].lower():
        return False, "intent wrong"

    for slot in label["slots"]:
        slot_name = slot["slot_name"].lower()
        if slot_name not in intent["slots"]:
            return False, "slot missing: {}".format(slot)

        label_value = slot["text"].strip()
        detection_value = intent["slots"][slot_name]

        # Number to text
        if slot_name == "brightness":
            detection_value = int(detection_value * 100)
            detection_value = num2words.num2words(detection_value)
            detection_value = detection_value.replace("-", " ")

        # Remove dashes (some numbers/artists in the labels have dashes, but most not)
        label_value = label_value.replace("-", " ")
        # Remove special sentence signs
        label_value = label_value.replace(".", "")
        label_value = label_value.replace("?", "")

        label_value = label_value.lower()
        detection_value = detection_value.lower().strip()

        # Replace synonyms, this is not handled correctly in the labels dataset
        for sr in synonym_replacers:
            if label_value == sr:
                label_value = synonym_replacers[sr]
            if detection_value == sr:
                detection_value = synonym_replacers[sr]
        # Replace wrong rooms that are not handled correctly in the labels dataset
        for sr in extra_synonym_replacers:
            if label_value == sr:
                label_value = extra_synonym_replacers[sr]
            if detection_value == sr:
                detection_value = extra_synonym_replacers[sr]

        if detection_value != label_value:
            return False, "value wrong: {} - {}".format(detection_value, label_value)

    return True, ""


# ==================================================================================================


def run_benchmark(dataset, wav_files):
    global client

    print("\nRunning benchmark ...")
    start_time = time.time()

    results = []
    for i, file_name in enumerate(tqdm.tqdm(wav_files)):
        # Reconnect for every file, else the clients always returns the first result
        client = connect_houndify(credentials)

        try:
            intent, result = get_intent(file_name)
        except houndify.houndify.HoundifySDKError:
            try:
                # Try request again if it failed
                time.sleep(3)
                client = connect_houndify(credentials)
                intent, result = get_intent(file_name)
            except houndify.houndify.HoundifySDKError:
                intent, result = None, None

        wav_name = os.path.basename(file_name)
        label = dataset.get_labels_from_wav(wav_name)
        same, error_reason = compare_intents(intent, label)

        label_words = re.sub(r"[^a-z ]", "", label["text"].lower()).split()
        text_words = re.sub(r"[^a-z ]", "", intent["text"].lower()).split()
        wer = utils.levenshtein(label_words, text_words) / len(label_words)

        results.append((same, wer))

        if not same:
            # Print wrong detections for debugging
            print("\n")
            print("ERROR", error_reason)
            print(file_name)
            print("Detected intent:", intent)
            print(label)
            print("WER:", wer)
            print(result)

        if len(results) % 50 == 0:
            status = " Numbers of correct and wrong files, step: {} {} {}"
            num_corr = sum((1 for r in results if r[0]))
            print(status.format(num_corr, len(results) - num_corr, i))

        # Wait some time that connections are not rebuilt too fast
        time.sleep(1)

    conv_dur = time.time() - start_time
    msg = " The benchmark took {}"
    print(msg.format(utils.seconds_to_hours(conv_dur)))

    # Count wrong and correct instead of using list length for debugging purposes
    # (allows to end the loop midways)
    num_correct = sum((1 for r in results if r[0]))
    acc = num_correct / len(results)
    avg_wer = sum((r[1] for r in results)) / len(results)
    msg = " Correctly converted {}/{}. The accuracy is {:.4f}, the average WER {:.4f}"
    print(msg.format(num_correct, len(results), acc, avg_wer))


# ==================================================================================================


def main():
    global client

    parser = argparse.ArgumentParser(description="Run benchmark with Jaco")
    parser.add_argument(
        "--wav_files_txt",
        required=True,
        help="Path to text file with list of wav files to use",
    )
    parser.add_argument(
        "--start_idx",
        required=True,
        type=int,
        help="Start with given file number",
    )
    parser.add_argument(
        "--num_samples",
        required=True,
        type=int,
        help="Benchmark given samples",
    )
    args = parser.parse_args()

    # Load dataset
    dataset = CrossValDataset.from_dir(data_dir)
    with open(args.wav_files_txt, "r", encoding="utf-8") as wfile:
        wav_files = wfile.readlines()
    wav_files = [w.strip() for w in wav_files if w.strip() != ""]
    wav_files = sorted(wav_files)

    for w in list(wav_files):
        wav_name = os.path.basename(w)
        label = dataset.get_labels_from_wav(wav_name)
        if label is None:
            print("WARNING: Dropping {} because it has no labels".format(wav_name))
            wav_files.remove(w)

    stop_idx = min(len(wav_files), args.start_idx + args.num_samples)
    wav_files = wav_files[args.start_idx : stop_idx]

    init_houndify(credentials)
    run_benchmark(dataset, wav_files)

    # There is still some thread running, maybe from houndify-client library
    sys.exit()


# ==================================================================================================

if __name__ == "__main__":
    main()
