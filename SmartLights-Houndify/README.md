# Benchmark Barrista Houndify

[Houndify](https://www.houndify.com/technology) is a speech to intent solution,
claiming to "deliver unprecedented speed and accuracy" without proofing it.

### Test Houndify

Steps to rebuild everything:

- Create an account and a new client for `Home Automation` platform

- Add `Home Automation Commands` domain

- Build the container:

  ```bash
  podman build -f SmartLights-Houndify/Containerfile -t smartlights_houndify
  ```

- Export rooms with `python3 SmartLights-Houndify/convert_rooms.py` and save them into `request_info.json` file.

- Insert your credentials into `houndify-credentials.template.json` and save as `houndify-credentials.json`.

- Run the benchmark: \
  (Note that you can only run about 70 request per day and account using the free tier)

  ```bash
  podman run --network host --rm --device /dev/snd --env ALSA_CARD=PCH \
    --volume `pwd`/SmartLights-Houndify/:/Benchmark-Jaco/SmartLights-Houndify/ \
    --volume `pwd`/SmartLightsAndSpeaker-Jaco/:/Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/ \
    --volume `pwd`/datasets/:/Benchmark-Jaco/datasets/ \
    -it smartlights_houndify

  export NLU_DIR=nlu_slc_1
  python3 /Benchmark-Jaco/SmartLights-Houndify/run_benchmark.py \
    --wav_files_txt /Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/dialog/${NLU_DIR}/test_wavs.txt \
    --start_idx 0 --num_samples 70
  ```

Accuracy 5-fold SmartLights-Close dataset: 0.5452 - WER: 0.1082 \
Runtime for 70 requests: ~5min, but this has to repeated for about 25 times.
