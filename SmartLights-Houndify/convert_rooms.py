import json

# ==================================================================================================

rooms = [
    "basement",
    "office",
    "bathroom",
    "pantry",
    "vestibule",
    "studio",
    "cubicle",
    "dining room",
    "changing room",
    "laundry room",
    "waiting room",
    "kitchen",
    "living room",
    "bedroom",
    "entrance",
    "backyard",
    "toilets",
    "shed",
    "kids bedroom",
    "garage",
    "parking",
    "garden",
    "patio",
    "cellar",
    "everywhere",
    "boardroom",
    "large meeting room",
    "small meeting room",
    "meeting room",
    "house",
    "flat",
    "appartment",
    "child bedroom",
    "room",
    "lounge",
    "bed room",
    "toilet",
    "loo",
    "facilities",
    "kids room",
    "cella",
    "entire house",
    "entire flat",
    "entire appartment",
    "child's bedroom",
    "children bedroom",
    "children's bedroom",
]

base_entry = {
    "HomeAutomationSolution": "Client",
    "Name": "",
    "ID": "",
}


# ==================================================================================================


entries = []
for i, room in enumerate(rooms):
    entry = dict(base_entry)
    entry["Name"] = room
    entry["ID"] = str(i)
    entries.append(entry)
print(json.dumps(entries, indent=2))
