# Benchmark Slurp Alexa

Tools to benchmark [Alexa](https://developer.amazon.com/en-US/alexa), a widely used voice assistant from Amazon. \
Running the benchmark with Alexa doesn't work, because it has too many intents or too many slots in the intents.

<br>

### Test Alexa

Steps to rebuild everything:

- Generate the SLU dataset for Jaco

- Convert the intents:

  ```bash
  python3 ./Slurp-Alexa/convert2alexa.py
  ```

- Create a new skill for Alexa: [Tutorial](https://developer.amazon.com/en-US/docs/alexa/custom-skills/create-custom-skill-from-quick-start-template.html) \
  Select the `custom` model, `python` skill hosting and `start from scratch` template.

- Copy the files from the exported directory into Alexa's skill console \
  (at `build->interaction model->json editor`)

- Building the skill will raise an error: \
  `The total number of slots and intents is 511, which exceeds the limit of 350. Error code: ExceededMaximumIntentAndSlot`
