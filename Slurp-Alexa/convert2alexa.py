import copy
import json
import os
import re

# ==================================================================================================


filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
slu_file_path = filepath + "../Slurp-Jaco/moduldata/generated/nlu/nlu.json"
outpath = filepath + "skill-export/slurp.json"

slotpattern = r"\[[^\(]*\]\(([^\)]*)\)"
alexadata = {
    "interactionModel": {
        "languageModel": {
            "invocationName": "benchmark experiment",
            "intents": [
                {"name": "AMAZON.CancelIntent", "samples": []},
                {"name": "AMAZON.HelpIntent", "samples": []},
                {"name": "AMAZON.StopIntent", "samples": []},
                {"name": "AMAZON.NavigateHomeIntent", "samples": []},
                {"name": "AMAZON.FallbackIntent", "samples": []},
            ],
            "types": [],
        }
    }
}
alexaintent = {
    "name": "",
    "slots": [],
    "samples": [],
}
alexatype = {
    "name": "",
    "values": [],
}

# ==================================================================================================


def convert_intents():
    with open(slu_file_path, "r", encoding="utf-8") as file:
        nludata = json.load(file)

    # Intents
    for intent in nludata["intents"]:
        aint = copy.deepcopy(alexaintent)
        aint["name"] = intent

        allslots = set()
        for sample in nludata["intents"][intent]:
            slots = re.findall(slotpattern, sample)
            allslots = allslots.union(set(slots))
            sample = re.sub(slotpattern, r"{\1}", sample)
            aint["samples"].append(sample)

        for slot in sorted(allslots):
            aint["slots"].append({"name": slot, "type": slot})

        alexadata["interactionModel"]["languageModel"]["intents"].append(aint)

    # Lookups
    for lookup in nludata["lookups"]:
        alook = copy.deepcopy(alexatype)
        alook["name"] = lookup

        for sample in nludata["lookups"][lookup]:
            sample = {"name": {"value": sample}}
            alook["values"].append(sample)

        alexadata["interactionModel"]["languageModel"]["types"].append(alook)

    alexastr = json.dumps(alexadata, indent=2)
    alexastr = alexastr.replace("-", "_x_")
    with open(outpath, "w+", encoding="utf-8") as file:
        file.write(alexastr)


# ==================================================================================================


def main():
    if not os.path.isdir(os.path.dirname(outpath)):
        os.makedirs(os.path.dirname(outpath))

    convert_intents()


# ==================================================================================================

if __name__ == "__main__":
    main()
