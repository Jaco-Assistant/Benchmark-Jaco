import matplotlib.pyplot as plt
import numpy as np

# ==================================================================================================

# Picovoice Rhino 432 (From Picovoice Repository)
pv_cafe = np.array([0.91, 0.96, 0.98, 0.98, 0.98, 0.99, 0.99])
pv_kitchen = np.array([0.96, 0.97, 0.98, 0.99, 0.99, 0.99, 0.99])
pv432 = (pv_cafe + pv_kitchen) / 2.0

# Google Dialogflow 432 (From Picovoice Repository)
df_cafe = np.array([0.63, 0.70, 0.76, 0.79, 0.82, 0.83, 0.82])
df_kitchen = np.array([0.70, 0.78, 0.78, 0.80, 0.81, 0.82, 0.83])
df432 = (df_cafe + df_kitchen) / 2.0

# Jaco 432 {'clean': 0.979}
ja_cafe432 = np.array([0.546, 0.7722, 0.8611, 0.9095, 0.9467, 0.9532, 0.979])
ja_kitchen432 = np.array([0.664, 0.8352, 0.8901, 0.9338, 0.9612, 0.9693, 0.9822])
ja432 = (ja_cafe432 + ja_kitchen432) / 2.0

# Finstreder (Quartznet) 432 {'clean': 0.9596}
fin_cafe432 = np.array([0.5945, 0.7496, 0.8595, 0.9095, 0.9402, 0.9515, 0.958])
fin_kitchen432 = np.array([0.7254, 0.8352, 0.9095, 0.9192, 0.9499, 0.9532, 0.9661])
fin_qn_432 = (fin_cafe432 + fin_kitchen432) / 2.0

# Finstreder (Conformer sentencepiece) 432 {'clean': 0.9758, 'duration': '2:03:53'}
fin_cafe432 = np.array([0.7868, 0.8659, 0.9338, 0.9532, 0.9661, 0.9709, 0.9806])
fin_kitchen432 = np.array([0.8401, 0.9095, 0.9564, 0.9661, 0.9661, 0.9725, 0.9742])
fin_cf_432 = (fin_cafe432 + fin_kitchen432) / 2.0

# Finstreder (Conformer sentencepiece) + SSR 432 {'clean': 0.9952}
fin_cafe432 = np.array([0.8191, 0.8950, 0.9548, 0.9677, 0.9806, 0.9871, 0.9935])
fin_kitchen432 = np.array([0.8659, 0.9354, 0.9758, 0.9790, 0.9790, 0.9903, 0.9887])
fin_cfs_432 = (fin_cafe432 + fin_kitchen432) / 2.0

# Finstreder (Conformer charbased) {'clean': 0.9952, 'duration': '4:37:38'}
fin_cafe432 = np.array([0.7496, 0.8498, 0.9257, 0.9418, 0.979, 0.9758, 0.9855])
fin_kitchen432 = np.array([0.8142, 0.8869, 0.9499, 0.9742, 0.9709, 0.9887, 0.9806])
fin_cfcb_432 = (fin_cafe432 + fin_kitchen432) / 2.0

# Slungt (Conformer charbased) {'clean': 0.9935, 'duration': '03:03:44'}
slg_cafe432 = np.array([0.7706, 0.8627, 0.9338, 0.958, 0.9758, 0.9806, 0.9871])
slg_kitchen432 = np.array([0.8304, 0.9208, 0.9515, 0.9806, 0.9758, 0.9887, 0.9919])
slg_cfcb_432 = (slg_cafe432 + slg_kitchen432) / 2.0

# Slungt (Conformer sentencepiece) {'clean': 0.9822, 'duration': '07:13:00'}
slg_cafe432 = np.array([0.8384, 0.9111, 0.9532, 0.9693, 0.9677, 0.9725, 0.9822])
slg_kitchen432 = np.array([0.8691, 0.9386, 0.9515, 0.9742, 0.9806, 0.9774, 0.9822])
slg_cfsp_432 = (slg_cafe432 + slg_kitchen432) / 2.0

# Amazon Alexa 432 {'clean': 0.8837}
ax_cafe = np.array([0.7011, 0.811, 0.853, 0.8724, 0.8724, 0.8756, 0.8805])
ax_kitchen = np.array([0.7706, 0.8481, 0.8627, 0.874, 0.8772, 0.8756, 0.8821])
ax432 = (ax_cafe + ax_kitchen) / 2.0

# Amazon Lex 432 (From Picovoice Repository)
aws_cafe = np.array([0.71, 0.82, 0.84, 0.87, 0.87, 0.87, 0.87])
aws_kitchen = np.array([0.80, 0.84, 0.85, 0.87, 0.87, 0.87, 0.88])
aws432 = (aws_cafe + aws_kitchen) / 2.0

# Ibm Watson 432 (From Picovoice Repository)
ibm_cafe = np.array([0.50, 0.76, 0.88, 0.92, 0.94, 0.95, 0.96])
ibm_kitchen = np.array([0.72, 0.84, 0.89, 0.93, 0.95, 0.95, 0.96])
ibm432 = (ibm_cafe + ibm_kitchen) / 2.0

# Microsoft Luis 432 (From Picovoice Repository)
luis_cafe = np.array([0.83, 0.85, 0.89, 0.90, 0.91, 0.93, 0.93])
luis_kitchen = np.array([0.86, 0.89, 0.90, 0.91, 0.92, 0.93, 0.93])
luis432 = (luis_cafe + luis_kitchen) / 2.0

# ==================================================================================================

snr = [6, 9, 12, 15, 18, 21, 24]

plt.plot(snr, df432, color="red", marker="X", linewidth=0.9, label="Google Dialogflow")
plt.plot(snr, pv432, color="blue", marker="o", linewidth=0.9, label="Picovoice Rhino")
plt.plot(
    snr, ax432, color="darkorange", marker="^", linewidth=0.9, label="Amazon Alexa"
)
plt.plot(snr, ibm432, color="black", marker="*", linewidth=0.9, label="Ibm Watson")
plt.plot(
    snr, luis432, color="magenta", marker="d", linewidth=0.9, label="Microsoft Luis"
)
plt.plot(snr, ja432, color="darkgreen", marker="p", linewidth=0.9, label="Jaco")
plt.plot(
    snr,
    fin_cf_432,
    color="lightgreen",
    marker="p",
    linewidth=0.9,
    linestyle="dashdot",
    label="Finstreder (sp)",
)
plt.plot(
    snr,
    fin_cfcb_432,
    color="lightgreen",
    marker="p",
    linewidth=0.9,
    label="Finstreder (cb)",
)
plt.plot(
    snr,
    slg_cfsp_432,
    color="turquoise",
    marker="p",
    linewidth=0.9,
    linestyle="dashdot",
    label="Slungt (sp)",
)
plt.plot(
    snr,
    slg_cfcb_432,
    color="turquoise",
    marker="p",
    linewidth=0.9,
    label="Slungt (cb)",
)
plt.ylim(0.59, 1.01)

plt.xlim(5.5, 24.5)
plt.title("Benchmark Barista Dataset English\n")
plt.xlabel("Signal to Noise ratio")
plt.ylabel("Accuracy perfect Intent and Entity extraction")
plt.xticks([6, 9, 12, 15, 18, 21, 24])
plt.legend()
plt.grid()

plt.savefig("/Benchmark-Jaco/media/compare_barrista_432.png", dpi=300)
