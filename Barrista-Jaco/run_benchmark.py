import argparse
import json
import os
import sys
import time

import numpy as np
import soundfile as sf
import tensorflow as tf
import tqdm

sys.path.insert(1, "/slungt/swig/")
import slungt  # noqa: E402 pylint: disable=wrong-import-position

sys.path.insert(1, "/slungt/tests/")
import test_interface  # noqa: E402 pylint: disable=wrong-import-position

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
rhino_bm_path = "/speech-to-intent-benchmark/"
bm_data_path = rhino_bm_path + "data/speech/"

snr_dbs = [6, 9, 12, 15, 18, 21, 24]
noises = ["cafe", "kitchen"]

reduced_dataset = 0
labels = None

stt_model_dir = file_path + "../sttmodels/en-conformerctc-large-cb4/"
# stt_model_dir = file_path + "../sttmodels/en-conformerctc-large-new/"
tf_model: tf.keras.Model

alphabet_path = stt_model_dir + "alphabet.json"
with open(alphabet_path, "r", encoding="utf-8") as file:
    alphabet = json.load(file)

datapath = file_path + "moduldata/sludata/"
vocabs, intents, lms = test_interface.load_data(datapath)

slu_decoder = slungt.Decoder(
    list(alphabet),
    256,
    vocabs,
    intents,
    lms,
    lm_alpha=1.5,
    token_min_logprob=-15.0,
    token_max_count=32,
)


# ==================================================================================================


def get_duration(filename):
    """Get duration of the wav file"""
    length = sf.info(filename).duration
    return length


# ==================================================================================================


def seconds_to_hours(secs):
    secs = int(secs)
    m, s = divmod(secs, 60)
    h, m = divmod(m, 60)
    t = "{:02d}:{:02d}:{:02d}".format(h, m, s)
    return t


# ==================================================================================================


def load_audio(wav_path):
    audio, _ = sf.read(wav_path, dtype="int16")
    audio = audio / (np.iinfo(np.int16).max + 1)
    audio = np.expand_dims(audio, axis=0)
    audio = audio.astype(np.float32)
    return audio


# ==================================================================================================


def predict_signal_tf(audio):
    global tf_model

    output_data = tf_model.predict(audio, steps=1)
    output_data = output_data[0]
    return output_data


# ==================================================================================================


def get_intent_from_audio(audio_path):
    """Get intent from audio file"""

    audio = load_audio(audio_path)
    prediction = predict_signal_tf(audio)
    probs = prediction.tolist()

    results = slu_decoder.decode(probs)
    result = results[0]
    slungt_intent = slu_decoder.extract_text2intent(result)
    intent = json.loads(str(slungt_intent))

    intent["result"] = str(result)
    intent["greedy"] = slu_decoder.decode_greedy_text(probs)

    return intent


# ==================================================================================================


def convert_intent(intent):
    """Convert intent to rhino format"""

    if intent is None or intent["name"] is None:
        return None

    rh_intent = {
        "intent": intent["name"].replace("barrista_jaco-", ""),
        "slots": {},
        "text": intent["text"],
        "greedy": intent["greedy"],
        "result": intent["result"],
    }

    if "entities" in intent:
        for e in intent["entities"]:
            entity = e["name"].replace("barrista_jaco-", "")
            rh_intent["slots"][entity] = e["value"]

    return rh_intent


# ==================================================================================================


def compare_intents(intent, label):
    if intent is None:
        return False, "intent missing"

    if label["intent"].lower() != intent["intent"]:
        return False, "intent wrong"

    for slot in label["slots"].keys():
        if slot.lower() not in intent["slots"]:
            return False, "slot missing: {}".format(slot)

        detection_value = intent["slots"][slot.lower()].strip()
        label_value = label["slots"][slot].strip()
        if detection_value != label_value:
            return False, "value wrong: {} - {}".format(detection_value, label_value)

    return True, ""


# ==================================================================================================


def process_dir(data_path):
    files = sorted(os.listdir(data_path))

    if 0 < reduced_dataset <= len(files):
        files = files[0:reduced_dataset]

    results = []
    for file_name in tqdm.tqdm(files):
        intent = get_intent_from_audio(data_path + file_name)
        rh_intent = convert_intent(intent)
        # print(rh_intent)
        label = labels[file_name]

        same, error_reason = compare_intents(rh_intent, label)
        results.append(same)

        if not same:
            # Print wrong detections for debugging
            print("\n\n\n")
            print("ERROR", error_reason)
            print(file_name)
            print("Detected intent:", rh_intent)
            print(label)

    return results


# ==================================================================================================


def run_clean_speech():
    """Test clean speech and measure runtime and audio duration"""

    print("\nProcessing the clean audio directory ...")
    data_path = bm_data_path + "clean/"
    start_time = time.time()
    results = process_dir(data_path)
    conv_dur = time.time() - start_time

    # Count wrong and correct instead of using list length for debugging purposes
    # (allows to end the loop midways)
    num_correct = sum((1 for r in results if r))
    acc = num_correct / len(results)

    # Get duration of sample files, extra because this needs some time too
    audio_dur = 0
    files = sorted(os.listdir(data_path))
    if 0 < reduced_dataset <= len(files):
        files = files[0:reduced_dataset]
    for file_name in tqdm.tqdm(files):
        audio_dur += get_duration(data_path + file_name)

    msg = " The conversion of {} audio took {}"
    print(msg.format(seconds_to_hours(audio_dur), seconds_to_hours(conv_dur)))
    msg = " Correctly converted {}/{}. The accuracy is {:.4f}"
    print(msg.format(num_correct, len(results), acc))

    return acc


# ==================================================================================================


def run_noise_speech(noise_type):
    print("\nProcessing the {} noise audio directory ...".format(noise_type))
    accs = []

    for sd in tqdm.tqdm(snr_dbs):
        data_path = bm_data_path + "{}_{}db/".format(noise_type, sd)
        results = process_dir(data_path)
        num_correct = sum((1 for r in results if r))
        acc = num_correct / len(results)
        accs.append(round(acc, 4))

        msg = " Correctly converted {}/{} in {} with {} snr. The accuracy is {:.4f}"
        print(msg.format(num_correct, len(results), noise_type, sd, acc))
    return accs


# ==================================================================================================


def main():
    global tf_model, labels, reduced_dataset

    parser = argparse.ArgumentParser(description="Run benchmark with Jaco")
    parser.add_argument(
        "--reduced_dataset",
        default=0,
        type=int,
        help="Test with fewer files in each step",
    )
    args = parser.parse_args()

    reduced_dataset = args.reduced_dataset

    # Load stt model and run some warmup predictions
    stime = time.time()
    tf_model = tf.keras.models.load_model(stt_model_dir + "pb/")
    tf_model.predict(np.random.rand(1, 16000).astype(np.float32))
    tf_model.predict(np.random.rand(1, 48000).astype(np.float32))
    tf_model.predict(np.random.rand(1, 32000).astype(np.float32))
    tf_model.summary()
    print("Time to load stt model:", time.time() - stime)

    with open(rhino_bm_path + "data/label/label.json", encoding="utf-8") as f:
        labels = json.load(f)

    accuracies = {}
    start_time = time.time()

    accuracies["clean"] = round(run_clean_speech(), 4)
    for n in noises:
        accuracies[n] = run_noise_speech(n)

    print("\nBenchmark completed:")
    accuracies["duration"] = seconds_to_hours(time.time() - start_time)
    print(accuracies)


# ==================================================================================================

if __name__ == "__main__":
    main()
