import json
import os
import re

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
df_path = file_path + "dialogflow-export/"


# ==================================================================================================


def convert_intents(org_intents):
    intents = []

    for intent in org_intents:
        convint = "- "

        for ip in intent["data"]:
            if "alias" not in ip:
                convint += ip["text"].lower()
            else:
                convint += "[{}]({}.txt)".format(ip["text"].lower(), ip["alias"])

        convint = re.sub(r"\s+", " ", convint)
        convint = convint.strip()
        intents.append(convint)

    return intents


# ==================================================================================================


def convert_entities(org_entities):
    entities = []

    for entity in org_entities:
        entities.extend(entity["synonyms"])
        if not entity["value"] in entity["synonyms"]:
            entities.append(entity["value"])

    return entities


# ==================================================================================================

for file_name in sorted(os.listdir(df_path)):
    with open(df_path + file_name, encoding="utf-8") as file:
        content = json.load(file)

    print("\nReading file {} ...".format(file_name))

    if "usersays" in file_name:
        conv_ints = convert_intents(content)
        print("\n".join(conv_ints))

    elif "entries" in file_name:
        conv_ents = convert_entities(content)
        print("\n".join(conv_ents))
