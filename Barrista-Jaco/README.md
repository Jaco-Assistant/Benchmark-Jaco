# Benchmark Barrista Jaco

Uses benchmark from [Picovoice Rhino](https://github.com/Picovoice/speech-to-intent-benchmark) to compare the performance. \
The training data is either a subset of 50 intents or all of the 432 intents.

### Test Jaco

Steps to rebuild everything:

- Get dialogflow data: [here](https://github.com/Picovoice/speech-to-intent-benchmark/blob/master/data/dialogflow/)

- Copy needed intents and entities to `dialogflow-export` directory

- Run `convert_intents.py` and use the output to create the nlu files.

- Build container:

  ```bash
  docker build --progress=plain -t barrista_jaco - < Barrista-Jaco/Containerfile
  ```

- Set language in Jaco-Master's `global_config` file to english

- Generate dialog dataset:

  ```bash
  # Run from parent directory

  export TRAIN_INTENTS=432
  docker run --network host --rm \
    --volume `pwd`/Jaco-Master/skills/collect_data.py:/Jaco-Master/skills/collect_data.py:ro \
    --volume `pwd`/Jaco-Master/skills/generate_data.py:/Jaco-Master/skills/generate_data.py:ro \
    --volume `pwd`/Jaco-Master/skills/map_intents_slots.py:/Jaco-Master/skills/map_intents_slots.py:ro \
    --volume `pwd`/Jaco-Master/skills/sentence_tools.py:/Jaco-Master/skills/sentence_tools.py:ro \
    --volume `pwd`/Jaco-Master/skills/text_tools.py:/Jaco-Master/skills/text_tools.py:ro \
    --volume `pwd`/Jaco-Master/skills/basic_normalizer.py:/Jaco-Master/skills/basic_normalizer.py:ro \
    --volume `pwd`/Jaco-Master/slu-parser/moduldata/langdicts.json:/Jaco-Master/slu-parser/moduldata/langdicts.json:ro \
    --volume `pwd`/Jaco-Master/jacolib/:/jacolib/:ro \
    --volume `pwd`/Benchmark-Jaco/Barrista-Jaco/dialog/nlu${TRAIN_INTENTS}/:/Jaco-Master/skills/skills/Barrista-Jaco/dialog/nlu/:ro \
    --volume `pwd`/Jaco-Master/userdata/config/:/Jaco-Master/userdata/config/:ro \
    --volume `pwd`/Benchmark-Jaco/Barrista-Jaco/userdata/skill_topic_keys.json:/Jaco-Master/userdata/skill_topic_keys.json:ro \
    --volume `pwd`/Benchmark-Jaco/Barrista-Jaco/moduldata/collected/:/Jaco-Master/skills/collected/ \
    --volume `pwd`/Benchmark-Jaco/Barrista-Jaco/moduldata/generated/:/Jaco-Master/skills/generated/ \
    -it master_base_image_amd64 /bin/bash -c ' \
      python3 /Jaco-Master/skills/collect_data.py && \
      python3 /Jaco-Master/skills/map_intents_slots.py && \
      python3 /Jaco-Master/skills/generate_data.py'
  ```

- Create language model:

  ```bash
  docker run --network host --rm \
    --volume `pwd`/Barrista-Jaco/:/Benchmark-Jaco/Barrista-Jaco/ \
    --volume `pwd`/../slungt/:/slungt/ \
    --volume `pwd`/sttmodels/:/Benchmark-Jaco/sttmodels/ \
    -it barrista_jaco \
      python3 /slungt/preprocess/fullbuild_slu.py \
        --workdir /Benchmark-Jaco/Barrista-Jaco/moduldata/sludata/ \
        --nlufile_path /Benchmark-Jaco/Barrista-Jaco/moduldata/generated/nlu/nlu.json \
        --alphabet_path /Benchmark-Jaco/sttmodels/en-conformerctc-large-cb4/alphabet.json
  ```

- Run benchmark:

  ```bash
  docker run --network host --ipc host --rm --device /dev/snd \
    --volume `pwd`/Barrista-Jaco/:/Benchmark-Jaco/Barrista-Jaco/ \
    --volume `pwd`/../slungt/:/slungt/ \
    --volume `pwd`/sttmodels/:/Benchmark-Jaco/sttmodels/ \
    --volume `pwd`/media/:/Benchmark-Jaco/media/ \
    --volume ~/.asoundrc:/etc/asound.conf:ro \
    -it barrista_jaco

  # Build slungt
  cd /slungt/swig/; make all; cd /

  # Run the benchmark
  python3 /Benchmark-Jaco/Barrista-Jaco/run_benchmark.py --reduced_dataset 100

  # Rebuild plot in media directory, update values with above outputs before
  python3 /Benchmark-Jaco/Barrista-Jaco/plot_comparison.py
  ```

Clean dataset accuracy: 0.9935 \
Benchmark Runtime: 00:00:03 + 03:03:44 \
See complete [results](plot_comparison.py)
