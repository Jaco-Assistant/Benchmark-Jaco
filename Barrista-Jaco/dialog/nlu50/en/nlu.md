## intent:userSaysHi
- just going to say hi
- heya
- hello hi
- howdy
- hey there
- hi there
- greetings
- hey
- long time no see
- hello
- lovely day isn't it
- i greet you
- hello again
- hi
- hello there
- hi there
- a good day

## lookup:coffeeDrink
coffeeDrink.txt

## lookup:milkAmount
milkAmount.txt

## lookup:numberOfShots
numberOfShots.txt

## lookup:roast
roast.txt

## lookup:sugarAmount
sugarAmount.txt

## lookup:size
size.txt

## intent:orderDrink
- brew a [eight ounce](size.txt) [dark roast](roast.txt) [drip coffee](coffeeDrink.txt) with [brown sugar](sugarAmount.txt)
- i'd like a [dark roast](roast.txt) [sixteen ounce](size.txt) [double shot](numberOfShots.txt) [house coffee](coffeeDrink.txt)
- get me a [twelve ounce](size.txt) [latte](coffeeDrink.txt) with [a lot of sugar](sugarAmount.txt)
- i'd like a [twenty ounce](size.txt) [cappuccino](coffeeDrink.txt) with [a bit of sweetener](sugarAmount.txt)
- i'd like an [americano](coffeeDrink.txt) with [sweetener](sugarAmount.txt)
- brew a [light roast](roast.txt) [mocha](coffeeDrink.txt) with [cream](milkAmount.txt)
- brew a [light roast](roast.txt) [twelve ounce](size.txt) [double shot](numberOfShots.txt) [latte](coffeeDrink.txt) with [almond milk](milkAmount.txt)
- can i have a [medium roast](roast.txt) [single shot](numberOfShots.txt) [iced mocha](coffeeDrink.txt) with [lots of skim milk](milkAmount.txt) and [sweetener](sugarAmount.txt)
- can i have a [medium roast](roast.txt) [double shot](numberOfShots.txt) [latte](coffeeDrink.txt) with [a little bit of whole milk](milkAmount.txt)
- can i get a [dark roast](roast.txt) [eight ounce](size.txt) [double shot](numberOfShots.txt) [drip coffee](coffeeDrink.txt) with [a lot of brown sugar](sugarAmount.txt)
- i'd like a [twenty ounce](size.txt) [single shot](numberOfShots.txt) [coffee](coffeeDrink.txt) with [lots of milk](milkAmount.txt)
- can i get a [large](size.txt) [dark roast](roast.txt) [latte](coffeeDrink.txt) with [some almond milk](milkAmount.txt) and [some sweetener](sugarAmount.txt)
- can i have a [medium](size.txt) [americano](coffeeDrink.txt)
- give me a [dark roast](roast.txt) [medium](size.txt) [iced mocha](coffeeDrink.txt)
- brew a [sixteen ounce](size.txt) [espresso](coffeeDrink.txt)
- make me a [small](size.txt) [latte](coffeeDrink.txt) with [soy milk](milkAmount.txt)
- i want a [single shot](numberOfShots.txt) [iced coffee](coffeeDrink.txt) with [some almond milk](milkAmount.txt)
- give me a [medium](size.txt) [light roast](roast.txt) [single shot](numberOfShots.txt) [latte](coffeeDrink.txt) with [a little bit of milk](milkAmount.txt)
- can i have a [medium](size.txt) [mocha](coffeeDrink.txt) with [a little bit of brown sugar](sugarAmount.txt)
- may i have a [triple shot](numberOfShots.txt) [americano](coffeeDrink.txt)
- get me a [cappuccino](coffeeDrink.txt) with [a bit of sugar](sugarAmount.txt)
- may i have a [cappuccino](coffeeDrink.txt) with [lots of skim milk](milkAmount.txt) and [brown sugar](sugarAmount.txt)
- brew a [americano](coffeeDrink.txt)
- get me a [sixteen ounce](size.txt) [cappuccino](coffeeDrink.txt) with [a lot of sweetener](sugarAmount.txt) and [soy milk](milkAmount.txt)
- can i get a [single shot](numberOfShots.txt) [medium](size.txt) [iced coffee](coffeeDrink.txt) with [lots of brown sugar](sugarAmount.txt)
- can i get a [double shot](numberOfShots.txt) [coffee](coffeeDrink.txt) with [cream](milkAmount.txt) and [some sweetener](sugarAmount.txt)
- i'd like a [medium roast](roast.txt) [single shot](numberOfShots.txt) [house coffee](coffeeDrink.txt) with [a lot of milk](milkAmount.txt) and [sugar](sugarAmount.txt)
- make me a [medium roast](roast.txt) [iced coffee](coffeeDrink.txt)
- i'd like a [triple shot](numberOfShots.txt) [eight ounce](size.txt) [espresso](coffeeDrink.txt) with [a little bit of soy milk](milkAmount.txt) and [some brown sugar](sugarAmount.txt)
- can i have a [dark roast](roast.txt) [triple shot](numberOfShots.txt) [mocha](coffeeDrink.txt) with [sweetener](sugarAmount.txt)
- can i get a [light roast](roast.txt) [single shot](numberOfShots.txt) [cappuccino](coffeeDrink.txt)
- i want a [double shot](numberOfShots.txt) [espresso](coffeeDrink.txt) with [brown sugar](sugarAmount.txt)
- i'd like a [twelve ounce](size.txt) [mocha](coffeeDrink.txt) with [cream](milkAmount.txt) and [lots of brown sugar](sugarAmount.txt)
- i want a [medium roast](roast.txt) [single shot](numberOfShots.txt) [drip coffee](coffeeDrink.txt) with [a lot of milk](milkAmount.txt) and [lots of brown sugar](sugarAmount.txt)
- make me a [triple shot](numberOfShots.txt) [dark roast](roast.txt) [mocha](coffeeDrink.txt)
- get me a [sixteen ounce](size.txt) [triple shot](numberOfShots.txt) [iced coffee](coffeeDrink.txt) with [a lot of brown sugar](sugarAmount.txt) and [skim milk](milkAmount.txt)
- make me a [double shot](numberOfShots.txt) [large](size.txt) [espresso](coffeeDrink.txt) with [whole milk](milkAmount.txt)
- can i have a [single shot](numberOfShots.txt) [small](size.txt) [espresso](coffeeDrink.txt) with [some skim milk](milkAmount.txt)
- give me a [small](size.txt) [medium roast](roast.txt) [drip coffee](coffeeDrink.txt) with [a little bit of sweetener](sugarAmount.txt) and [almond milk](milkAmount.txt)
- can i have a [single shot](numberOfShots.txt) [small](size.txt) [espresso](coffeeDrink.txt) with [sugar](sugarAmount.txt)
- give me a [single shot](numberOfShots.txt) [light roast](roast.txt) [large](size.txt) [coffee](coffeeDrink.txt) with [brown sugar](sugarAmount.txt)
- make me a [light roast](roast.txt) [eight ounce](size.txt) [iced mocha](coffeeDrink.txt)
- make me a [latte](coffeeDrink.txt)
- i want a [twelve ounce](size.txt) [mocha](coffeeDrink.txt)
- can i get a [sixteen ounce](size.txt) [coffee](coffeeDrink.txt) with [some soy milk](milkAmount.txt)
- i'd like a [dark roast](roast.txt) [drip coffee](coffeeDrink.txt)
- get me a [coffee](coffeeDrink.txt) with [lots of brown sugar](sugarAmount.txt) and [milk](milkAmount.txt)
- may i have a [medium roast](roast.txt) [single shot](numberOfShots.txt) [mocha](coffeeDrink.txt) with [a little bit of whole milk](milkAmount.txt) and [sweetener](sugarAmount.txt)
- make me a [double shot](numberOfShots.txt) [espresso](coffeeDrink.txt)
- can i have a [coffee](coffeeDrink.txt)
