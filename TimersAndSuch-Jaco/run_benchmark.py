import argparse
import json
import os
import sys
import time

import num2words as n2w
import numpy as np
import pandas as pd
import soundfile as sf
import tensorflow as tf
import tqdm
from word2number import w2n

sys.path.insert(1, "/slungt/swig/")
import slungt  # noqa: E402 pylint: disable=wrong-import-position

sys.path.insert(1, "/slungt/tests/")
import test_interface  # noqa: E402 pylint: disable=wrong-import-position

# ==================================================================================================

filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
data_dir = filepath + "../datasets/timers-and-such/"
label_csv = data_dir + "test-real.csv"

stt_model_dir = filepath + "../sttmodels/en-conformerctc-large-new/"
# stt_model_dir = filepath + "../sttmodels/en-conformerctc-large-cb4/"
tf_model: tf.keras.Model

alphabet_path = stt_model_dir + "alphabet.json"
with open(alphabet_path, "r", encoding="utf-8") as afile:
    alphabet = json.load(afile)

datapath = filepath + "moduldata/sludata/"
vocabs, intents, lms = test_interface.load_data(datapath)

slu_decoder = slungt.Decoder(
    list(alphabet),
    96,
    vocabs,
    intents,
    lms,
    lm_alpha=1.5,
    token_min_logprob=-10.0,
)


# ==================================================================================================


def seconds_to_hours(secs):
    secs = int(secs)
    m, s = divmod(secs, 60)
    h, m = divmod(m, 60)
    t = "{:02d}:{:02d}:{:02d}".format(h, m, s)
    return t


# ==================================================================================================


def get_duration(filename):
    """Get duration of the wav file"""
    length = sf.info(filename).duration
    return length


# ==================================================================================================


def load_labels(path: str) -> list:
    dataset = pd.read_csv(path, sep=",", keep_default_na=False)
    dataset = dataset[["path", "semantics", "asr_transcription"]]
    dataset = dataset.rename(columns={"asr_transcription": "text"})

    # Load intent dictionary
    dataset = dataset.to_dict(orient="records")
    for entry in dataset:
        semantics = entry["semantics"].replace("'", '"')
        semantics = json.loads(semantics)
        entry.pop("semantics")
        entry["intent"] = semantics["intent"]
        entry["slots"] = semantics["slots"]
        entry["path"] = data_dir + entry["path"]

    return dataset


# ==================================================================================================


def load_audio(wav_path):
    audio, _ = sf.read(wav_path, dtype="int16")
    audio = audio / (np.iinfo(np.int16).max + 1)
    audio = np.expand_dims(audio, axis=0)
    audio = audio.astype(np.float32)
    return audio


# ==================================================================================================


def predict_signal_tf(audio):
    global tf_model

    output_data = tf_model.predict(audio, steps=1)
    output_data = output_data[0]
    return output_data


# ==================================================================================================


def get_intent_from_audio(audio_path):
    """Get intent from audio file"""

    audio = load_audio(audio_path)
    prediction = predict_signal_tf(audio)
    probs = prediction.tolist()

    results = slu_decoder.decode(probs)
    result = results[0]
    slungt_intent = slu_decoder.extract_text2intent(result)
    intent = json.loads(str(slungt_intent))

    intent["result"] = str(result)
    intent["greedy"] = slu_decoder.decode_greedy_text(probs)

    return intent


# ==================================================================================================


def words2num(text: str) -> float:
    text = text.lower()
    num = 0

    # words2num can't handle negative values
    if any(t in text for t in ["minus", "negative"]):
        text = " ".join(text.split(" ")[1:])
        num_is_negative = True
    else:
        num_is_negative = False

    if "dot" in text:
        text = text.replace("dot", "point")

    # Handle digits
    if "point" in text:
        text, t2 = text.split("point")
        text = text.strip()
        t2 = t2.strip()
        if " " in t2 and "y" not in t2.split(" ")[0]:
            # Handle "point six six" versions
            try:
                n1 = w2n.word_to_num(t2.split(" ")[0].strip())
                n2 = w2n.word_to_num(t2.split(" ")[1].strip())
                n = n1 * 0.1 + n2 * 0.01
            except ValueError:
                print("Could not convert:", t2)
                n = 0
        else:
            # Handle "point sixty six" versions
            n = w2n.word_to_num(t2)
            if n >= 10:
                n = n * 0.01
            else:
                n = n * 0.1
        num += round(n, 2)

    # Handle spoken "one four five" version for numbers above 100
    if " " in text and all(
        t in [n2w.num2words(i) for i in range(10)] for t in text.split(" ")
    ):
        for i, t in enumerate(reversed(text.split(" "))):
            try:
                n = w2n.word_to_num(t)
            except ValueError:
                print("Could not convert:", t)
                n = 0
            num += 10**i * n

    # Handle spoken "one twenty" version for numbers above 100
    elif (
        " " in text
        and text.split(" ")[0] in [n2w.num2words(i) for i in range(10)]
        and text.split(" ")[1] != "hundred"
    ):
        t1, t2 = text.split(" ", maxsplit=1)
        try:
            n1 = w2n.word_to_num(t1)
        except ValueError:
            print("Could not convert:", t1)
            n1 = 0
        try:
            n2 = w2n.word_to_num(t2)
        except ValueError:
            print("Could not convert:", t2)
            n2 = 0

        num += n1 * 100 + n2

    # Thats how users should normally speak numbers ...
    else:
        try:
            num += w2n.word_to_num(text)
        except ValueError:
            print("Could not convert:", text)
            num = 0

    if num_is_negative:
        num = num * -1

    return num


# ==================================================================================================


def convert_intent(intent):
    """Convert intent format"""

    convint = {
        "intent": "",
        "slots": {},
        "text": intent["text"],
        "greedy": intent["greedy"],
        "result": intent["result"],
    }

    if "-" in intent["name"]:
        convint["intent"] = intent["name"].split("-")[1]

    # Add empty slots
    if convint["intent"] == "setalarm":
        convint["slots"] = {"am_or_pm": "", "alarm_hour": 0, "alarm_minute": 0}
    elif convint["intent"] == "settimer":
        convint["slots"] = {"hours": 0, "minutes": 0, "seconds": 0}

    # Insert detected slot values
    if "entities" in intent:
        for e in intent["entities"]:
            if "role" in e and e["role"] != "":
                entname = e["role"]
            else:
                entname = e["name"].split("-")[1]

            entval = e["value"]
            if entname in [
                "alarm_hour",
                "alarm_minute",
                "hours",
                "minutes",
                "seconds",
                "number1",
                "number2",
                "amount",
            ]:
                entval = words2num(entval)

            convint["slots"][entname] = entval

    return convint


# ==================================================================================================


def compare_intents(intent, label):
    if label["intent"].lower() != intent["intent"]:
        return False, "intent wrong"

    for slot in label["slots"]:
        if slot not in intent["slots"]:
            return False, "slot missing: {}".format(slot)

        labelval = label["slots"][slot]
        if isinstance(labelval, str):
            labelval = labelval.lower().strip()
        if labelval != intent["slots"][slot]:
            return False, "slot wrong: {}".format(slot)

    return True, ""


# ==================================================================================================


def run_benchmark(labels: dict):
    print("\nRunning benchmark ...")
    start_time = time.time()

    results = []
    for label in tqdm.tqdm(labels):
        intent = get_intent_from_audio(label["path"])
        convint = convert_intent(intent)
        same, error_reason = compare_intents(convint, label)
        results.append(same)

        if not same:
            # Print wrong predictions for debugging
            print("\n")
            print("ERROR", error_reason)
            print("Detected intent:", intent)
            print("Converted intent:", convint)
            print("Label:", label)

    conv_dur = time.time() - start_time
    msg = " The benchmark took {}"
    print(msg.format(seconds_to_hours(conv_dur)))

    # Count wrong and correct instead of using list length for debugging purposes
    # (allows to end the loop midways)
    num_correct = sum((1 for r in results if r))
    acc = num_correct / len(results)
    msg = " Correctly converted {}/{}. The accuracy is {:.4f}"
    print(msg.format(num_correct, len(results), acc))


# ==================================================================================================


def main():
    global tf_model

    parser = argparse.ArgumentParser(description="Run benchmark with Jaco")
    parser.add_argument(
        "--reduced_dataset",
        default=0,
        type=int,
        help="Test with fewer files",
    )
    args = parser.parse_args()

    # Load stt model and run some warmup predictions
    stime = time.time()
    tf_model = tf.keras.models.load_model(stt_model_dir + "pb/")
    tf_model.predict(np.random.rand(1, 16000).astype(np.float32))
    tf_model.predict(np.random.rand(1, 48000).astype(np.float32))
    tf_model.predict(np.random.rand(1, 32000).astype(np.float32))
    tf_model.summary()
    print("Time to load stt model:", time.time() - stime)

    # Load labels
    labels = load_labels(label_csv)
    if 0 < args.reduced_dataset <= len(labels):
        # Reduce dataset size if flag given
        labels = labels[0 : args.reduced_dataset]

    # Get duration of sample files, extra because this needs some time too
    audio_dur = 0
    wav_files = [lb["path"] for lb in labels]
    for file_name in tqdm.tqdm(wav_files):
        audio_dur += get_duration(os.path.join(file_name))
    msg = "The benchmark contains {} files with {} hours of audio"
    print(msg.format(len(wav_files), seconds_to_hours(audio_dur)))

    # Now run the benchmark
    run_benchmark(labels)


# ==================================================================================================

if __name__ == "__main__":
    main()
