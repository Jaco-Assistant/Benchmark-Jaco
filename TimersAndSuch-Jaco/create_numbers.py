import os
import re

import num2words as n2w

# ==================================================================================================


filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
skillpath = filepath + "skills/tas/dialog/nlu/en/"
path_hundred = skillpath + "numbers2hundred.txt"
path_thousand = skillpath + "numbers2thousand.txt"
path_digits = skillpath + "numbers_with_digits.txt"
path_alarm_hours = skillpath + "alarm_hour.txt"
path_alarm_minutes = skillpath + "alarm_minute.txt"
path_amounts = skillpath + "amount.txt"

# ==================================================================================================


def save_numbers(numbers: list, path: str):
    numbers = [re.sub(r"-(?!\>)", " ", n) for n in numbers]
    with open(path, "w+", encoding="utf-8") as file:
        file.write("\n".join(numbers) + "\n")


# ==================================================================================================


def add_digits(i, numw):
    numbers = []

    # Only add digits to numbers lower than 100
    if -100 < i < 100:
        # Add "xx point zero" versions
        num = "{} point zero".format(numw)
        numbers.append(num)

        # Add the formal "xx point x" numbers
        for j in range(1, 10):
            num = "{} point {}".format(numw, n2w.num2words(j))
            numbers.append(num)

        for j in range(0, 100):
            # Add the formal "xx point x x" numbers
            num = "{} point {} {}".format(
                numw, n2w.num2words(divmod(j, 10)[0]), n2w.num2words(divmod(j, 10)[1])
            )
            numbers.append(num)

            # Add the spoken "xx point xx" version
            num = "{} point {}".format(numw, n2w.num2words(j))
            numbers.append(num)

    return numbers


# ==================================================================================================


def build_numbers(numrange, digits=False, negative=False):
    numbers = []
    for i in range(-numrange if negative else 0, numrange + 1):
        # Standard formal version
        numw = n2w.num2words(i)
        numbers.append(numw)
        if digits:
            numbers.extend(add_digits(i, numw))

        # Add spoken "eight hundred eighty eight" without "and" for numbers above 100
        if i < -100 or i > 100:
            numw = n2w.num2words(i)
            numw = numw.replace(" and ", " ")
            numbers.append(numw)
            if digits:
                numbers.extend(add_digits(i, numw))
            if negative:
                numw = "minus " + numw
                numbers.append(numw)
                if digits:
                    numbers.extend(add_digits(i, numw))

        # Add version where single digits are spoken separately
        if i >= 10:
            numw = ""
            for j in range(len(str(i))):
                numw += n2w.num2words(int(str(i)[j])) + " "
            numw = numw.strip()
            numbers.append(numw)
            if digits:
                numbers.extend(add_digits(i, numw))
            if negative:
                numw = "minus " + numw
                numbers.append(numw)
                if digits:
                    numbers.extend(add_digits(i, numw))

        # Add spoken "one twenty" for numbers above 100 without digits
        if i > 100 and divmod(i, 100)[1] > 9:
            numw = "{} {}".format(
                n2w.num2words(divmod(i, 100)[0]), (n2w.num2words(divmod(i, 100)[1]))
            )
            numbers.append(numw)
            if digits:
                numbers.extend(add_digits(i, numw))
            if negative:
                numw = "minus " + numw
                numbers.append(numw)
                if digits:
                    numbers.extend(add_digits(i, numw))

    # Workaround for negative 0.xx values
    if negative and digits:
        for j in range(1, 100):
            if j < 10:
                num = "minus zero point {}".format(n2w.num2words(j))
                numbers.append(num)
            else:
                num = "minus zero point {} {}".format(
                    n2w.num2words(divmod(j, 10)[0]), n2w.num2words(divmod(j, 10)[1])
                )
                numbers.append(num)

                # Add the spoken "xx point xx" version
                num = "minus zero point {}".format(n2w.num2words(j))
                numbers.append(num)

    # Add "negative" as synonym for "minus"
    if negative:
        for num in list(numbers):
            if "minus" in num:
                numbers.append(num.replace("minus", "negative"))

    # Add "dot" as synonym for "point"
    if digits:
        for num in list(numbers):
            if "point" in num:
                numbers.append(num.replace("point", "dot"))

    return numbers


# ==================================================================================================


def build_amounts(numrange):
    numbers = []
    amounts = [
        "({n} a half|{n} and a half)->{n} point five",
        "({n} a third|{n} and a third|{n} and one third|{n} one third)->{n} point three three",
        "({n} two thirds|{n} and two thirds|{n} two thirds)->{n} point six seven",
        "({n} a quarter|{n} and a quarter|{n} and one quarter|{n} one quarter)->{n} point two five",
        "({n} three quarters|{n} and three quarters)->{n} point seven five",
    ]
    for i in range(-numrange, numrange + 1):
        num = n2w.num2words(i)
        numbers.append(num)

        if 0 <= i < 10:
            for am in amounts:
                if i == 0:
                    am = am.replace("{n} ", "", 1)
                    anum = am.format(n=num)
                    numbers.append(anum)
                else:
                    anum = am.format(n=num)
                    numbers.append(anum)

        # Add spoken "eight hundred eighty eight" without "and" for numbers above 100
        if i > 100:
            num = n2w.num2words(i)
            num = num.replace(" and ", " ")
            numbers.append(num)
            num = "minus " + num
            numbers.append(num)

        # Add spoken "one twenty" for numbers above 100
        if i > 100 and divmod(i, 100)[1] > 9:
            num = "{} {}".format(
                n2w.num2words(divmod(i, 100)[0]), (n2w.num2words(divmod(i, 100)[1]))
            )
            numbers.append(num)
            num = "minus " + num
            numbers.append(num)

        # Add version where single digits are spoken separately
        if i >= 100:
            num = ""
            for j in range(len(str(i))):
                num += n2w.num2words(int(str(i)[j])) + " "
            num = num.strip()
            numbers.append(num)
            num = "minus " + num
            numbers.append(num)

    # Add "negative" as synonym for "minus"
    for num in list(numbers):
        if "minus" in num:
            numbers.append(num.replace("minus", "negative"))

    return numbers


# ==================================================================================================


def main():
    if not os.path.isdir(skillpath):
        os.makedirs(skillpath)

    # Hours
    numbers = list(range(1, 13))
    numbers = [n2w.num2words(n) for n in numbers]
    save_numbers(numbers, path_alarm_hours)

    # Minutes
    numbers = list(range(1, 60))
    numbers = [n2w.num2words(n) for n in numbers]
    save_numbers(numbers, path_alarm_minutes)

    # Percent numbers
    numbers = list(range(101))
    numbers = [n2w.num2words(n) for n in numbers]
    save_numbers(numbers, path_hundred)

    # Numbers to thousand
    numbers = build_numbers(numrange=1000, digits=False, negative=False)
    save_numbers(numbers, path_thousand)

    # Digit numbers
    numbers = build_numbers(numrange=1000, digits=True, negative=True)
    save_numbers(numbers, path_digits)

    # Amounts
    numbers = build_amounts(numrange=1000)
    save_numbers(numbers, path_amounts)


# ==================================================================================================

if __name__ == "__main__":
    main()
