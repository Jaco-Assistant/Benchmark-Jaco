# Benchmark TimersAndSuch Jaco

TimersAndSuch tests common use-cases involving numbers.
The corresponding paper can be found [here](https://arxiv.org/pdf/2104.01604.pdf).

<br>

### Test Jaco

Steps to rebuild everything:

- Download dataset (real partition only), from [here](https://zenodo.org/record/4623772#.YMoemnVfiV4)

- Extract dataset to `datasets/timers-and-such/`.

- Build container:

  ```bash
  docker build --progress=plain -t timersandsuch_jaco - < TimersAndSuch-Jaco/Containerfile
  ```

- Build number text files and convert intents

  ```bash
  docker run --rm \
    --volume `pwd`/TimersAndSuch-Jaco/:/Benchmark-Jaco/TimersAndSuch-Jaco/ \
    --volume `pwd`/datasets/:/Benchmark-Jaco/datasets/ \
    -it timersandsuch_jaco

  python3 TimersAndSuch-Jaco/create_numbers.py
  python3 TimersAndSuch-Jaco/convert_intents.py
  ```

- Set language in Jaco-Master's `global_config` file to english

- Generate dialog dataset:

  ```bash
  # Run from parent directory
  docker run --network host --rm \
    --volume `pwd`/Jaco-Master/skills/collect_data.py:/Jaco-Master/skills/collect_data.py:ro \
    --volume `pwd`/Jaco-Master/skills/generate_data.py:/Jaco-Master/skills/generate_data.py:ro \
    --volume `pwd`/Jaco-Master/skills/map_intents_slots.py:/Jaco-Master/skills/map_intents_slots.py:ro \
    --volume `pwd`/Jaco-Master/skills/sentence_tools.py:/Jaco-Master/skills/sentence_tools.py:ro \
    --volume `pwd`/Jaco-Master/skills/text_tools.py:/Jaco-Master/skills/text_tools.py:ro \
    --volume `pwd`/Jaco-Master/skills/basic_normalizer.py:/Jaco-Master/skills/basic_normalizer.py:ro \
    --volume `pwd`/Jaco-Master/slu-parser/moduldata/langdicts.json:/Jaco-Master/slu-parser/moduldata/langdicts.json:ro \
    --volume `pwd`/Jaco-Master/jacolib/:/jacolib/:ro \
    --volume `pwd`/Benchmark-Jaco/TimersAndSuch-Jaco/skills/:/Jaco-Master/skills/skills/:ro \
    --volume `pwd`/Jaco-Master/userdata/config/:/Jaco-Master/userdata/config/:ro \
    --volume `pwd`/Benchmark-Jaco/TimersAndSuch-Jaco/userdata/skill_topic_keys.json:/Jaco-Master/userdata/skill_topic_keys.json:ro \
    --volume `pwd`/Benchmark-Jaco/TimersAndSuch-Jaco/moduldata/collected/:/Jaco-Master/skills/collected/ \
    --volume `pwd`/Benchmark-Jaco/TimersAndSuch-Jaco/moduldata/generated/:/Jaco-Master/skills/generated/ \
    -it master_base_image_amd64 /bin/bash -c ' \
      python3 /Jaco-Master/skills/collect_data.py && \
      python3 /Jaco-Master/skills/map_intents_slots.py && \
      python3 /Jaco-Master/skills/generate_data.py'
  ```

- Create language model:

  ```bash
  docker run --network host --rm \
    --volume `pwd`/TimersAndSuch-Jaco/:/Benchmark-Jaco/TimersAndSuch-Jaco/ \
    --volume `pwd`/../slungt/:/slungt/ \
    --volume `pwd`/sttmodels/:/Benchmark-Jaco/sttmodels/ \
    -it timersandsuch_jaco \
      python3 /slungt/preprocess/fullbuild_slu.py \
        --workdir /Benchmark-Jaco/TimersAndSuch-Jaco/moduldata/sludata/ \
        --nlufile_path /Benchmark-Jaco/TimersAndSuch-Jaco/moduldata/generated/nlu/nlu.json \
        --alphabet_path /Benchmark-Jaco/sttmodels/en-conformerctc-large-new/alphabet.json
  ```

- Run benchmark:

  ```bash
  docker run --rm --network host \
    --volume `pwd`/TimersAndSuch-Jaco/:/Benchmark-Jaco/TimersAndSuch-Jaco/ \
    --volume `pwd`/../slungt/:/slungt/ \
    --volume `pwd`/datasets/:/Benchmark-Jaco/datasets/ \
    --volume `pwd`/sttmodels/:/Benchmark-Jaco/sttmodels/ \
    -it timersandsuch_jaco

  # Build slungt
  cd /slungt/swig/; make all; cd /

  # Run the benchmark
  python3 /Benchmark-Jaco/TimersAndSuch-Jaco/run_benchmark.py --reduced_dataset 50
  ```

Accuracy: 0.9250 \
Benchmark Runtime: 00:00:07 + 00:01:21
