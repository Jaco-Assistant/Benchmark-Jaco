import json
import os
import re

import num2words
import pandas as pd

# ==================================================================================================


filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
skillpath = filepath + "skills/tas/dialog/nlu/en/"
datapath = filepath + "../datasets/timers-and-such/"

path_input_train = datapath + "train-real.csv"
path_input_dev = datapath + "dev-real.csv"
path_nlu = skillpath + "nlu.md"

am_or_pm = ["(a m)->am", "(p m)->pm"]
math_ops = ["times", "divided by", "plus", "minus"]

unit_conversion = [
    "convert [---](amount.txt) [---](unit.txt?unit1) to [---](unit.txt?unit2)",
    "how many [---](unit.txt?unit2) are there in [---](amount.txt) [---](unit.txt?unit1)",
    "calculate [---](amount.txt) [---](unit.txt?unit1) in [---](unit.txt?unit2)",
    "(what is|what's) [---](amount.txt) [---](unit.txt?unit1) in [---](unit.txt?unit2)",
]

units = {
    "(celsius|degrees celsius)->celsius",
    "(fahrenheit|degrees fahrenheit)->fahrenheit",
    "(kelvin|degrees kelvin)->kelvin",
    "(feet|foot)->foot",
    "(inch|inches)->inch",
}

slu_md = (
    "## lookup:am_or_pm\n"
    "am_or_pm.txt\n\n"
    "## lookup:op\n"
    "op.txt\n\n"
    "## lookup:unit\n"
    "unit.txt\n\n"
    "## lookup:alarm_hour\n"
    "alarm_hour.txt\n\n"
    "## lookup:alarm_minute\n"
    "alarm_minute.txt\n\n"
    "## lookup:numbers2hundred\n"
    "numbers2hundred.txt\n\n"
    "## lookup:numbers2thousand\n"
    "numbers2thousand.txt\n\n"
    "## lookup:numbers_with_digits\n"
    "numbers_with_digits.txt\n"
)

# ==================================================================================================


def save_numbers(numbers: list, path: str):
    numbers = [n.replace("-", " ") for n in numbers]
    with open(path, "w+", encoding="utf-8") as file:
        file.write("\n".join(numbers) + "\n")


# ==================================================================================================


def convert_entry(entry: dict) -> dict:
    if entry["intent"] == "SetAlarm":
        text = entry["text"]
        text = re.sub(r" \d.*", "", text)
        text += " [{}](alarm_hour.txt) {} [{}](am_or_pm.txt)"

        if entry["slots"]["alarm_minute"] == 0:
            minutes = ""
        else:
            minutes = "[{}](alarm_minute.txt)"
            minutes = minutes.format(
                num2words.num2words(entry["slots"]["alarm_minute"])
            )

        hours = num2words.num2words(entry["slots"]["alarm_hour"])
        ampm = " ".join(list(entry["slots"]["am_or_pm"]))

        text = text.format(hours, minutes, ampm)
        entry["text"] = text
        return entry

    if entry["intent"] == "SetTimer":
        text = entry["text"]
        text = re.sub(r" \d.*", "", text)
        text += " {} {} {}"

        conjunction = " (and| ) "
        hours, minutes, seconds = "", "", ""

        if entry["slots"]["hours"] > 0:
            hours = num2words.num2words(entry["slots"]["hours"])
            if entry["slots"]["hours"] == 1:
                hours = "[{}](numbers2hundred.txt?hours) hour".format(hours)
            else:
                hours = "[{}](numbers2hundred.txt?hours) hours".format(hours)
        if entry["slots"]["minutes"] > 0:
            minutes = num2words.num2words(entry["slots"]["minutes"])
            if entry["slots"]["minutes"] == 1:
                minutes = "[{}](numbers2hundred.txt?minutes) minute".format(minutes)
            else:
                minutes = "[{}](numbers2hundred.txt?minutes) minutes".format(minutes)
        if entry["slots"]["seconds"] > 0:
            seconds = num2words.num2words(entry["slots"]["seconds"])
            if entry["slots"]["seconds"] == 1:
                seconds = "[{}](numbers2thousand.txt?seconds) second".format(seconds)
            else:
                seconds = "[{}](numbers2thousand.txt?seconds) seconds".format(seconds)

        if hours != "" and minutes != "":
            minutes = conjunction + minutes
        if (minutes != "" and seconds != "") or (hours != "" and seconds != ""):
            seconds = conjunction + seconds

        text = text.format(hours, minutes, seconds)
        entry["text"] = text
        return entry

    if entry["intent"] == "SimpleMath":
        text = entry["text"]
        text = re.sub(r" \d.*", "", text)
        text += " [{}](numbers_with_digits.txt?number1) [{}](op.txt)"
        text += " [{}](numbers_with_digits.txt?number2)"

        number1 = num2words.num2words(entry["slots"]["number1"])
        number2 = num2words.num2words(entry["slots"]["number2"])
        text = text.format(number1, entry["slots"]["op"].strip(), number2)
        entry["text"] = text
        return entry

    if entry["intent"] == "UnitConversion":
        return {}

    # No matching intent
    raise ValueError()


# ==================================================================================================


def main():
    global units, slu_md

    if not os.path.isdir(skillpath):
        os.makedirs(skillpath)

    # Load dataset
    data_train = pd.read_csv(path_input_train, sep=",", keep_default_na=False)
    data_dev = pd.read_csv(path_input_dev, sep=",", keep_default_na=False)
    dataset = pd.concat([data_train, data_dev], axis=0, join="inner")
    dataset = dataset[["semantics", "transcription"]]
    dataset = dataset.rename(columns={"transcription": "text"})

    # Load intent dictionary
    dataset = dataset.to_dict(orient="records")
    for entry in dataset:
        semantics = entry["semantics"].replace("'", '"')
        semantics = json.loads(semantics)
        entry.pop("semantics")
        entry["intent"] = semantics["intent"]
        entry["slots"] = semantics["slots"]

    # Collect units
    for entry in dataset:
        if entry["intent"] == "UnitConversion":
            unit1 = entry["slots"]["unit1"].lower()
            unit2 = entry["slots"]["unit2"].lower()

            skipunits = ["celsius", "fahrenheit", "kelvin", "foot", "inch"]
            if unit1 not in skipunits:
                units = units.union(set([unit1]))
            if unit2 not in skipunits:
                units = units.union(set([unit2]))

    # Add plural form to units
    units = list(units)
    for i, unit in enumerate(list(units)):
        if not unit.startswith("("):
            units[i] = "({}|{}s)->{}".format(unit, unit, unit)

    # Convert transcriptions
    new_ds = []
    for entry in dataset:
        entry = convert_entry(entry)
        if entry != {}:
            new_ds.append(entry)

    # Some formatting
    dataset = new_ds
    for entry in dataset:
        entry["text"] = entry["text"].lower()
        entry["text"] = entry["text"].replace("-", " ")
        entry["text"] = " ".join(entry["text"].split())

    # Write nlu file
    for intent in ["SetAlarm", "SetTimer", "SimpleMath"]:
        slu_md += "\n## intent:{}\n".format(intent)
        for entry in dataset:
            if entry["intent"] == intent:
                slu_md += "- {}\n".format(entry["text"])
    slu_md += "\n## intent:{}\n".format("UnitConversion")
    slu_md += "- " + "\n- ".join(unit_conversion) + "\n"
    with open(path_nlu, "w+", encoding="utf-8") as file:
        file.write(slu_md)

    # Write lookups
    with open(skillpath + "am_or_pm.txt", "w+", encoding="utf-8") as file:
        file.write("\n".join(am_or_pm) + "\n")
    with open(skillpath + "op.txt", "w+", encoding="utf-8") as file:
        file.write("\n".join(math_ops) + "\n")
    with open(skillpath + "unit.txt", "w+", encoding="utf-8") as file:
        file.write("\n".join(units) + "\n")


# ==================================================================================================

if __name__ == "__main__":
    main()
