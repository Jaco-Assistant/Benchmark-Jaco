import copy
import json
import os
import re
from typing import List

# ==================================================================================================


filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
slu_file_path = filepath + "../TimersAndSuch-Jaco/moduldata/generated/nlu/nlu.json"
outpath = filepath + "skill-export/tas.json"

slotpattern = r"\[[^\(]*\]\(([^\)]*)\)"
alexadata = {
    "interactionModel": {
        "languageModel": {
            "invocationName": "benchmark experiment",
            "intents": [
                {"name": "AMAZON.CancelIntent", "samples": []},
                {"name": "AMAZON.HelpIntent", "samples": []},
                {"name": "AMAZON.StopIntent", "samples": []},
                {"name": "AMAZON.NavigateHomeIntent", "samples": []},
                {"name": "AMAZON.FallbackIntent", "samples": []},
            ],
            "types": [],
        }
    }
}
alexaintent = {
    "name": "",
    "slots": [],
    "samples": [],
}
alexatype = {
    "name": "",
    "values": [],
}

# ==================================================================================================


def inject_alternatives(sentence: str) -> List[str]:
    """Create list of sentences with every possible alternative word combination"""

    lines = [sentence]
    alt_pattern = r"(?<!])\(([^)]+)\)"
    matches = re.findall(alt_pattern, sentence)

    if len(matches) > 0:
        for block in matches:
            partial_lines = []
            for line in lines:
                for alt in block.split("|"):
                    sen = re.sub(alt_pattern, alt, line, count=1)
                    sen = re.sub(r"\s+", " ", sen)
                    partial_lines.append(sen)

            # If there are multiple alternatives in one sentence, go through the list again
            # This will result in exponential increase of the sentence amount
            lines = partial_lines

    lines = [line.strip() for line in lines]
    return lines


# ==================================================================================================


def convert_intents():
    with open(slu_file_path, "r", encoding="utf-8") as file:
        nludata = json.load(file)

    # Intents
    for intent in nludata["intents"]:
        aint = copy.deepcopy(alexaintent)
        aint["name"] = intent

        allslots = set()
        for sample in nludata["intents"][intent]:
            if intent == "tas-settimer":
                # Use builtin slots for durations
                sample = re.sub(r"\[.*", "{duration}", sample)
                allslots = set(["duration"])
            elif intent == "tas-setalarm":
                # Use builtin slots for times
                sample = re.sub(r"\[.*", "{time}", sample)
                allslots = set(["time"])
            else:
                slots = re.findall(slotpattern, sample)
                allslots = allslots.union(set(slots))
                sample = re.sub(slotpattern, r"{\1}", sample)
                sample = re.sub(r"[a-z0-9_]+\?", "", sample)

            # Expand alternatives
            if "|" in sample:
                samples = inject_alternatives(sample)
                aint["samples"].extend(samples)
            else:
                aint["samples"].append(sample)

        # Drop duplicates
        aint["samples"] = sorted(set(aint["samples"]))

        for slot in sorted(allslots):
            slot = re.sub(r"[a-z0-9_]+\?", "", slot)

            if slot in ["tas-number1", "tas-number2", "tas-amount"]:
                aint["slots"].append({"name": slot, "type": "AMAZON.NUMBER"})
            elif slot in ["duration", "time"]:
                aint["slots"].append({"name": slot, "type": "AMAZON." + slot.upper()})
            else:
                stype = slot
                if slot.startswith("tas-unit"):
                    stype = "tas-unit"
                aint["slots"].append({"name": slot, "type": stype})

        alexadata["interactionModel"]["languageModel"]["intents"].append(aint)

    # Lookups
    for lookup in nludata["lookups"]:
        if lookup not in ["tas-unit", "tas-op"]:
            continue

        alook = copy.deepcopy(alexatype)
        alook["name"] = lookup

        for sample in nludata["lookups"][lookup]:
            if "->" in sample:
                syns, val = sample.split("->")
                syns = syns[1:-1].split("|")
                sample = {"name": {"value": val, "synonyms": syns}}
            else:
                sample = {"name": {"value": sample}}
            alook["values"].append(sample)

        alexadata["interactionModel"]["languageModel"]["types"].append(alook)

    alexastr = json.dumps(alexadata, indent=2)
    alexastr = alexastr.replace("tas-", "")
    alexastr = alexastr.replace("number1", "number_a")
    alexastr = alexastr.replace("number2", "number_b")
    alexastr = alexastr.replace("unit1", "unit_a")
    alexastr = alexastr.replace("unit2", "unit_b")
    with open(outpath, "w+", encoding="utf-8") as file:
        file.write(alexastr)


# ==================================================================================================


def main():
    if not os.path.isdir(os.path.dirname(outpath)):
        os.makedirs(os.path.dirname(outpath))

    convert_intents()


# ==================================================================================================

if __name__ == "__main__":
    main()
