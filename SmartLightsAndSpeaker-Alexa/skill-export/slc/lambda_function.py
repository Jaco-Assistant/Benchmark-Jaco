# -*- coding: utf-8 -*-

# This sample demonstrates handling intents from an Alexa skill using the Alexa Skills Kit SDK for Python.
# Please visit https://alexa.design/cookbook for additional examples on implementing slots, dialog management,
# session persistence, api calls, and more.
# This sample is built using the handler classes approach in skill builder.
import json
import logging
from typing import Optional, Union

import ask_sdk_core.utils as ask_utils
from ask_sdk_core.dispatch_components import (
    AbstractExceptionHandler,
    AbstractRequestHandler,
)
from ask_sdk_core.handler_input import HandlerInput
from ask_sdk_core.skill_builder import SkillBuilder
from ask_sdk_model import Response
from ask_sdk_model.ui import SimpleCard

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
AnyStr = Union[str, bytes]


def get_canonical_slot_value(handler_input, slot_name):
    # type: (HandlerInput, str) -> Optional[AnyStr]
    """Alexa does return the spoken slot value instead of the canonical value and has no builtin
    function to get the canonical value that is mostly required."""

    slot = ask_utils.request_util.get_slot(
        handler_input=handler_input, slot_name=slot_name
    )

    try:
        value = slot.resolutions.resolutions_per_authority[0].values[0].value.name
        return value
    except AttributeError:
        if slot is not None:
            return slot.value
        else:
            return None


class LaunchRequestHandler(AbstractRequestHandler):
    """Handler for Skill Launch."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool

        return ask_utils.is_request_type("LaunchRequest")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Welcome, you can ask me to control the lights."

        card_title = "started"
        card_text = "SmartLights-Alexa"

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .set_card(SimpleCard(card_title, card_text))
            .response
        )


class decreasebrightnessIntentHandler(AbstractRequestHandler):
    """Handler for decreasebrightness Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("decreasebrightness")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Ok"

        intent = {
            "intent": "decreasebrightness",
            "slots": {
                "house_room_unique": get_canonical_slot_value(
                    handler_input, "house_room_unique"
                ),
            },
        }

        card_title = "decreasebrightness"
        card_text = json.dumps(intent)

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .set_card(SimpleCard(card_title, card_text))
            .response
        )


class increasebrightnessIntentHandler(AbstractRequestHandler):
    """Handler for increasebrightness Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("increasebrightness")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Ok"

        intent = {
            "intent": "increasebrightness",
            "slots": {
                "house_room_unique": get_canonical_slot_value(
                    handler_input, "house_room_unique"
                ),
            },
        }

        card_title = "increasebrightness"
        card_text = json.dumps(intent)

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .set_card(SimpleCard(card_title, card_text))
            .response
        )


class setlightbrightnessIntentHandler(AbstractRequestHandler):
    """Handler for setlightbrightness Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("setlightbrightness")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Ok"

        intent = {
            "intent": "setlightbrightness",
            "slots": {
                "house_room_unique": get_canonical_slot_value(
                    handler_input, "house_room_unique"
                ),
                "number": get_canonical_slot_value(handler_input, "number"),
            },
        }

        card_title = "setlightbrightness"
        card_text = json.dumps(intent)

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .set_card(SimpleCard(card_title, card_text))
            .response
        )


class setlightcolorIntentHandler(AbstractRequestHandler):
    """Handler for setlightcolor Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("setlightcolor")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Ok"

        intent = {
            "intent": "setlightcolor",
            "slots": {
                "house_room_unique": get_canonical_slot_value(
                    handler_input, "house_room_unique"
                ),
                "color": get_canonical_slot_value(handler_input, "color"),
            },
        }

        card_title = "setlightcolor"
        card_text = json.dumps(intent)

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .set_card(SimpleCard(card_title, card_text))
            .response
        )


class switchlightoffIntentHandler(AbstractRequestHandler):
    """Handler for switchlightoff Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("switchlightoff")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Ok"

        intent = {
            "intent": "switchlightoff",
            "slots": {
                "house_room_unique": get_canonical_slot_value(
                    handler_input, "house_room_unique"
                ),
            },
        }

        card_title = "switchlightoff"
        card_text = json.dumps(intent)

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .set_card(SimpleCard(card_title, card_text))
            .response
        )


class switchlightonIntentHandler(AbstractRequestHandler):
    """Handler for switchlighton Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("switchlighton")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Ok"

        intent = {
            "intent": "switchlighton",
            "slots": {
                "house_room_unique": get_canonical_slot_value(
                    handler_input, "house_room_unique"
                ),
            },
        }

        card_title = "switchlighton"
        card_text = json.dumps(intent)

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .set_card(SimpleCard(card_title, card_text))
            .response
        )


class HelpIntentHandler(AbstractRequestHandler):
    """Handler for Help Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("AMAZON.HelpIntent")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Ask me to control the lights"

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .response
        )


class CancelOrStopIntentHandler(AbstractRequestHandler):
    """Single handler for Cancel and Stop Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("AMAZON.CancelIntent")(
            handler_input
        ) or ask_utils.is_intent_name("AMAZON.StopIntent")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Goodbye!"

        return handler_input.response_builder.speak(speak_output).response


class SessionEndedRequestHandler(AbstractRequestHandler):
    """Handler for Session End."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_request_type("SessionEndedRequest")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response

        # Any cleanup logic goes here.

        return handler_input.response_builder.response


class IntentReflectorHandler(AbstractRequestHandler):
    """The intent reflector is used for interaction model testing and debugging.
    It will simply repeat the intent the user said. You can create custom handlers
    for your intents by defining them above, then also adding them to the request
    handler chain below.
    """

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_request_type("IntentRequest")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        intent_name = ask_utils.get_intent_name(handler_input)
        speak_output = "You just triggered " + intent_name + "."

        return (
            handler_input.response_builder.speak(speak_output)
            # .ask("add a reprompt if you want to keep the session open for the user to respond")
            .response
        )


class CatchAllExceptionHandler(AbstractExceptionHandler):
    """Generic error handling to capture any syntax or routing errors. If you receive an error
    stating the request handler chain is not found, you have not implemented a handler for
    the intent being invoked or included it in the skill builder below.
    """

    def can_handle(self, handler_input, exception):
        # type: (HandlerInput, Exception) -> bool
        return True

    def handle(self, handler_input, exception):
        # type: (HandlerInput, Exception) -> Response
        logger.error(exception, exc_info=True)

        speak_output = "Sorry, I had trouble doing what you asked. Please try again."

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .response
        )


# The SkillBuilder object acts as the entry point for your skill, routing all request and response
# payloads to the handlers above. Make sure any new handlers or interceptors you've
# defined are included below. The order matters - they're processed top to bottom.


sb = SkillBuilder()

sb.add_request_handler(LaunchRequestHandler())
sb.add_request_handler(decreasebrightnessIntentHandler())
sb.add_request_handler(increasebrightnessIntentHandler())
sb.add_request_handler(setlightbrightnessIntentHandler())
sb.add_request_handler(setlightcolorIntentHandler())
sb.add_request_handler(switchlightoffIntentHandler())
sb.add_request_handler(switchlightonIntentHandler())
sb.add_request_handler(HelpIntentHandler())
sb.add_request_handler(CancelOrStopIntentHandler())
sb.add_request_handler(SessionEndedRequestHandler())
sb.add_request_handler(
    IntentReflectorHandler()
)  # make sure IntentReflectorHandler is last so it doesn't override your custom intent handlers

sb.add_exception_handler(CatchAllExceptionHandler())

lambda_handler = sb.lambda_handler()
