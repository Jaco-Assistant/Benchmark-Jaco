# -*- coding: utf-8 -*-

# This sample demonstrates handling intents from an Alexa skill using the Alexa Skills Kit SDK for Python.
# Please visit https://alexa.design/cookbook for additional examples on implementing slots, dialog management,
# session persistence, api calls, and more.
# This sample is built using the handler classes approach in skill builder.
import json
import logging
from typing import Optional, Union

import ask_sdk_core.utils as ask_utils
from ask_sdk_core.dispatch_components import (
    AbstractExceptionHandler,
    AbstractRequestHandler,
)
from ask_sdk_core.handler_input import HandlerInput
from ask_sdk_core.skill_builder import SkillBuilder
from ask_sdk_model import Response
from ask_sdk_model.ui import SimpleCard

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
AnyStr = Union[str, bytes]


def get_canonical_slot_value(handler_input, slot_name):
    # type: (HandlerInput, str) -> Optional[AnyStr]
    """Alexa does return the spoken slot value instead of the canonical value and has no builtin
    function to get the canonical value that is mostly required."""

    slot = ask_utils.request_util.get_slot(
        handler_input=handler_input, slot_name=slot_name
    )

    try:
        value = slot.resolutions.resolutions_per_authority[0].values[0].value.name
        return value
    except AttributeError:
        if slot is not None:
            return slot.value
        else:
            return None


class LaunchRequestHandler(AbstractRequestHandler):
    """Handler for Skill Launch."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool

        return ask_utils.is_request_type("LaunchRequest")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Welcome, you can ask me for songs to play."

        card_title = "started"
        card_text = "SmartSpeaker-Alexa"

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .set_card(SimpleCard(card_title, card_text))
            .response
        )


class getinfosIntentHandler(AbstractRequestHandler):
    """Handler for getinfos Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("getinfos")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "No"

        intent = {
            "intent": "getinfos",
            "slots": {},
        }

        card_title = "getinfos"
        card_text = json.dumps(intent)

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .set_card(SimpleCard(card_title, card_text))
            .response
        )


class nextsongIntentHandler(AbstractRequestHandler):
    """Handler for nextsong Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("nextsong")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "No"

        intent = {
            "intent": "nextsong",
            "slots": {},
        }

        card_title = "nextsong"
        card_text = json.dumps(intent)

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .set_card(SimpleCard(card_title, card_text))
            .response
        )


class playmusicIntentHandler(AbstractRequestHandler):
    """Handler for playmusic Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("playmusic")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Ok"

        intent = {
            "intent": "playmusic",
            "slots": {
                "musictrack": get_canonical_slot_value(handler_input, "musictrack"),
                "musicartist": get_canonical_slot_value(handler_input, "musicartist"),
                "musicalbum": get_canonical_slot_value(handler_input, "musicalbum"),
                "playlistmodelectureen": get_canonical_slot_value(
                    handler_input, "playlistmodelectureen"
                ),
                "playlistnameen": get_canonical_slot_value(
                    handler_input, "playlistnameen"
                ),
                "playlistmodelecturefr": get_canonical_slot_value(
                    handler_input, "playlistmodelecturefr"
                ),
                "playlistnamefr": get_canonical_slot_value(
                    handler_input, "playlistnamefr"
                ),
            },
        }

        card_title = "playmusic"
        card_text = json.dumps(intent)

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .set_card(SimpleCard(card_title, card_text))
            .response
        )


class previoussongIntentHandler(AbstractRequestHandler):
    """Handler for previoussong Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("previoussong")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "No"

        intent = {
            "intent": "previoussong",
            "slots": {},
        }

        card_title = "previoussong"
        card_text = json.dumps(intent)

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .set_card(SimpleCard(card_title, card_text))
            .response
        )


class resumemusicIntentHandler(AbstractRequestHandler):
    """Handler for resumemusic Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("resumemusic")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "No"

        intent = {
            "intent": "resumemusic",
            "slots": {},
        }

        card_title = "resumemusic"
        card_text = json.dumps(intent)

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .set_card(SimpleCard(card_title, card_text))
            .response
        )


class speakerinterruptIntentHandler(AbstractRequestHandler):
    """Handler for speakerinterrupt Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("speakerinterrupt")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "No"

        intent = {
            "intent": "speakerinterrupt",
            "slots": {},
        }

        card_title = "speakerinterrupt"
        card_text = json.dumps(intent)

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .set_card(SimpleCard(card_title, card_text))
            .response
        )


class volumedownIntentHandler(AbstractRequestHandler):
    """Handler for volumedown Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("volumedown")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "No"

        intent = {
            "intent": "volumedown",
            "slots": {},
        }

        card_title = "volumedown"
        card_text = json.dumps(intent)

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .set_card(SimpleCard(card_title, card_text))
            .response
        )


class volumesetIntentHandler(AbstractRequestHandler):
    """Handler for volumeset Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("volumeset")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "No"

        intent = {
            "intent": "volumeset",
            "slots": {},
        }

        card_title = "volumeset"
        card_text = json.dumps(intent)

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .set_card(SimpleCard(card_title, card_text))
            .response
        )


class volumeshiftIntentHandler(AbstractRequestHandler):
    """Handler for volumeshift Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("volumeshift")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "No"

        intent = {
            "intent": "volumeshift",
            "slots": {},
        }

        card_title = "volumeshift"
        card_text = json.dumps(intent)

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .set_card(SimpleCard(card_title, card_text))
            .response
        )


class volumeupIntentHandler(AbstractRequestHandler):
    """Handler for volumeup Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("volumeup")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "No"

        intent = {
            "intent": "volumeup",
            "slots": {},
        }

        card_title = "volumeup"
        card_text = json.dumps(intent)

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .set_card(SimpleCard(card_title, card_text))
            .response
        )


class HelpIntentHandler(AbstractRequestHandler):
    """Handler for Help Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("AMAZON.HelpIntent")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Ask me to play a song"

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .response
        )


class CancelOrStopIntentHandler(AbstractRequestHandler):
    """Single handler for Cancel and Stop Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("AMAZON.CancelIntent")(
            handler_input
        ) or ask_utils.is_intent_name("AMAZON.StopIntent")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Goodbye!"

        return handler_input.response_builder.speak(speak_output).response


class SessionEndedRequestHandler(AbstractRequestHandler):
    """Handler for Session End."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_request_type("SessionEndedRequest")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response

        # Any cleanup logic goes here.

        return handler_input.response_builder.response


class IntentReflectorHandler(AbstractRequestHandler):
    """The intent reflector is used for interaction model testing and debugging.
    It will simply repeat the intent the user said. You can create custom handlers
    for your intents by defining them above, then also adding them to the request
    handler chain below.
    """

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_request_type("IntentRequest")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        intent_name = ask_utils.get_intent_name(handler_input)
        speak_output = "You just triggered " + intent_name + "."

        return (
            handler_input.response_builder.speak(speak_output)
            # .ask("add a reprompt if you want to keep the session open for the user to respond")
            .response
        )


class CatchAllExceptionHandler(AbstractExceptionHandler):
    """Generic error handling to capture any syntax or routing errors. If you receive an error
    stating the request handler chain is not found, you have not implemented a handler for
    the intent being invoked or included it in the skill builder below.
    """

    def can_handle(self, handler_input, exception):
        # type: (HandlerInput, Exception) -> bool
        return True

    def handle(self, handler_input, exception):
        # type: (HandlerInput, Exception) -> Response
        logger.error(exception, exc_info=True)

        speak_output = "Sorry, I had trouble doing what you asked. Please try again."

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .response
        )


# The SkillBuilder object acts as the entry point for your skill, routing all request and response
# payloads to the handlers above. Make sure any new handlers or interceptors you've
# defined are included below. The order matters - they're processed top to bottom.


sb = SkillBuilder()

sb.add_request_handler(LaunchRequestHandler())
sb.add_request_handler(getinfosIntentHandler())
sb.add_request_handler(nextsongIntentHandler())
sb.add_request_handler(playmusicIntentHandler())
sb.add_request_handler(previoussongIntentHandler())
sb.add_request_handler(resumemusicIntentHandler())
sb.add_request_handler(speakerinterruptIntentHandler())
sb.add_request_handler(volumedownIntentHandler())
sb.add_request_handler(volumesetIntentHandler())
sb.add_request_handler(volumeupIntentHandler())
sb.add_request_handler(volumeshiftIntentHandler())
sb.add_request_handler(HelpIntentHandler())
sb.add_request_handler(CancelOrStopIntentHandler())
sb.add_request_handler(SessionEndedRequestHandler())
sb.add_request_handler(
    IntentReflectorHandler()
)  # make sure IntentReflectorHandler is last so it doesn't override your custom intent handlers

sb.add_exception_handler(CatchAllExceptionHandler())

lambda_handler = sb.lambda_handler()
