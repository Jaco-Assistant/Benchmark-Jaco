# Benchmark SmartLightsAndSpeaker Alexa

Tools to benchmark [Alexa](https://developer.amazon.com/en-US/alexa), a widely used voice assistant from Amazon. \
NOT WORKING ANYMORE

<br>

### Test Alexa

Steps to rebuild everything:

- Generate the SLU dataset for Jaco

- Convert the intents: \
  (To run the French SmartSpeaker benchmark, rebuild the dialog file with the `convert_intents.py` script first, because you have to enable the full French alphabet)

  ```bash
  sed 's/- //g' SmartLightsAndSpeaker-Jaco/dialog/nlu_ssc/en/nlu.md > SmartLightsAndSpeaker-Alexa/tmp1.txt
  sed -r 's/\[.*\]\(([a-zA-Z_]*)\.txt\)/{\1}/g' SmartLightsAndSpeaker-Alexa/tmp1.txt > SmartLightsAndSpeaker-Alexa/tmp2.txt

  # Remove lines like "play {entity} 's song", because Alexa can't work with them
  sed "s/.*} '.*//g" SmartLightsAndSpeaker-Alexa/tmp2.txt > SmartLightsAndSpeaker-Alexa/tmp3.txt

  # Replace punctuation characters
  sed -r "s/\.|\!|\?|,//g" SmartLightsAndSpeaker-Alexa/tmp3.txt > SmartLightsAndSpeaker-Alexa/tmp4.txt
  ```

- Change the localization of your _Amazon_ account:

  - Set your address to somewhere in the USA
  - Change your shop to _"amazon.com"_ by following this [link](https://www.amazon.com/gp/help/customer/display.html?nodeId=201248840). \
    (there was a text _"You are currently linked to shop on Amazon.de"_ (it was no link) below, click on it, then will show another link where you can transfer to the new region)

- Create a new skill for Alexa: [Tutorial](https://developer.amazon.com/en-US/docs/alexa/custom-skills/create-custom-skill-from-quick-start-template.html) \
  Select the `custom` model, `python` skill hosting and `start from scratch` template.

- Create intents and slot types using bulk edit option (use some online duplicate remover for the intents),
  then match slots to slot types. Save and build the skill.

- Copy the exported lambda function script in your skill to send the intents back to our code and deploy it.

- Test the skill with textual inputs in the console \
  (don't forget to activate the skill with its invocation name)

- Build the container:

  ```bash
  docker build --progress=plain -t smartlightsandspeaker_alexa - < SmartLightsAndSpeaker-Alexa/Containerfile
  ```

- Create a new virtual Alexa device: \
  (Following `aws-client` setup [here](https://github.com/richtier/alexa-voice-service-client#authentication))

  - Follow the link to AWS product page (https://developer.amazon.com/alexa/console/avs/products), optionally click _get-started_, add a new product
  - Fill forms and setup security profile. Activate display cards with text (Product-Details -> Capabilities). Click Update.

  - Copy `alexa-credentials.template.json` to `alexa-credentials.json` and fill in your credentials
  - Get `client-id` and `secret` from the product's detail page

  - Get the `request_token`. Use your product-id (Product-Details -> Information) for the `device-type-id` flag.

    ```bash
    docker run --network host --rm \
      -it smartlightsandspeaker_alexa

    python3 -m alexa_client.refreshtoken.serve \
      --device-type-id="enter-device-type-id-here" \
      --client-id="enter-client-id-here" \
      --client-secret="enter-client-secret-here"

    # Open the printed link to register this device
    ```

- Create a launch sentence for Alexa (you can use Jaco's _text-to-speech_ module for this),
  then convert it to 16kHz `alexa_launch_en.wav`

- You can view the transcripts and returned cards at: https://alexa.amazon.de/spa/index.html#cards \
   History of speech inputs: https://alexa.amazon.de/spa/index.html#cards -> Settings -> History -> Manage Your Content and Devices -> Privacy Settings -> Alexa -> Review Voice History

- Run the benchmark:

  ```bash
  docker run --network host --rm --device /dev/snd --env ALSA_CARD=PCH \
    --volume `pwd`/SmartLightsAndSpeaker-Alexa/:/Benchmark-Jaco/SmartLightsAndSpeaker-Alexa/ \
    --volume `pwd`/SmartLightsAndSpeaker-Jaco/:/Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/ \
    --volume `pwd`/../alexa-voice-service-client/alexa_client/alexa_client/:/usr/local/lib/python3.6/dist-packages/alexa_client/alexa_client/ \
    --volume `pwd`/datasets/:/Benchmark-Jaco/datasets/ \
    -it smartlightsandspeaker_alexa

  python3 /Benchmark-Jaco/SmartLightsAndSpeaker-Alexa/run_benchmark.py \
    --wav_files_txt /Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/dialog/nlu_ssc/test_wavs_en.txt --reduced_dataset 10
  ```

- For the SmartLights benchmark, you have to rebuild the skill for each of the five folds.
  Only replace the intent examples, build and test the skill again and wait a few minutes before starting the next run.

_Developer's Note: Nooooo. Alexa spoke German again:/
To solve it this time, I had to move into two new homes in LasVegas and Paris.
Of course, I also had to change Amazon's language to French and now I have no idea what's written on the buttons I'm clicking..._

Accuracy SmartLights-Close dataset: 0.7922 \
Benchmark Runtime: 5x 0:19h

Accuracy SmartSpeaker-Close dataset English (custom/builtin slots): 0.4546 / 0.2598 \
Accuracy SmartSpeaker-Close dataset French: 0.8892 / 0.3289 \
Benchmark Runtime (en/fr): 1:20h / 0:50h

_Developer's Note: I have no idea why Alexa's performance is so good in French but so bad in English.
The only thing I noticed was that the musician names in English often had spelling errors, because Alexa did use the correct spelling instead of the artificial names,
for example, "Bones Mc" instead of "Bonez Mc". I didn't notice this in French that often._
