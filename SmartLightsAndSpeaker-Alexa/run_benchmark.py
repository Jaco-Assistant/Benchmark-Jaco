import argparse
import copy
import json
import os
import sys
import time

import alexa_client
import hyper
import num2words
import requests
import tqdm
from alexa_client.alexa_client import constants

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
sys.path.insert(1, file_path + "../SmartLightsAndSpeaker-Jaco/")
import convert_intents  # noqa: E402 pylint: disable=wrong-import-position

sys.path.insert(1, "/spoken-language-understanding-research-datasets/")
from dataset_handler import (  # noqa: E402 pylint: disable=wrong-import-position
    CrossValDataset,
    TrainTestDataset,
)

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
language = "en"
# datasetname = "smartlights"
datasetname = "smartspeaker"

if datasetname == "smartlights":
    data_dir = file_path + "../datasets/snips_slu/smart-lights-en-close-field/"
elif datasetname == "smartspeaker":
    data_dir = file_path + "../datasets/snips_slu/smart-speaker-{}-close-field/"
    data_dir = data_dir.format(language)
else:
    raise ValueError

with open(file_path + "alexa-credentials.json", "r", encoding="utf-8") as acfile:
    credentials = json.load(acfile)

dialog_request_id = None
alexa = None
reduced_dataset = 0

synonym_replacers = {
    "pink": "red",
    "lounge": "living room",
    "bed room": "bedroom",
    "toilet": "toilets",
    "loo": "toilets",
    "facilities": "toilets",
    "kids room": "kids bedroom",
    "cella": "cellar",
    "entire house": "house",
    "entire flat": "flat",
    "entire appartment": "appartment",
    "apartment": "appartment",
    "child's bedroom": "child bedroom",
    "children bedroom": "child bedroom",
    "children's bedroom": "child bedroom",
}

extra_synonym_replacers = {
    "toilets room": "toilets",
    "toilet room": "toilets",
    "cublicle": "cubicle",
}

# ==================================================================================================


def connect_alexa():
    """Create and connect a new alexa client"""

    if language == "fr":
        avs_language = "fr-FR"
        base_url = constants.BASE_URL_EUROPE
    else:
        avs_language = "en-US"
        base_url = constants.BASE_URL_NORTH_AMERICA

    client = alexa_client.AlexaClient(
        client_id=credentials["client_id"],
        secret=credentials["secret"],
        refresh_token=credentials["refresh_token"],
        language=avs_language,
        base_url=base_url,
    )
    client.connect()
    print(" Established connection to Alexa")
    return client


# ==================================================================================================


def start_new_dialog():
    """Start dialog by sending an audio file saying 'Alexa, start benchmark experiment'"""

    request_id = alexa_client.alexa_client.helpers.generate_unique_id()
    if language == "fr":
        launch_wav = "alexa_launch_fr.wav"
    else:
        launch_wav = "alexa_launch_en.wav"

    with open(file_path + launch_wav, "rb") as f:
        alexa.send_audio_file(f, dialog_request_id=dialog_request_id)
    return request_id


# ==================================================================================================


def seconds_to_hours(secs):
    secs = int(secs)
    m, s = divmod(secs, 60)
    h, m = divmod(m, 60)
    t = "{:d}:{:02d}:{:02d}".format(h, m, s)
    return t


# ==================================================================================================


def get_intent(audio_path):
    """Send audio file to Alexa and extract the intent from the returned SimpleCard
    If Alexa doesn't respond, the request is send two times again"""

    response = None
    intent = None

    # Wait some time that the requests are not sent too fast
    time.sleep(1)

    for _ in range(3):
        with open(audio_path, "rb") as f:
            response = alexa.send_audio_file(f, dialog_request_id=dialog_request_id)

        if response is not None:
            for res in response:
                if res.name == "RenderTemplate":
                    if (
                        "payload" in res.directive
                        and "textField" in res.directive["payload"]
                    ):
                        intent = res.directive["payload"]["textField"]
            break

        # Wait some time before sending the unanswered request again
        time.sleep(3)

    if response is None:
        print(" No response for file:", audio_path)
        return None, ""

    if intent is None:
        print(" No intent for file:", audio_path)
        return None, ""

    try:
        intent = json.loads(intent)
        # Alexa can't return the text of a voice query, so just return an empty string
        return intent, ""
    except json.decoder.JSONDecodeError:
        print(" Invalid json string:", intent, audio_path)
        return None, ""


# ==================================================================================================


def convert_intent(intent):
    """Convert intent format"""

    if intent is None or intent["intent"] is None or "slots" not in intent:
        return None

    convint = copy.deepcopy(intent)

    # Remove empty slots
    for s in intent["slots"]:
        if intent["slots"][s] is None:
            del convint["slots"][s]

    return convint


# ==================================================================================================


def compare_intents(intent, label):
    if intent is None or "intent" not in intent:
        return False, "intent missing"

    if label["intent"].lower() != intent["intent"].lower():
        return False, "intent wrong"

    for slot in label["slots"]:
        entity_name = slot["entity"].lower()
        if entity_name.startswith("snips/"):
            entity_name = entity_name.replace("snips/", "")

        if entity_name not in intent["slots"]:
            return False, "slot missing: {}".format(slot)

        label_value = slot["text"].strip()
        detection_value = intent["slots"][entity_name].strip()

        if entity_name == "number":
            detection_value = num2words.num2words(detection_value, lang=language)
            detection_value = detection_value.replace("-", " ")

        # Remove dashes (some numbers/artists in the labels have dashes, but most not)
        label_value = label_value.replace("-", " ")
        detection_value = detection_value.replace("-", " ")

        # Remove special sentence signs
        if datasetname == "smartspeaker":
            label_value = convert_intents.convert_sample(  # pylint: disable=no-member
                label_value
            )
            detection_value = (
                convert_intents.convert_sample(  # pylint: disable=no-member
                    detection_value
                )
            )
        else:
            label_value = label_value.replace("?", "")
        label_value = label_value.replace(".", "")
        detection_value = detection_value.replace(".", "")

        if datasetname == "smartlights":
            # Replace synonyms, this is not handled correctly in the labels dataset
            for sr in synonym_replacers:
                if label_value == sr:
                    label_value = synonym_replacers[sr]
            # Replace wrong rooms that are not handled correctly in the labels dataset
            for sr in extra_synonym_replacers:
                if label_value == sr:
                    label_value = extra_synonym_replacers[sr]
                if detection_value == sr:
                    detection_value = extra_synonym_replacers[sr]

        if detection_value != label_value:
            return False, "value wrong: {} - {}".format(detection_value, label_value)

    return True, ""


# ==================================================================================================


def run_benchmark(dataset, wav_files):
    global dialog_request_id, alexa

    print("\nRunning benchmark ...")
    start_time = time.time()

    results = []
    for file_name in tqdm.tqdm(wav_files):
        try:
            intent, _ = get_intent(file_name)
            if intent is None:
                # If no response was returned, restart the dialog, because Alexa seems to drop it
                # This may be the same case when an invalid json file is returned
                dialog_request_id = start_new_dialog()

        except (
            requests.exceptions.HTTPError,
            hyper.http20.exceptions.StreamResetError,
            hyper.http20.exceptions.ConnectionError,
            ConnectionResetError,
        ):
            # The connection was lost, try to create a new client
            # This may result in another connection error in the avs-client thread, ignore this one
            alexa = connect_alexa()
            intent, _ = get_intent(file_name)

        convint = convert_intent(intent)
        wav_name = os.path.basename(file_name)
        label = dataset.get_labels_from_wav(wav_name)

        same, error_reason = compare_intents(convint, label)
        results.append(same)

        if not same:
            # Print wrong detections for debugging
            print("\n")
            print("ERROR", error_reason)
            print(file_name)
            print("Detected intent:", intent)
            print(convint)
            print(label)

        if len(results) % 20 == 0:
            # Start new dialog every 20 requests,
            # attempt to fix empty intent responses after some time
            _ = start_new_dialog()
            dialog_request_id = start_new_dialog()

        if len(results) % 50 == 0:
            status = " Numbers of correct files: {}/{}"
            print(status.format(sum((1 for r in results if r)), len(results)))

    conv_dur = time.time() - start_time
    msg = " The benchmark took {}"
    print(msg.format(seconds_to_hours(conv_dur)))

    # Count wrong and correct instead of using list length for debugging purposes
    # (allows to end the loop midways)
    num_correct = sum((1 for r in results if r))
    acc = num_correct / len(results)
    msg = " Correctly converted {}/{}. The accuracy is {:.4f}"
    print(msg.format(num_correct, len(results), acc))


# ==================================================================================================


def main():
    global alexa, dialog_request_id

    parser = argparse.ArgumentParser(description="Run benchmark with Alexa")
    parser.add_argument(
        "--wav_files_txt",
        required=True,
        help="Path to text file with list of wav files to use",
    )
    parser.add_argument(
        "--reduced_dataset",
        default=0,
        type=int,
        help="Test with fewer files",
    )
    args = parser.parse_args()

    alexa = connect_alexa()
    dialog_request_id = start_new_dialog()

    # Load dataset
    if "slc" in args.wav_files_txt:
        dataset = CrossValDataset.from_dir(data_dir)
    else:
        dataset = TrainTestDataset.from_dir(data_dir)
    with open(args.wav_files_txt, "r", encoding="utf-8") as file:
        wav_files = file.readlines()
        wav_files = [w.strip() for w in wav_files if w.strip() != ""]
    wav_files = sorted(wav_files)

    for w in list(wav_files):
        wav_name = os.path.basename(w)
        label = dataset.get_labels_from_wav(wav_name)
        if label is None:
            print("WARNING: Dropping {} because it has no labels".format(wav_name))
            wav_files.remove(w)

    if 0 < args.reduced_dataset <= len(wav_files):
        wav_files = wav_files[0 : args.reduced_dataset]

    if language == "fr":
        # Load full french alphabet instead of Jaco's cross-word puzzle substitutions
        convert_intents.use_full_french_alphabet = True
    # Set correct language for char replacements
    convert_intents.language = language
    convert_intents.load_char_replacers()  # pylint: disable=no-member

    run_benchmark(dataset, wav_files)

    # There is still some thread running, maybe from avs-client library
    sys.exit()


# ==================================================================================================

if __name__ == "__main__":
    main()
