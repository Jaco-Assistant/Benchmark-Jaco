import argparse
import json
import os
import sys
import time

import alexa_client
import hyper
import pandas as pd
import requests
import tqdm
from alexa_client.alexa_client import constants

# ==================================================================================================

filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
data_dir = filepath + "../datasets/fluent_speech_commands/"
label_csv = data_dir + "data/test_data.csv"

with open(filepath + "alexa-credentials.json", encoding="utf-8") as acfile:
    credentials = json.load(acfile)

dialog_request_id = None
alexa = None
reduced_dataset = 0


# ==================================================================================================


def connect_alexa():
    """Create and connect a new alexa client"""

    client = alexa_client.AlexaClient(
        client_id=credentials["client_id"],
        secret=credentials["secret"],
        refresh_token=credentials["refresh_token"],
        language="en-US",
        base_url=constants.BASE_URL_NORTH_AMERICA,
    )
    client.connect()
    print(" Established connection to Alexa")
    return client


# ==================================================================================================


def start_new_dialog():
    """Start dialog by sending an audio file saying 'Alexa, start benchmark experiment'"""

    request_id = alexa_client.alexa_client.helpers.generate_unique_id()
    with open(filepath + "alexa_launch_en.wav", "rb") as f:
        alexa.send_audio_file(f, dialog_request_id=dialog_request_id)
    return request_id


# ==================================================================================================


def seconds_to_hours(secs):
    secs = int(secs)
    m, s = divmod(secs, 60)
    h, m = divmod(m, 60)
    t = "{:d}:{:02d}:{:02d}".format(h, m, s)
    return t


# ==================================================================================================


def load_labels(path: str) -> list:
    dataset = pd.read_csv(path, sep=",", keep_default_na=False)
    dataset = dataset[["path", "transcription", "action", "object", "location"]]
    dataset = dataset.rename(columns={"transcription": "text"})
    dataset = dataset.to_dict(orient="records")
    for entry in dataset:
        entry["path"] = data_dir + entry["path"]
    return dataset


# ==================================================================================================


def get_intent(audio_path):
    """Send audio file to Alexa and extract the intent from the returned SimpleCard
    If Alexa doesn't respond, the request is send two times again"""

    response = None
    intent = None

    # Wait some time that the requests are not sent too fast
    time.sleep(1)

    for _ in range(3):
        with open(audio_path, "rb") as f:
            response = alexa.send_audio_file(f, dialog_request_id=dialog_request_id)

        if response is not None:
            for res in response:
                if res.name == "RenderTemplate":
                    if (
                        "payload" in res.directive
                        and "textField" in res.directive["payload"]
                    ):
                        intent = res.directive["payload"]["textField"]
            break

        # Wait some time before sending the unanswered request again
        time.sleep(3)

    if response is None:
        print(" No response for file:", audio_path)
        return None, ""

    if intent is None:
        print(" No intent for file:", audio_path)
        return None, ""

    try:
        intent = json.loads(intent)
        # Alexa can't return the text of a voice query, so just return an empty string
        return intent, ""
    except json.decoder.JSONDecodeError:
        print(" Invalid json string:", intent, audio_path)
        return None, ""


# ==================================================================================================


def convert_intent(intent):
    """Convert intent format"""

    if intent is None or intent["intent"] is None or "slots" not in intent:
        return None

    convint = {
        "intent": intent["intent"],
        "entities": {},
    }

    for e in intent["slots"]:
        if intent["slots"][e] is not None:
            convint["entities"][e] = intent["slots"][e]

    return convint


# ==================================================================================================


def compare_intents(intent, label):
    if intent is None:
        return False, "intent missing"

    intentname = intent["intent"]
    obj, loc = "none", "none"

    if "_" in intentname:
        if intentname in ["bring", "change_language"]:
            action = intentname.replace("_", " ")
        else:
            action, obj = intentname.split("_")
    else:
        action = intentname

    if action == "bring" and "object" in intent["entities"]:
        obj = intent["entities"]["object"]
    elif action == "change language" and "language" in intent["entities"]:
        obj = intent["entities"]["language"]

    if "location" in intent["entities"]:
        loc = intent["entities"]["location"]

    if action != label["action"].lower():
        return False, "action wrong"

    if obj != label["object"].lower():
        return False, "object wrong"

    if loc != label["location"].lower():
        return False, "location wrong"

    return True, ""


# ==================================================================================================


def run_benchmark(labels: dict):
    global dialog_request_id, alexa

    print("\nRunning benchmark ...")
    start_time = time.time()

    results = []
    for label in tqdm.tqdm(labels):
        try:
            intent, _ = get_intent(label["path"])
            if intent is None:
                # If no response was returned, restart the dialog, because Alexa seems to drop it
                # This may be the same case when an invalid json file is returned
                dialog_request_id = start_new_dialog()

        except (
            requests.exceptions.HTTPError,
            hyper.http20.exceptions.StreamResetError,
            hyper.http20.exceptions.ConnectionError,
            ConnectionResetError,
        ):
            # The connection was lost, try to create a new client
            # This may result in another connection error in the avs-client thread, ignore this one
            alexa = connect_alexa()
            intent, _ = get_intent(label["path"])

        convint = convert_intent(intent)
        same, error_reason = compare_intents(convint, label)
        results.append(same)

        if not same:
            # Print wrong predictions for debugging
            print("\n")
            print("ERROR", error_reason)
            print("Detected intent:", intent)
            print("Converted intent:", convint)
            print("Label:", label)

        if len(results) % 20 == 0:
            # Start new dialog every 20 requests,
            # attempt to fix empty intent responses after some time
            _ = start_new_dialog()
            dialog_request_id = start_new_dialog()

        if len(results) % 50 == 0:
            status = " Numbers of correct files: {}/{}"
            print(status.format(sum((1 for r in results if r)), len(results)))

    conv_dur = time.time() - start_time
    msg = " The benchmark took {}"
    print(msg.format(seconds_to_hours(conv_dur)))

    # Count wrong and correct instead of using list length for debugging purposes
    # (allows to end the loop midways)
    num_correct = sum((1 for r in results if r))
    acc = num_correct / len(results)
    msg = " Correctly converted {}/{}. The accuracy is {:.4f}"
    print(msg.format(num_correct, len(results), acc))


# ==================================================================================================


def main():
    global alexa, dialog_request_id

    parser = argparse.ArgumentParser(description="Run benchmark with Alexa")
    parser.add_argument(
        "--reduced_dataset",
        default=0,
        type=int,
        help="Test with fewer files",
    )
    args = parser.parse_args()

    alexa = connect_alexa()
    dialog_request_id = start_new_dialog()

    # Load labels
    labels = load_labels(label_csv)
    if 0 < args.reduced_dataset <= len(labels):
        # Reduce dataset size if flag given
        labels = labels[0 : args.reduced_dataset]

    # Run the benchmark
    run_benchmark(labels)

    # There is still some thread running, maybe from avs-client library
    sys.exit()


# ==================================================================================================

if __name__ == "__main__":
    main()
