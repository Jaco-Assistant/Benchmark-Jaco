# -*- coding: utf-8 -*-

# This sample demonstrates handling intents from an Alexa skill using the Alexa Skills Kit SDK for Python.
# Please visit https://alexa.design/cookbook for additional examples on implementing slots, dialog management,
# session persistence, api calls, and more.
# This sample is built using the handler classes approach in skill builder.
import json
import logging
from typing import Optional, Union

import ask_sdk_core.utils as ask_utils
from ask_sdk_core.dispatch_components import (
    AbstractExceptionHandler,
    AbstractRequestHandler,
)
from ask_sdk_core.handler_input import HandlerInput
from ask_sdk_core.skill_builder import SkillBuilder
from ask_sdk_model import Response
from ask_sdk_model.ui import SimpleCard

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
AnyStr = Union[str, bytes]


def get_canonical_slot_value(handler_input, slot_name):
    # type: (HandlerInput, str) -> Optional[AnyStr]
    """Alexa does return the spoken slot value instead of the canonical value and has no builtin
    function to get the canonical value that is mostly required."""

    slot = ask_utils.request_util.get_slot(
        handler_input=handler_input, slot_name=slot_name
    )

    try:
        value = slot.resolutions.resolutions_per_authority[0].values[0].value.name
        return value
    except AttributeError:
        if slot is not None:
            return slot.value
        else:
            return None


class LaunchRequestHandler(AbstractRequestHandler):
    """Handler for Skill Launch."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool

        return ask_utils.is_request_type("LaunchRequest")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Welcome, ask me some simple requests."

        card_title = "started"
        card_text = "FluentSpeechCommands-Alexa"

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .set_card(SimpleCard(card_title, card_text))
            .response
        )


class ChangeLanguageIntentHandler(AbstractRequestHandler):
    """Handler for ChangeLanguage Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("change_language")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Ok"

        intent = {
            "intent": "change_language",
            "slots": {
                "language": get_canonical_slot_value(handler_input, "language"),
            },
        }

        card_title = "change_language"
        card_text = json.dumps(intent)

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .set_card(SimpleCard(card_title, card_text))
            .response
        )


class ActivateMusicIntentHandler(AbstractRequestHandler):
    """Handler for ActivateMusic Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("activate_music")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Ok"

        intent = {
            "intent": "activate_music",
            "slots": {},
        }

        card_title = "activate_music"
        card_text = json.dumps(intent)

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .set_card(SimpleCard(card_title, card_text))
            .response
        )


class ActivateLightsIntentHandler(AbstractRequestHandler):
    """Handler for ActivateLights Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("activate_lights")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Ok"

        intent = {
            "intent": "activate_lights",
            "slots": {
                "location": get_canonical_slot_value(handler_input, "location"),
            },
        }

        card_title = "activate_lights"
        card_text = json.dumps(intent)

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .set_card(SimpleCard(card_title, card_text))
            .response
        )


class DeactivateLightsIntentHandler(AbstractRequestHandler):
    """Handler for DeactivateLights Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("deactivate_lights")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Ok"

        intent = {
            "intent": "deactivate_lights",
            "slots": {
                "location": get_canonical_slot_value(handler_input, "location"),
            },
        }

        card_title = "deactivate_lights"
        card_text = json.dumps(intent)

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .set_card(SimpleCard(card_title, card_text))
            .response
        )


class IncreaseVolumeIntentHandler(AbstractRequestHandler):
    """Handler for IncreaseVolume Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("increase_volume")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Ok"

        intent = {
            "intent": "increase_volume",
            "slots": {},
        }

        card_title = "increase_volume"
        card_text = json.dumps(intent)

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .set_card(SimpleCard(card_title, card_text))
            .response
        )


class DecreaseVolumeIntentHandler(AbstractRequestHandler):
    """Handler for DecreaseVolume Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("decrease_volume")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Ok"

        intent = {
            "intent": "decrease_volume",
            "slots": {},
        }

        card_title = "decrease_volume"
        card_text = json.dumps(intent)

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .set_card(SimpleCard(card_title, card_text))
            .response
        )


class IncreaseHeatIntentHandler(AbstractRequestHandler):
    """Handler for IncreaseHeat Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("increase_heat")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Ok"

        intent = {
            "intent": "increase_heat",
            "slots": {
                "location": get_canonical_slot_value(handler_input, "location"),
            },
        }

        card_title = "increase_heat"
        card_text = json.dumps(intent)

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .set_card(SimpleCard(card_title, card_text))
            .response
        )


class DecreaseHeatIntentHandler(AbstractRequestHandler):
    """Handler for DecreaseHeat Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("decrease_heat")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Ok"

        intent = {
            "intent": "decrease_heat",
            "slots": {
                "location": get_canonical_slot_value(handler_input, "location"),
            },
        }

        card_title = "decrease_heat"
        card_text = json.dumps(intent)

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .set_card(SimpleCard(card_title, card_text))
            .response
        )


class DeactivateMusicIntentHandler(AbstractRequestHandler):
    """Handler for DeactivateMusic Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("deactivate_music")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Ok"

        intent = {
            "intent": "deactivate_music",
            "slots": {},
        }

        card_title = "deactivate_music"
        card_text = json.dumps(intent)

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .set_card(SimpleCard(card_title, card_text))
            .response
        )


class ActivateLampIntentHandler(AbstractRequestHandler):
    """Handler for ActivateLamp Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("activate_lamp")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Ok"

        intent = {
            "intent": "activate_lamp",
            "slots": {},
        }

        card_title = "activate_lamp"
        card_text = json.dumps(intent)

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .set_card(SimpleCard(card_title, card_text))
            .response
        )


class DeactivateLampIntentHandler(AbstractRequestHandler):
    """Handler for DeactivateLamp Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("deactivate_lamp")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Ok"

        intent = {
            "intent": "deactivate_lamp",
            "slots": {},
        }

        card_title = "deactivate_lamp"
        card_text = json.dumps(intent)

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .set_card(SimpleCard(card_title, card_text))
            .response
        )


class BringIntentHandler(AbstractRequestHandler):
    """Handler for Bring Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("bring")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Ok"

        intent = {
            "intent": "bring",
            "slots": {
                "object": get_canonical_slot_value(handler_input, "object"),
            },
        }

        card_title = "bring"
        card_text = json.dumps(intent)

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .set_card(SimpleCard(card_title, card_text))
            .response
        )


class HelpIntentHandler(AbstractRequestHandler):
    """Handler for Help Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("AMAZON.HelpIntent")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Let's do some calculations"

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .response
        )


class CancelOrStopIntentHandler(AbstractRequestHandler):
    """Single handler for Cancel and Stop Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("AMAZON.CancelIntent")(
            handler_input
        ) or ask_utils.is_intent_name("AMAZON.StopIntent")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Goodbye!"

        return handler_input.response_builder.speak(speak_output).response


class SessionEndedRequestHandler(AbstractRequestHandler):
    """Handler for Session End."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_request_type("SessionEndedRequest")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response

        # Any cleanup logic goes here.

        return handler_input.response_builder.response


class IntentReflectorHandler(AbstractRequestHandler):
    """The intent reflector is used for interaction model testing and debugging.
    It will simply repeat the intent the user said. You can create custom handlers
    for your intents by defining them above, then also adding them to the request
    handler chain below.
    """

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_request_type("IntentRequest")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        intent_name = ask_utils.get_intent_name(handler_input)
        speak_output = "You just triggered " + intent_name + "."

        return (
            handler_input.response_builder.speak(speak_output)
            # .ask("add a reprompt if you want to keep the session open for the user to respond")
            .response
        )


class CatchAllExceptionHandler(AbstractExceptionHandler):
    """Generic error handling to capture any syntax or routing errors. If you receive an error
    stating the request handler chain is not found, you have not implemented a handler for
    the intent being invoked or included it in the skill builder below.
    """

    def can_handle(self, handler_input, exception):
        # type: (HandlerInput, Exception) -> bool
        return True

    def handle(self, handler_input, exception):
        # type: (HandlerInput, Exception) -> Response
        logger.error(exception, exc_info=True)

        speak_output = "Sorry, I had trouble doing what you asked. Please try again."

        return (
            handler_input.response_builder.speak(speak_output)
            .ask(speak_output)
            .response
        )


# The SkillBuilder object acts as the entry point for your skill, routing all request and response
# payloads to the handlers above. Make sure any new handlers or interceptors you've
# defined are included below. The order matters - they're processed top to bottom.


sb = SkillBuilder()

sb.add_request_handler(LaunchRequestHandler())
sb.add_request_handler(ChangeLanguageIntentHandler())
sb.add_request_handler(ActivateMusicIntentHandler())
sb.add_request_handler(ActivateLightsIntentHandler())
sb.add_request_handler(DeactivateLightsIntentHandler())
sb.add_request_handler(IncreaseVolumeIntentHandler())
sb.add_request_handler(DecreaseVolumeIntentHandler())
sb.add_request_handler(IncreaseHeatIntentHandler())
sb.add_request_handler(DecreaseHeatIntentHandler())
sb.add_request_handler(DeactivateMusicIntentHandler())
sb.add_request_handler(ActivateLampIntentHandler())
sb.add_request_handler(DeactivateLampIntentHandler())
sb.add_request_handler(BringIntentHandler())
sb.add_request_handler(HelpIntentHandler())
sb.add_request_handler(CancelOrStopIntentHandler())
sb.add_request_handler(SessionEndedRequestHandler())
sb.add_request_handler(
    IntentReflectorHandler()
)  # make sure IntentReflectorHandler is last so it doesn't override your custom intent handlers

sb.add_exception_handler(CatchAllExceptionHandler())

lambda_handler = sb.lambda_handler()
