import argparse
import json
import os
import re
import sys
import time

import numpy as np
import soundfile as sf
import tensorflow as tf
import tqdm

sys.path.insert(1, "/slungt/swig/")
import slungt  # noqa: E402 pylint: disable=wrong-import-position

sys.path.insert(1, "/slungt/tests/")
import test_interface  # noqa: E402 pylint: disable=wrong-import-position

# ==================================================================================================

filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
stt_model_dir = filepath + "../sttmodels/en-conformerctc-large-new/"
# stt_model_dir = filepath + "../sttmodels/en-conformerctc-large-cb4/"
tf_model: tf.keras.Model

wav_files_txt = filepath + "testwavs.txt"
gold_data_path = filepath + "slurp/dataset/slurp/test.jsonl"
result_path = filepath + "predictions.jsonl"

alphabet_path = stt_model_dir + "alphabet.json"
with open(alphabet_path, "r", encoding="utf-8") as afile:
    alphabet = json.load(afile)

datapath = filepath + "moduldata/sludata/"
vocabs, intents, lms = test_interface.load_data(datapath)

slu_decoder = slungt.Decoder(
    list(alphabet),
    256,
    vocabs,
    intents,
    lms,
    lm_alpha=1.5,
    token_min_logprob=-10.0,
    token_max_count=32,
)


# ==================================================================================================


def seconds_to_hours(secs):
    secs = int(secs)
    m, s = divmod(secs, 60)
    h, m = divmod(m, 60)
    t = "{:02d}:{:02d}:{:02d}".format(h, m, s)
    return t


# ==================================================================================================


def get_duration(filename):
    """Get duration of the wav file"""
    length = sf.info(filename).duration
    return length


# ==================================================================================================


def load_labels(path: str) -> list:
    with open(path, "r", encoding="utf-8") as file:
        content = file.readlines()

    dataset = []
    for line in content:
        entry = json.loads(line)
        dataset.append(entry)

    datamap = {}
    for entry in dataset:
        for recording in entry["recordings"]:
            file = recording["file"].replace(".flac", ".wav")

            entrynew = dict(entry)
            entrynew.pop("tokens")
            entrynew.pop("recordings")
            entrynew.pop("entities")
            datamap[file] = entrynew

    return datamap


# ==================================================================================================


def load_audio(wav_path):
    audio, _ = sf.read(wav_path, dtype="int16")
    audio = audio / (np.iinfo(np.int16).max + 1)
    audio = np.expand_dims(audio, axis=0)
    audio = audio.astype(np.float32)
    return audio


# ==================================================================================================


def predict_signal_tf(audio):
    global tf_model

    output_data = tf_model.predict(audio, steps=1)
    output_data = output_data[0]
    return output_data


# ==================================================================================================


def get_intent_from_audio(audio_path):
    """Get intent from audio file"""

    audio = load_audio(audio_path)
    prediction = predict_signal_tf(audio)
    probs = prediction.tolist()

    results = slu_decoder.decode(probs)
    result = results[0]
    slungt_intent = slu_decoder.extract_text2intent(result)
    intent = json.loads(str(slungt_intent))

    intent["result"] = str(result)
    intent["greedy"] = slu_decoder.decode_greedy_text(probs)

    return intent


# ==================================================================================================


def convert_intent(intent):
    """Convert to slurp result format"""

    convint = {
        "file": intent["file"],
        "scenario": "",
        "action": "",
        "entities": [],
        "text": intent["text"],
        "greedy": intent["greedy"],
        "result": intent["result"],
    }

    if "-" in intent["name"]:
        scen, act = intent["name"].split("-")
        convint["scenario"] = scen
        convint["action"] = act

    if "entities" in intent:
        for e in intent["entities"]:
            entity = {
                "type": e["name"].split("-")[1],
                "filler": e["value"],
            }

            convint["entities"].append(entity)

    return convint


# ==================================================================================================


def compare_intents(intent, label):
    if label["scenario"].lower() != intent["scenario"]:
        return False, "scenario wrong"

    if label["action"].lower() != intent["action"]:
        return False, "action wrong"

    slot_pattern = r"(\[([^\[]*) : ([^\[]*)\])"
    matches = re.findall(slot_pattern, label["sentence_annotation"])
    for match in matches:
        entity = {
            "type": match[1].lower(),
            "filler": match[2].lower(),
        }
        if entity not in intent["entities"]:
            return False, "slot wrong: {}".format(entity)

    return True, ""


# ==================================================================================================


def run_benchmark(wav_files: list, labels: dict):
    print("\nRunning benchmark ...")
    start_time = time.time()

    results = []
    predictions = []
    for filename in tqdm.tqdm(wav_files):
        lfile = os.path.basename(filename)
        label = labels[lfile]
        intent = get_intent_from_audio(filename)

        intent["file"] = lfile
        convint = convert_intent(intent)
        same, error_reason = compare_intents(convint, label)
        results.append(same)

        convint["slurp_id"] = label["slurp_id"]
        predictions.append(convint)

        if not same:
            # Print wrong predictions for debugging
            print("\n")
            print("ERROR", error_reason)
            print("Detected intent:", intent)
            print("Converted intent:", convint)
            print("Label:", label)

    conv_dur = time.time() - start_time
    msg = " The benchmark took {}"
    print(msg.format(seconds_to_hours(conv_dur)))

    # Save predictions for evaluation script
    with open(result_path, "w+", encoding="utf-8") as file:
        content = [json.dumps(p) for p in predictions]
        content = [c.replace(".wav", ".flac") for c in content]
        file.write("\n".join(content))

    # Count wrong and correct instead of using list length for debugging purposes
    # (allows to end the loop midways)
    num_correct = sum((1 for r in results if r))
    acc = num_correct / len(results)
    msg = " Correctly converted {}/{}. The accuracy is {:.4f}"
    print(msg.format(num_correct, len(results), acc))


# ==================================================================================================


def main():
    global tf_model

    parser = argparse.ArgumentParser(description="Run benchmark with Jaco")
    parser.add_argument(
        "--reduced_dataset",
        default=0,
        type=int,
        help="Test with fewer files",
    )
    args = parser.parse_args()

    # Load stt model and run some warmup predictions
    stime = time.time()
    tf_model = tf.keras.models.load_model(stt_model_dir + "pb/")
    tf_model.predict(np.random.rand(1, 16000).astype(np.float32))
    tf_model.predict(np.random.rand(1, 48000).astype(np.float32))
    tf_model.predict(np.random.rand(1, 32000).astype(np.float32))
    tf_model.summary()
    print("Time to load stt model:", time.time() - stime)

    # Load testfiles
    with open(wav_files_txt, "r", encoding="utf-8") as file:
        wav_files = file.readlines()
        wav_files = [w.strip() for w in wav_files if w.strip() != ""]
    wav_files = sorted(wav_files)

    if 0 < args.reduced_dataset <= len(wav_files):
        # Reduce dataset size if flag given
        wav_files = wav_files[0 : args.reduced_dataset]

    # Load labels
    labels = load_labels(gold_data_path)

    # Get duration of sample files, extra because this needs some time too
    audio_dur = 0
    for file_name in tqdm.tqdm(wav_files):
        audio_dur += get_duration(os.path.join(file_name))
    msg = "The benchmark contains {} files with {} hours of audio"
    print(msg.format(len(wav_files), seconds_to_hours(audio_dur)))

    # Now run the benchmark
    run_benchmark(wav_files, labels)


# ==================================================================================================

if __name__ == "__main__":
    main()
