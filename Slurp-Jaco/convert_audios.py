import multiprocessing as mp
import os
from functools import partial
from io import BytesIO
from typing import List, Tuple

import tqdm
from pydub import AudioSegment

# ==============================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
audio_dir_original = file_path + "../datasets/slurp_real/"
audio_dir_converted = file_path + "../datasets/slurp_wavs/"
audiotxt = file_path + "skills/testflacs.txt"
audiotxtnew = file_path + "testwavs.txt"

# ==============================================================================================


def audio_to_mono_wav(paths: Tuple[str, str], sample_rate: int) -> None:
    """Copied from corcua"""

    path_source = paths[0]
    path_target = paths[1]

    if not os.path.exists(path_source):
        msg = "This file does not exist: {}".format(path_source)
        raise FileNotFoundError(msg)

    _, extension = os.path.splitext(os.path.basename(path_source))
    extension = extension.replace(".", "")

    if extension == "opus":
        with open(path_source, "rb") as file:
            opus_audio_bytes = file.read()
        opus_data = BytesIO(opus_audio_bytes)
        audio = AudioSegment.from_file(opus_data, codec="opus")
    else:
        audio = AudioSegment.from_file(path_source, extension)

    audio = audio.set_frame_rate(sample_rate)
    audio = audio.set_channels(1)
    audio.export(path_target, codec="pcm_s16le", format="wav")


# ==============================================================================================


def convert_audio(audio_mappings: List[Tuple[str, str]], sample_rate: int) -> None:
    """Convert list of audio files with input and output paths from various encodings
    into mono-channel wav format with given sample rate. Copied from corcua."""

    pfunc = partial(audio_to_mono_wav, sample_rate=sample_rate)
    with mp.Pool(mp.cpu_count()) as p:
        list(tqdm.tqdm(p.imap(pfunc, audio_mappings), total=len(audio_mappings)))


# ==============================================================================================


def main():
    with open(audiotxt, "r", encoding="utf-8") as file:
        testflacs = file.readlines()
        testflacs = [f.strip() for f in testflacs]

    if not os.path.isdir(audio_dir_converted):
        os.makedirs(audio_dir_converted)

    audio_mappings = []
    new_paths = []
    for fp in testflacs:
        am1 = os.path.join(audio_dir_original, fp)
        if not os.path.exists(am1):
            print("This file does not exist:", fp)
            continue

        am2 = os.path.join(audio_dir_converted, fp)
        am2 = am2.replace(".flac", ".wav")
        new_paths.append(am2)

        audio_mappings.append((am1, am2))

    print("\nConverting audio ...")
    convert_audio(audio_mappings, sample_rate=16000)

    with open(audiotxtnew, "w+", encoding="utf-8") as file:
        file.write("\n".join(new_paths))


# ==================================================================================================

if __name__ == "__main__":
    main()
