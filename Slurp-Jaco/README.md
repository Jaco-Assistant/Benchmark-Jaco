# Benchmark SLURP Jaco

SLURP is a large SLU dataset which includes multiple different domains.
The corresponding paper can be found [here](https://arxiv.org/pdf/2011.13205.pdf).

<br>

### Test Jaco

Steps to rebuild everything:

- Download dataset (real partition only), from [here](https://github.com/pswietojanski/slurp)

- Extract dataset to `datasets/slurp_real/`.

- Add dataset repository as submodule:

  ```bash
  cd Slurp-Jaco/
  git submodule add https://github.com/pswietojanski/slurp

  # Only to update submodule
  git pull && git submodule update --init --recursive
  ```

- Build container:

  ```bash
  docker build --progress=plain -f Slurp-Jaco/Containerfile -t slurp_jaco ./Slurp-Jaco/
  ```

- Convert intents: \
  (Optionally add lookup values from the test examples with `--add_test_lookups` flag)

  ```bash
  docker run --rm \
    --volume `pwd`/Slurp-Jaco/:/Benchmark-Jaco/Slurp-Jaco/ \
    -it slurp_jaco python3 /Benchmark-Jaco/Slurp-Jaco/convert_intents.py
  ```

- Convert audio file format:

  ```bash
  docker run --rm \
    --volume `pwd`/Slurp-Jaco/:/Benchmark-Jaco/Slurp-Jaco/ \
    --volume `pwd`/datasets/:/Benchmark-Jaco/datasets/ \
    -it slurp_jaco python3 /Benchmark-Jaco/Slurp-Jaco/convert_audios.py
  ```

- Set language in Jaco-Master's `global_config` file to english

- Generate dialog dataset:

  ```bash
  # Run from parent directory
  docker run --network host --rm \
    --volume `pwd`/Jaco-Master/skills/collect_data.py:/Jaco-Master/skills/collect_data.py:ro \
    --volume `pwd`/Jaco-Master/skills/generate_data.py:/Jaco-Master/skills/generate_data.py:ro \
    --volume `pwd`/Jaco-Master/skills/map_intents_slots.py:/Jaco-Master/skills/map_intents_slots.py:ro \
    --volume `pwd`/Jaco-Master/skills/sentence_tools.py:/Jaco-Master/skills/sentence_tools.py:ro \
    --volume `pwd`/Jaco-Master/skills/text_tools.py:/Jaco-Master/skills/text_tools.py:ro \
    --volume `pwd`/Jaco-Master/skills/basic_normalizer.py:/Jaco-Master/skills/basic_normalizer.py:ro \
    --volume `pwd`/Jaco-Master/slu-parser/moduldata/langdicts.json:/Jaco-Master/slu-parser/moduldata/langdicts.json:ro \
    --volume `pwd`/Jaco-Master/jacolib/:/jacolib/:ro \
    --volume `pwd`/Benchmark-Jaco/Slurp-Jaco/skills/:/Jaco-Master/skills/skills/:ro \
    --volume `pwd`/Jaco-Master/userdata/config/:/Jaco-Master/userdata/config/:ro \
    --volume `pwd`/Benchmark-Jaco/Slurp-Jaco/userdata/skill_topic_keys.json:/Jaco-Master/userdata/skill_topic_keys.json:ro \
    --volume `pwd`/Benchmark-Jaco/Slurp-Jaco/moduldata/collected/:/Jaco-Master/skills/collected/ \
    --volume `pwd`/Benchmark-Jaco/Slurp-Jaco/moduldata/generated/:/Jaco-Master/skills/generated/ \
    -it master_base_image_amd64 /bin/bash -c ' \
      python3 /Jaco-Master/skills/collect_data.py && \
      python3 /Jaco-Master/skills/map_intents_slots.py && \
      python3 /Jaco-Master/skills/generate_data.py'
  ```

- Create language model:

  ```bash
  docker run --network host --rm \
    --volume `pwd`/Slurp-Jaco/:/Benchmark-Jaco/Slurp-Jaco/ \
    --volume `pwd`/../slungt/:/slungt/ \
    --volume `pwd`/sttmodels/:/Benchmark-Jaco/sttmodels/ \
    -it slurp_jaco \
      python3 /slungt/preprocess/fullbuild_slu.py \
        --workdir /Benchmark-Jaco/Slurp-Jaco/moduldata/sludata/ \
        --nlufile_path /Benchmark-Jaco/Slurp-Jaco/moduldata/generated/nlu/nlu.json \
        --alphabet_path /Benchmark-Jaco/sttmodels/en-conformerctc-large-new/alphabet.json
  ```

- Run benchmark:

  ```bash
  docker run --rm --network host \
    --volume `pwd`/Slurp-Jaco/:/Benchmark-Jaco/Slurp-Jaco/ \
    --volume `pwd`/../slungt/:/slungt/ \
    --volume `pwd`/datasets/:/Benchmark-Jaco/datasets/ \
    --volume `pwd`/sttmodels/:/Benchmark-Jaco/sttmodels/ \
    -it slurp_jaco

  # Build slungt
  cd /slungt/swig/; make all; cd /

  # Run predictions
  python3 /Benchmark-Jaco/Slurp-Jaco/run_benchmark.py --reduced_dataset 100

  # Evaluate
  python3 /Benchmark-Jaco/Slurp-Jaco/slurp/scripts/evaluation/evaluate.py \
    --gold-data /Benchmark-Jaco/Slurp-Jaco/slurp/dataset/slurp/test.jsonl \
    --prediction-file /Benchmark-Jaco/Slurp-Jaco/predictions.jsonl \
    --average macro --errors
  ```

Accuracy: 0.4218 - SA-F1: 0.5474 - E-F1: 0.3935 - SLU-F1: 0.4595 \
Benchmark Runtime: 00:01:21 + 04:03:27
