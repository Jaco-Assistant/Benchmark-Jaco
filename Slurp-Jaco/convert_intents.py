import argparse
import json
import os
import re
import shutil

import tqdm

# ==================================================================================================


file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
skills_dir = file_path + "skills/"
userdata_dir = file_path + "userdata/"

data_train_path = file_path + "slurp/dataset/slurp/train.jsonl"
data_eval_path = file_path + "slurp/dataset/slurp/devel.jsonl"
data_test_path = file_path + "slurp/dataset/slurp/test.jsonl"

special_replacers = {
    "@": "at",
    "#": "hash tag",
}
extra_replacers = {"tech n9ne": "tech nine", "unk>": ""}

# ==================================================================================================


def emtpy_with_ignore(path: str) -> None:
    """Deletes and recreates a directory. Adds a gitignore file to ignore directory contents"""

    if os.path.isdir(path):
        try:
            shutil.rmtree(path)
            os.mkdir(path)
        except OSError as err:
            if len(os.listdir(path)) == 0:
                # In the special case that this directory is mounted as volume it can't be deleted
                # But it will be emptied so the error can be ignored
                pass
            else:
                raise err
    else:
        os.mkdir(path)

    with open(path + ".gitignore", "w+", encoding="utf-8") as file:
        file.write("*\n!.gitignore\n")


# ==================================================================================================


def load_dataset(path: str) -> list:
    with open(path, "r", encoding="utf-8") as file:
        content = file.readlines()

    dataset = []
    for line in content:
        entry = json.loads(line)
        dataset.append(entry)

    return dataset


# ==================================================================================================


def convert_traindata(traindata: list):
    nludata = {}
    slot_pattern = r"(\[([^\[]*) : ([^\[]*)\])"
    lookup_skill = "skill_dialogs"
    nludata[lookup_skill] = {"intents": {}, "lookups": {}}

    for sample in tqdm.tqdm(traindata):
        skill = sample["scenario"]
        intent = sample["action"].lower()
        annot = str(sample["sentence_annotation"])

        if skill not in nludata:
            nludata[skill] = {"intents": {}, "lookups": {}}
        if intent not in nludata[skill]["intents"]:
            nludata[skill]["intents"][intent] = []

        matches = re.findall(slot_pattern, annot)
        for match in matches:
            slot_val = match[2]
            slot_type = match[1].lower()
            new_text = "[{}]({}-{})".format(slot_val, lookup_skill, slot_type)
            annot = annot.replace(match[0], new_text)

            if slot_type not in nludata[lookup_skill]["lookups"]:
                nludata[lookup_skill]["lookups"][slot_type] = []
            nludata[lookup_skill]["lookups"][slot_type].append(slot_val)
        nludata[skill]["intents"][intent].append(annot)

    return nludata


# ==================================================================================================


def add_test_lookups(nludata: dict, testdata: list):
    slot_pattern = r"(\[([^\[]*) : ([^\[]*)\])"
    lookup_skill = "skill_dialogs"

    for sample in tqdm.tqdm(testdata):
        annot = str(sample["sentence_annotation"])

        matches = re.findall(slot_pattern, annot)
        for match in matches:
            slot_val = match[2]
            slot_type = match[1].lower()

            if slot_type not in nludata[lookup_skill]["lookups"]:
                nludata[lookup_skill]["lookups"][slot_type] = []
            nludata[lookup_skill]["lookups"][slot_type].append(slot_val)

    return nludata


# ==================================================================================================


def clean_sample(sample: str) -> str:
    # Replace special characters
    for to_replace, replacement in special_replacers.items():
        sample = sample.replace(to_replace, " {} ".format(replacement))

    # Replace extra words
    for to_replace, replacement in extra_replacers.items():
        sample = sample.replace(to_replace, " {} ".format(replacement))

    # Remove all dots except those of the entity link
    sample = re.sub(r"\.(?!txt)", "", sample)

    # Remove all dashes and underscores except those in the entity names
    sample = re.sub(r"-(?![^(]*\))", " ", sample)
    sample = re.sub(r"_(?![^(]*\))", " ", sample)

    sample = sample.lower()
    sample = " ".join(sample.split())

    return sample


# ==================================================================================================


def clean_nludata(nludata: dict) -> dict:
    new_nludata = {}
    for skill in nludata:
        new_nludata[skill] = {"intents": {}, "lookups": {}}

        for intent in nludata[skill]["intents"]:
            new_nludata[skill]["intents"][intent] = []

            for sample in nludata[skill]["intents"][intent]:
                sample = clean_sample(sample)
                new_nludata[skill]["intents"][intent].append(sample)

        for lookup in nludata[skill]["lookups"]:
            new_nludata[skill]["lookups"][lookup] = []

            for sample in nludata[skill]["lookups"][lookup]:
                sample = clean_sample(sample)
                new_nludata[skill]["lookups"][lookup].append(sample)

    return new_nludata


# ==================================================================================================


def save_nludata(skillpath: str, nludata: dict) -> None:
    for skill in nludata:
        skilldir = os.path.join(skillpath, skill, "dialog/nlu/en/")
        os.makedirs(skilldir)

        # Write lookup files
        for lookup in nludata[skill]["lookups"]:
            values = nludata[skill]["lookups"][lookup]
            content = "\n".join(set(values))

            outpath = os.path.join(skilldir, "{}.txt".format(lookup))
            with open(outpath, "w+", encoding="utf-8") as file:
                file.write(content)

        # Write nlu file
        content = ""
        for lookup in nludata[skill]["lookups"]:
            s = "## lookup:{}\n{}.txt\n\n".format(lookup, lookup)
            content += s
        for intent in nludata[skill]["intents"]:
            s = "## intent:{}\n".format(intent)
            content += s

            for sample in nludata[skill]["intents"][intent]:
                s = "- {}\n".format(sample)
                content += s
            content += "\n"

        outpath = os.path.join(skilldir, "nlu.md")
        with open(outpath, "w+", encoding="utf-8") as file:
            file.write(content)


# ==================================================================================================


def collect_testaudios(testdata: list) -> list:
    testfiles = []

    for sample in testdata:
        for recording in sample["recordings"]:
            testfiles.append(recording["file"])

    testfiles = sorted(set(testfiles))
    return testfiles


# ==================================================================================================


def create_topic_keys(traindata: dict, savepath: str) -> None:
    tpks = {}
    for skill in traindata:
        for intent in traindata[skill]["intents"]:
            iname = intent.title().replace("_", "")
            tpk = "Jaco/Intents/{}/{}".format(skill.title(), iname)
            tpks[tpk] = ""

    with open(savepath, "w+", encoding="utf-8") as file:
        json.dump(tpks, file, indent=2)


# ==================================================================================================


def main():
    parser = argparse.ArgumentParser(description="")
    parser.add_argument(
        "--add_test_lookups",
        action="store_true",
        help="Add lookup values from test dataset to training values",
    )
    args = parser.parse_args()

    traindata = load_dataset(data_train_path)
    traindata.extend(load_dataset(data_eval_path))
    testdata = load_dataset(data_test_path)

    nludata = convert_traindata(traindata)
    if args.add_test_lookups:
        nludata = add_test_lookups(nludata, testdata)

    nludata = clean_nludata(nludata)
    emtpy_with_ignore(skills_dir)
    save_nludata(skills_dir, nludata)

    testaudios = collect_testaudios(testdata)
    outpath = os.path.join(skills_dir, "testflacs.txt")
    with open(outpath, "w+", encoding="utf-8") as file:
        file.write("\n".join(testaudios))

    outpath = os.path.join(userdata_dir, "skill_topic_keys.json")
    create_topic_keys(nludata, outpath)


# ==================================================================================================

if __name__ == "__main__":
    main()
