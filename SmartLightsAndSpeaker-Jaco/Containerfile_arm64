FROM docker.io/ubuntu:20.04

ARG DEBIAN_FRONTEND=noninteractive
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

RUN apt-get update && apt-get upgrade -y
RUN apt-get update && apt-get install -y --no-install-recommends nano wget curl git

# Install python
RUN apt-get update && apt-get install -y --no-install-recommends python3 python3-pip
RUN pip3 install --upgrade --no-cache-dir pip
RUN python3 -V && pip3 --version

# Audio tools
RUN apt-get install -y --no-install-recommends alsa-base alsa-utils
RUN apt-get install -y --no-install-recommends libsndfile1

# Install swig
RUN apt-get update && apt-get install -y --no-install-recommends python3-dev
RUN apt-get update && apt-get install -y --no-install-recommends build-essential
RUN apt-get update && apt-get install -y --no-install-recommends swig

# Install kenlm
RUN apt-get update && apt-get install -y --no-install-recommends \
 cmake libboost-system-dev libboost-thread-dev libboost-program-options-dev \
 libboost-test-dev libeigen3-dev zlib1g-dev libbz2-dev liblzma-dev
RUN git clone --depth 1 https://github.com/kpu/kenlm.git
RUN cd /kenlm/; mkdir -p build/
RUN cd /kenlm/build/; cmake ..
RUN cd /kenlm/build/; make -j 4
RUN pip3 install --no-cache https://github.com/kpu/kenlm/archive/master.zip

# Python dependencies
RUN pip3 install --no-cache-dir --upgrade numpy
RUN pip3 install --no-cache-dir --upgrade soundfile
RUN pip3 install --no-cache-dir --upgrade tqdm
RUN pip3 install --no-cache-dir --upgrade matplotlib
RUN pip3 install --no-cache-dir --upgrade num2words
RUN pip3 install --no-cache-dir --upgrade future

# Install optimized tflite-runtime
RUN wget https://github.com/PINTO0309/TensorflowLite-bin/releases/download/v2.9.0/tflite_runtime-2.9.0-cp38-none-linux_aarch64.whl
RUN pip3 install --no-cache-dir tflite_runtime-*.whl
RUN rm tflite_runtime-*.whl

# Get benchmark tools
WORKDIR /
RUN git clone https://github.com//snipsco/spoken-language-understanding-research-datasets

# Install jacolib
RUN git clone https://gitlab.com/Jaco-Assistant/jacolib.git
RUN pip3 install --no-cache-dir --upgrade --user -e jacolib

# Clear cache to save space, only has an effect if image is squashed
RUN apt-get autoremove -y \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

WORKDIR /Benchmark-Jaco/
CMD ["/bin/bash"]
