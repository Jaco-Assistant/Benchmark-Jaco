import matplotlib.pyplot as plt

# ==================================================================================================

# Jaco
jaco_slc = 0.8536
jaco_ssc_en = 0.6268
jaco_ssc_fr = 0.4796

# Finstreder
finstreder_cfsp_slc = 0.9024
finstreder_cfcb_ssc_en = 0.8787
finstreder_cfcb_ssc_fr = 0.8652

# Slungt
slungt_cfsp_slc = 0.8928
slungt_cfcb_ssc_en = 0.8474
slungt_cfcb_ssc_fr = 0.8679

# Google
google_slc = 0.7927
google_ssc_en = 0.4781
google_ssc_fr = 0.4231

# Snips
snips_slc = 0.8422
snips_ssc_en = 0.6873
snips_ssc_fr = 0.7513

# Alexa
alexa_slc = 0.7922
alexa_ssc_en = 0.4546
alexa_ssc_fr = 0.8892

# Houndify
houndify_slc = 0.5452

# SynSLU https://arxiv.org/pdf/1910.09463.pdf
synslu_slc = 0.714

# AT-AT https://arxiv.org/pdf/2012.08549.pdf
atat_slc = 0.8488


# ==================================================================================================


width = 0.7

labels = [
    "Google",
    "Snips",
    "Jaco",
    "Finstreder",
    "Slungt",
    "AT-AT",
    "Alexa",
    "SynSLU",
    "Houndify",
]
fig, ax = plt.subplots(figsize=(6, 5))
plt.title("Benchmark SmartLights Dataset\n")
plt.bar(1, google_slc, width=width, color="red", label="")
plt.bar(2, snips_slc, width=width, color="dimgray", label="")
plt.bar(3, jaco_slc, width=width, color="darkgreen", label="")
plt.bar(4, finstreder_cfsp_slc, width=width, color="lightgreen", label="")
plt.bar(5, slungt_cfsp_slc, width=width, color="turquoise", label="")
plt.bar(6, atat_slc, width=width, color="brown", label="")
plt.bar(7, alexa_slc, width=width, color="darkorange", label="")
plt.bar(8, synslu_slc, width=width, color="purple", label="")
plt.bar(9, houndify_slc, width=width, color="lightblue", label="")


plt.ylabel("Accuracy perfect Intent and Entity extraction")
plt.xticks(range(1, len(labels) + 1), labels, fontsize=7)
plt.ylim(0.0, 1.0)
plt.savefig("/Benchmark-Jaco/media/compare_smart_lights.png", dpi=300)
plt.clf()

labels = [
    "Google",
    "Snips",
    "Jaco",
    "Finstreder",
    "Slungt",
    "Alexa",
]
fig, ax = plt.subplots(figsize=(7, 5))
plt.title("Benchmark SmartSpeaker Dataset English\n")
plt.bar(1, google_ssc_en, width=width, color="red", label="")
plt.bar(2, snips_ssc_en, width=width, color="dimgray", label="")
plt.bar(3, jaco_ssc_en, width=width, color="darkgreen", label="")
plt.bar(4, finstreder_cfcb_ssc_en, width=width, color="lightgreen", label="")
plt.bar(5, slungt_cfcb_ssc_en, width=width, color="turquoise", label="")
plt.bar(6, alexa_ssc_en, width=width, color="darkorange", label="")

plt.ylabel("Accuracy perfect Intent and Entity extraction")
plt.xticks(range(1, len(labels) + 1), labels)
plt.ylim(0.0, 1.0)
plt.savefig("/Benchmark-Jaco/media/compare_smart_speaker_en.png", dpi=300)
plt.clf()

labels = [
    "Google",
    "Snips",
    "Jaco",
    "Finstreder",
    "Slungt",
    "Alexa",
]
fig, ax = plt.subplots(figsize=(7, 5))
plt.title("Benchmark SmartSpeaker Dataset French\n")
plt.bar(1, google_ssc_fr, width=width, color="red", label="")
plt.bar(2, snips_ssc_fr, width=width, color="dimgray", label="")
plt.bar(3, jaco_ssc_fr, width=width, color="darkgreen", label="")
plt.bar(4, finstreder_cfcb_ssc_fr, width=width, color="lightgreen", label="")
plt.bar(5, slungt_cfcb_ssc_fr, width=width, color="turquoise", label="")
plt.bar(6, alexa_ssc_fr, width=width, color="darkorange", label="")

plt.ylabel("Accuracy perfect Intent and Entity extraction")
plt.xticks(range(1, len(labels) + 1), labels)
plt.ylim(0.0, 1.0)
plt.savefig("/Benchmark-Jaco/media/compare_smart_speaker_fr.png", dpi=300)
plt.clf()
