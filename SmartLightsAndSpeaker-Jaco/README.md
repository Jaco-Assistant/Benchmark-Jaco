# Benchmark SmartLightsAndSpeaker Jaco

SmartLights and SmartSpeakers are SLU benchmark dataset from [Snips](https://snips.ai/).
The corresponding paper can be found [here](https://arxiv.org/pdf/1810.12735.pdf). \
SmartLights allows to control lights in different rooms.
SmartSpeakers allows to control a smart speaker through playback control but also play music from large libraries of artists, tracks and album. \
You can request the dataset here: [LINK](https://github.com//snipsco/spoken-language-understanding-research-datasets)

<br>

### Test Jaco

Steps to rebuild everything:

- Extract the dataset to `datasets/snips_slu/`.

- Build container:

  ```bash
  docker build --progress=plain -t smartlightsandspeaker_jaco - < SmartLightsAndSpeaker-Jaco/Containerfile
  ```

- Copy slu texts for numbers from [Skill-Dialogs](https://gitlab.com/Jaco-Assistant/Skill-Dialogs/-/tree/master/dialog).

- Convert intents: \
  (There is a flag to change the dataset at the top of the script)

  ```bash
  docker run --rm \
    --volume `pwd`/SmartLightsAndSpeaker-Jaco/:/Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/ \
    --volume `pwd`/datasets/:/Benchmark-Jaco/datasets/ \
    -it smartlightsandspeaker_jaco python3 /Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/convert_intents.py
  ```

- Set language in Jaco-Master's `global_config` file to english

- Generate dialog dataset:

  ```bash
  # Run from parent directory

  export NLU_DIR=nlu_slc_1
  docker run --network host --rm \
    --volume `pwd`/Jaco-Master/skills/collect_data.py:/Jaco-Master/skills/collect_data.py:ro \
    --volume `pwd`/Jaco-Master/skills/generate_data.py:/Jaco-Master/skills/generate_data.py:ro \
    --volume `pwd`/Jaco-Master/skills/map_intents_slots.py:/Jaco-Master/skills/map_intents_slots.py:ro \
    --volume `pwd`/Jaco-Master/skills/sentence_tools.py:/Jaco-Master/skills/sentence_tools.py:ro \
    --volume `pwd`/Jaco-Master/skills/text_tools.py:/Jaco-Master/skills/text_tools.py:ro \
    --volume `pwd`/Jaco-Master/skills/basic_normalizer.py:/Jaco-Master/skills/basic_normalizer.py:ro \
    --volume `pwd`/Jaco-Master/slu-parser/moduldata/langdicts.json:/Jaco-Master/slu-parser/moduldata/langdicts.json:ro \
    --volume `pwd`/Jaco-Master/jacolib/:/jacolib/:ro \
    --volume `pwd`/Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/dialog/${NLU_DIR}/:/Jaco-Master/skills/skills/SmartLightsAndSpeaker-Jaco/dialog/nlu/:ro \
    --volume `pwd`/Jaco-Master/userdata/config/:/Jaco-Master/userdata/config/:ro \
    --volume `pwd`/Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/userdata/skill_topic_keys.json:/Jaco-Master/userdata/skill_topic_keys.json:ro \
    --volume `pwd`/Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/moduldata/collected/:/Jaco-Master/skills/collected/ \
    --volume `pwd`/Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/moduldata/generated/:/Jaco-Master/skills/generated/ \
    -it master_base_image_amd64 /bin/bash -c ' \
      python3 /Jaco-Master/skills/collect_data.py && \
      python3 /Jaco-Master/skills/map_intents_slots.py && \
      python3 /Jaco-Master/skills/generate_data.py'
  ```

- Create language model:

  ```bash
  docker run --network host --rm \
    --volume `pwd`/SmartLightsAndSpeaker-Jaco/:/Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/ \
    --volume `pwd`/../slungt/:/slungt/ \
    --volume `pwd`/sttmodels/:/Benchmark-Jaco/sttmodels/ \
    -it smartlightsandspeaker_jaco \
      python3 /slungt/preprocess/fullbuild_slu.py \
        --workdir /Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/moduldata/sludata/ \
        --nlufile_path /Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/moduldata/generated/nlu/nlu.json \
        --alphabet_path /Benchmark-Jaco/sttmodels/en-conformerctc-large-new/alphabet.json
  ```

- Run benchmark:

  ```bash
  docker run --network host --rm \
    --volume `pwd`/SmartLightsAndSpeaker-Jaco/:/Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/ \
    --volume `pwd`/../slungt/:/slungt/ \
    --volume `pwd`/datasets/:/Benchmark-Jaco/datasets/ \
    --volume `pwd`/sttmodels/:/Benchmark-Jaco/sttmodels/ \
    --volume `pwd`/media/:/Benchmark-Jaco/media/ \
    -it smartlightsandspeaker_jaco

  # Build slungt
  cd /slungt/swig/; make all; cd /

  # Run the benchmark, change datadir in file if needed
  export NLU_DIR=nlu_slc_1
  python3 /Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/run_benchmark.py \
    --wav_files_txt /Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/dialog/${NLU_DIR}/test_wavs_en.txt --reduced_dataset 100
  ```

- Run 5-fold SmartLights benchmark:

  ```bash
  # Run from parent directory
  nohup ./Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/run_kfold_smartlight.sh > nohup.out 2>&1 &
  ```

- Plot graphs:
  ```bash
  # Run in container
  python3 /Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/plot_comparison.py
  ```

Accuracy 5-fold SmartLights-Close dataset: 0.8928 - gWER: 0.0750 - tWER: 0.0513 \
Benchmark Runtime: 00:04:59

Accuracy SmartSpeaker-Close dataset English: 0.8474 - gWER: 0.2199 - tWER: 0.0603 \
Accuracy SmartSpeaker-Close dataset French: 0.8679 - gWER: 0.3602 - tWER: 0.0718 \
Benchmark Runtime: 00:03:44 / 00:03:18

<br>

### Test plain decoding

To allow a comparison with other beam-search decoders, we need to create a language model that only contains standard words and has all intents merged together.

- Generate old style kenlm model: \

  - Create a second copy of Jaco-Master and reset to old version `git checkout --recurse-submodules 70be896fd62e20cd1cf3b8b65ac16e44a2aa3c97`

  - Generate all possible sentences

    ```bash
    export NLU_DIR=nlu_slc_1
    docker run --network host --rm \
    --volume `pwd`/Jaco-Master2/skills/collect_data.py:/Jaco-Master/skills/collect_data.py:ro \
    --volume `pwd`/Jaco-Master2/skills/generate_data.py:/Jaco-Master/skills/generate_data.py:ro \
    --volume `pwd`/Jaco-Master2/skills/map_intents_slots.py:/Jaco-Master/skills/map_intents_slots.py:ro \
    --volume `pwd`/Jaco-Master2/skills/sentence_tools.py:/Jaco-Master/skills/sentence_tools.py:ro \
    --volume `pwd`/Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/dialog/${NLU_DIR}/:/Jaco-Master/skills/skills/SmartLightsAndSpeaker-Jaco/dialog/nlu/:ro \
    --volume `pwd`/Jaco-Master2/userdata/config/:/Jaco-Master/userdata/config/:ro \
    --volume `pwd`/Jaco-Master2/jacolib/:/jacolib/:ro \
    --volume `pwd`/Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/userdata/skill_topic_keys.json:/Jaco-Master/userdata/skill_topic_keys.json:ro \
    --volume `pwd`/Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/moduldata/collected_old/:/Jaco-Master/skills/collected/ \
    --volume `pwd`/Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/moduldata/generated_old/:/Jaco-Master/skills/generated/ \
    -it master_base_image_ubuntu /bin/bash -c '\
      pip3 install num2words && \
      python3 /Jaco-Master/skills/collect_data.py &&  \
      python3 /Jaco-Master/skills/map_intents_slots.py && \
      python3 /Jaco-Master/skills/generate_data.py'
    ```

  - Create vocab

    ```bash
    cd Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/moduldata/generated_old/stt/
    cp sentences.txt sentences_1words.txt
    sed -i $'s/ /\\\n/g' sentences_1words.txt
    sed -i '1i hello hello hello hello hello' sentences_1words.txt
    awk '!x[$0]++' sentences_1words.txt > sentences_1words_unique.txt
    cp sentences_1words_unique.txt  1words.txt
    sed -i $'s/ /\\\n/g' 1words.txt
    awk '!x[$0]++' 1words.txt > vocab.txt
    cd ../../../../
    mkdir SmartLightsAndSpeaker-Jaco/moduldata/kenlm_old/
    ```

  - Build 1gram or 5gram langmodel

    ```bash
    export NCOUNT=1
    export SENFILE=sentences_1words_unique.txt
    docker run --network host --rm \
      --volume `pwd`/SmartLightsAndSpeaker-Jaco/:/Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/ \
      --volume `pwd`/../slungt/:/slungt/ \
      --volume `pwd`/sttmodels/:/Benchmark-Jaco/sttmodels/ \
      -it smartlightsandspeaker_jaco \
      /kenlm/build/bin/lmplz --order 5 --temp_prefix /tmp/ --memory 95% \
        --discount_fallback --prune 0 0 1  \
        --text /Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/moduldata/generated_old/stt/${SENFILE} --arpa /Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/moduldata/kenlm_old/all${NCOUNT}.arpa
    ```

  - Download tools from _DeepSpeech_: https://github.com/mozilla/DeepSpeech/releases/download/v0.9.3/native_client.amd64.cpu.linux.tar.xz

  - Generate scorer for _dsctcdecoder_

    ```bash
    docker run --network host --rm \
      --volume `pwd`/SmartLightsAndSpeaker-Jaco/:/Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/ \
      --volume `pwd`/../slungt/:/slungt/ \
      --volume `pwd`/sttmodels/:/Benchmark-Jaco/sttmodels/ \
      -it smartlightsandspeaker_jaco \
      /kenlm/build/bin/build_binary -a 255 -q 8 -v trie \
        /Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/moduldata/kenlm_old/all${NCOUNT}.arpa \
        /Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/moduldata/kenlm_old/all${NCOUNT}.bin

    docker run --network host --rm \
      --volume `pwd`/SmartLightsAndSpeaker-Jaco/:/Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/ \
      --volume `pwd`/sttmodels/:/Benchmark-Jaco/sttmodels/ \
      -it smartlightsandspeaker_jaco \
      ./SmartLightsAndSpeaker-Jaco/native_client.amd64.cpu.linux/generate_scorer_package \
        --alphabet /Benchmark-Jaco/sttmodels/alphabet_en.txt \
        --lm /Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/moduldata/kenlm_old/all${NCOUNT}.bin \
        --vocab /Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/moduldata/generated_old/stt/vocab.txt \
        --package /Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/moduldata/kenlm_old/kenlm_${NCOUNT}gram.scorer \
        --default_alpha 1.0 \
        --default_beta 1.0
    ```

- Generate general task language model:

  - Download `3-gram.arpa.gz` and `librispeech-vocab.txt` from: https://www.openslr.org/11 and put them to `moduldata/ls_arpa/`

  - Convert vocab and langmodel to lower case

    ```bash
    cd Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/moduldata/ls_arpa/
    awk '{print tolower($0)}' < librispeech-vocab.txt > vocab.txt
    awk '{print tolower($0)}' < 3-gram.arpa > 3-gram-lc.arpa
    ```

  - Generate scorer for _dsctcdecoder_

    ```bash
    docker run --network host --rm \
      --volume `pwd`/SmartLightsAndSpeaker-Jaco/:/Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/ \
      --volume `pwd`/../slungt/:/slungt/ \
      --volume `pwd`/sttmodels/:/Benchmark-Jaco/sttmodels/ \
      -it smartlightsandspeaker_jaco \
      /kenlm/build/bin/build_binary -a 255 -q 8 -v trie \
        /Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/moduldata/ls_arpa/3-gram-lc.arpa \
        /Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/moduldata/ls_arpa/3-gram-lc.bin

    docker run --network host --rm \
      --volume `pwd`/SmartLightsAndSpeaker-Jaco/:/Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/ \
      --volume `pwd`/sttmodels/:/Benchmark-Jaco/sttmodels/ \
      -it smartlightsandspeaker_jaco \
      ./SmartLightsAndSpeaker-Jaco/native_client.amd64.cpu.linux/generate_scorer_package \
        --alphabet /Benchmark-Jaco/sttmodels/alphabet_en.txt \
        --lm //Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/moduldata/ls_arpa/3-gram-lc.bin \
        --vocab /Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/moduldata/ls_arpa/vocab.txt \
        --package /Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/moduldata/ls_arpa/kenlm_3gram.scorer \
        --default_alpha 1.0 \
        --default_beta 1.0
    ```

- Enable the other decoders in the benchmark script and use the _character-based_ model

- Run benchmark: \
  (use default container)

  ```bash
  python3 /Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/run_transcriptions.py \
    --wav_files_txt /Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/dialog/nlu_slc_1/test_wavs_en.txt --reduced_dataset 10
  ```

<br>

### Run on RaspberryPi

- Build container:

  ```bash
  docker build -t smartlightsandspeaker_jaco - < SmartLightsAndSpeaker-Jaco/Containerfile_arm64
  ```

- Rename _Jaco_ base image to call it with the existing commands

  ```bash
  docker tag master_base_image_arm64 master_base_image_amd64
  ```

- Run benchmarks as described above

Accuracy 5-fold SmartLights-Close dataset: 0.8819 - gWER: 0.1061 - tWER: 0.0666 \
Benchmark Runtime: 00:37:19
