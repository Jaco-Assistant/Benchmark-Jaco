import argparse
import json
import os
import re
import sys
import time

import num2words
import numpy as np
import soundfile as sf
import tensorflow as tf
import tqdm
import whisper
from ds_ctcdecoder import Alphabet, Scorer, ctc_beam_search_decoder
from pyctcdecode import build_ctcdecoder

import convert_intents
import utils

sys.path.insert(1, "/spoken-language-understanding-research-datasets/")
from dataset_handler import (  # noqa: E402 pylint: disable=wrong-import-position
    CrossValDataset,
    TrainTestDataset,
)

sys.path.insert(1, "/slungt/swig/")
import slungt  # noqa: E402 pylint: disable=wrong-import-position

sys.path.insert(1, "/slungt/tests/")
import test_interface  # noqa: E402 pylint: disable=wrong-import-position

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
language = "en"
datasetname = "smartlights"
beam_size = 128

use_dsscorer = False
use_ctcdecode = False
use_slungt_plain = False
use_whisper = False

if datasetname == "smartlights":
    data_dir = file_path + "../datasets/snips_slu/smart-lights-en-close-field/"
else:
    raise ValueError

synonym_replacers = {
    "pink": "red",
    "lounge": "living room",
    "bed room": "bedroom",
    "toilet": "toilets",
    "loo": "toilets",
    "facilities": "toilets",
    "kids room": "kids bedroom",
    "cella": "cellar",
    "entire house": "house",
    "entire flat": "flat",
    "child's bedroom": "child bedroom",
    "children bedroom": "child bedroom",
    "children's bedroom": "child bedroom",
}

extra_label_replacers = {
    "toilets room": "toilets",
    "toilet room": "toilets",
    "facilities room": "toilets",
    "cella room": "cellar",
    "cellar room": "cellar",
    "children room": "child bedroom",
    "flat room": "flat",
    "house room": "room",
    "parking room": "parking",
    "cublicle": "cubicle",
    "entire appartment": "apartment",
    "appartment": "apartment",
    "fifty in": "fifty",
}

stt_model_dir = file_path + "../sttmodels/{}-conformerctc-large-cb4/pb/"
stt_model_dir = stt_model_dir.format(language)
tf_model: tf.keras.Model

alphabet_path = stt_model_dir + "../alphabet.json"
with open(alphabet_path, "r", encoding="utf-8") as afile:
    alphabet = json.load(afile)

datapath = file_path + "moduldata/sludata/"
vocabs, intents, lms = test_interface.load_data(datapath)

slu_decoder = slungt.Decoder(
    list(alphabet),
    beam_size,
    vocabs,
    intents,
    lms,
    lm_alpha=1.0,
    token_min_logprob=-10.0,
)

if use_ctcdecode:
    ctcdecoder1 = build_ctcdecoder(
        list(alphabet),
        kenlm_model_path=file_path + "moduldata/kenlm_old/all1.arpa",
        alpha=1.0,
        beta=1.0,
    )
    ctcdecoder5 = build_ctcdecoder(
        list(alphabet),
        kenlm_model_path=file_path + "moduldata/kenlm_old/all5.arpa",
        alpha=1.0,
        beta=1.0,
    )
    ctcdecoder3 = build_ctcdecoder(
        list(alphabet),
        kenlm_model_path=file_path + "moduldata/ls_arpa/3-gram-lc.arpa",
        alpha=1.0,
        beta=1.0,
    )

if use_dsscorer:
    ds_ab_path = file_path + "../sttmodels/alphabet_en.txt"
    ds_alphabet = Alphabet(ds_ab_path)
    ds_scorer1 = Scorer(
        alpha=1.0,
        beta=1.0,
        scorer_path=file_path + "moduldata/kenlm_old/kenlm_1gram.scorer",
        alphabet=ds_alphabet,
    )
    ds_scorer5 = Scorer(
        alpha=1.0,
        beta=1.0,
        scorer_path=file_path + "moduldata/kenlm_old/kenlm_5gram.scorer",
        alphabet=ds_alphabet,
    )
    ds_scorer3 = Scorer(
        alpha=1.0,
        beta=1.0,
        scorer_path=file_path + "moduldata/ls_arpa/kenlm_3gram.scorer",
        alphabet=ds_alphabet,
    )

if use_slungt_plain:
    vocab_path = file_path + "moduldata/generated_old/stt/vocab.txt"
    with open(vocab_path, "r", encoding="utf-8") as afile:
        vocab = afile.readlines()
    vocab = [v.strip() for v in vocab]
    vocab = [v for v in vocab if v != ""]
    slungt_plain1 = slungt.Decoder(
        list(alphabet),
        beam_size,
        [vocab],
        ["text"],
        [file_path + "moduldata/kenlm_old/all1.arpa"],
        lm_alpha=1.0,
        lm_beta=1.0,
    )
    slungt_plain5 = slungt.Decoder(
        list(alphabet),
        beam_size,
        [vocab],
        ["text"],
        [file_path + "moduldata/kenlm_old/all5.arpa"],
        lm_alpha=1.0,
        lm_beta=1.0,
    )

    vocab_path = file_path + "moduldata/ls_arpa/vocab.txt"
    with open(vocab_path, "r", encoding="utf-8") as afile:
        vocab = afile.readlines()
    vocab = [v.strip() for v in vocab]
    vocab = [v for v in vocab if v != ""]
    slungt_general3 = slungt.Decoder(
        list(alphabet),
        beam_size,
        [vocab],
        ["text"],
        [file_path + "moduldata/ls_arpa/3-gram-lc.arpa"],
        lm_alpha=1.0,
        lm_beta=1.0,
    )

if use_whisper:
    whisper_model = whisper.load_model("small")


# ==================================================================================================


def get_duration(filename):
    """Get duration of the wav file"""
    length = sf.info(filename).duration
    return length


# ==================================================================================================


def load_audio(wav_path):
    audio, _ = sf.read(wav_path, dtype="int16")
    audio = audio / (np.iinfo(np.int16).max + 1)
    audio = np.expand_dims(audio, axis=0)
    audio = audio.astype(np.float32)
    return audio


# ==================================================================================================


def get_intent_from_audio(audio_path):
    """Get intent from audio file"""

    intent = {}
    audio = load_audio(audio_path)
    intent["audio_dur"] = get_duration(audio_path)

    stime = time.time()
    prediction = tf_model.predict(audio)[0]
    probs = prediction.tolist()
    tf_dur = time.time() - stime
    intent["tf_dur"] = tf_dur

    stime = time.time()
    results = slu_decoder.decode(probs)
    result = results[0]
    slungt_intent = slu_decoder.extract_text2intent(result)
    intent.update(json.loads(str(slungt_intent)))
    slu_dur = time.time() - stime
    intent["slu_dur"] = slu_dur

    intent["result"] = str(result)
    intent["text_slu"] = intent["text"]
    intent.pop("text")

    stime = time.time()
    intent["text_greedy"] = slu_decoder.decode_greedy_text(probs)
    gd_dur = time.time() - stime
    intent["greedy_dur"] = gd_dur

    if use_ctcdecode:
        probsn = np.array(probs)
        probsn = np.log(np.clip(probsn, 1e-15, 1))
        stime = time.time()
        intent["text_pyctc1"] = ctcdecoder1.decode(probsn, beam_width=beam_size)
        intent["pyctc1_dur"] = time.time() - stime
        stime = time.time()
        intent["text_pyctc5"] = ctcdecoder5.decode(probsn, beam_width=beam_size)
        intent["pyctc5_dur"] = time.time() - stime
        stime = time.time()
        intent["text_pyctc3"] = ctcdecoder3.decode(probsn, beam_width=beam_size)
        intent["pyctc3_dur"] = time.time() - stime

    if use_dsscorer:
        probsl = np.array(probs).tolist()
        stime = time.time()
        ldecoded = ctc_beam_search_decoder(
            probsl,
            alphabet=ds_alphabet,
            beam_size=beam_size,
            scorer=ds_scorer1,
            hot_words={},
            num_results=1,
        )
        intent["text_dsctc1"] = ldecoded[0][1]
        intent["dsctc1_dur"] = time.time() - stime
        stime = time.time()
        ldecoded = ctc_beam_search_decoder(
            probsl,
            alphabet=ds_alphabet,
            beam_size=beam_size,
            scorer=ds_scorer5,
            hot_words={},
            num_results=1,
        )
        intent["text_dsctc5"] = ldecoded[0][1]
        intent["dsctc5_dur"] = time.time() - stime
        stime = time.time()
        ldecoded = ctc_beam_search_decoder(
            probsl,
            alphabet=ds_alphabet,
            beam_size=beam_size,
            scorer=ds_scorer3,
            hot_words={},
            num_results=1,
        )
        intent["text_dsctc3"] = ldecoded[0][1]
        intent["dsctc3_dur"] = time.time() - stime

    if use_slungt_plain:
        stime = time.time()
        result = slungt_plain1.decode(probs)[0]
        slungt_intent = json.loads(str(slungt_plain1.extract_text2intent(result)))
        intent["text_sp1"] = slungt_intent["text"]
        intent["sp1_dur"] = time.time() - stime
        stime = time.time()
        result = slungt_plain5.decode(probs)[0]
        slungt_intent = json.loads(str(slungt_plain5.extract_text2intent(result)))
        intent["text_sp5"] = slungt_intent["text"]
        intent["sp5_dur"] = time.time() - stime
        stime = time.time()
        result = slungt_general3.decode(probs)[0]
        slungt_intent = json.loads(str(slungt_general3.extract_text2intent(result)))
        intent["text_sg3"] = slungt_intent["text"]
        intent["sg3_dur"] = time.time() - stime

    if use_whisper:
        stime = time.time()
        text = whisper_model.transcribe(audio_path, fp16=False)["text"]
        text = text.replace("%", " percent ")
        text = text.replace("°", " degrees ")
        text = re.sub(r"\d+", lambda x: num2words.num2words(int(x.group(0))), text)
        text = text.replace("-", " ")
        text = re.sub(r"\s+", " ", text).strip()
        intent["text_wispr"] = text
        wispr_dur = time.time() - stime
        intent["wispr_dur"] = wispr_dur

    return intent


# ==================================================================================================


def calc_wer(text, target):
    """Calculate WER between two texts"""

    text = text.lower()
    target = target.lower()
    text = re.sub(r"[^a-z ]", " ", text).split()
    target = re.sub(r"[^a-z ]", " ", target).split()
    wer = utils.levenshtein(text, target) / len(target)
    return wer


# ==================================================================================================


def run_benchmark(dataset, wav_files):
    print("\nRunning benchmark ...")
    start_time = time.time()

    results = []
    for filename in tqdm.tqdm(wav_files):
        intent = get_intent_from_audio(filename)
        wav_name = os.path.basename(filename)
        label = dataset.get_labels_from_wav(wav_name)

        label_text = label["text"].lower()
        label_text = convert_intents.convert_sample(  # pylint: disable=no-member
            label_text
        )
        for k in list(intent.keys()):
            if "text_" in k:
                intent["wer_" + k.replace("text_", "")] = calc_wer(
                    intent[k], label_text
                )

        intent["filename"] = filename
        results.append(intent)

    conv_dur = time.time() - start_time
    msg = " The benchmark took {}"
    print(msg.format(utils.seconds_to_hours(conv_dur)))

    wers = {}
    for k in results[0]:
        if "wer_" in k:
            wers[k] = sum((r[k] for r in results)) / len(results)
    print(" Average WERs: {}".format(wers))

    durs = {}
    for k in results[0]:
        if "_dur" in k:
            durs[k] = sum((r[k] for r in results)) / len(results)
    print(" Average durations: {}".format(durs))


# ==================================================================================================


def main():
    global tf_model

    parser = argparse.ArgumentParser(description="Run benchmark with Jaco")
    parser.add_argument(
        "--wav_files_txt",
        required=True,
        help="Path to text file with list of wav files to use",
    )
    parser.add_argument(
        "--reduced_dataset",
        default=0,
        type=int,
        help="Test with fewer files",
    )
    args = parser.parse_args()

    reduced_dataset = args.reduced_dataset

    # Load stt model and run some warmup predictions
    stime = time.time()
    tf_model = tf.keras.models.load_model(stt_model_dir)
    tf_model.predict(np.random.rand(1, 16000).astype(np.float32))
    tf_model.predict(np.random.rand(1, 48000).astype(np.float32))
    tf_model.predict(np.random.rand(1, 32000).astype(np.float32))
    tf_model.summary()
    print("Time to load stt model:", time.time() - stime)

    # Load dataset
    if "slc" in args.wav_files_txt:
        dataset = CrossValDataset.from_dir(data_dir)
    else:
        dataset = TrainTestDataset.from_dir(data_dir)
    with open(args.wav_files_txt, "r", encoding="utf-8") as file:
        wav_files = file.readlines()
        wav_files = [w.strip() for w in wav_files if w.strip() != ""]
    wav_files = sorted(wav_files)

    for w in list(wav_files):
        wav_name = os.path.basename(w)
        label = dataset.get_labels_from_wav(wav_name)
        if label is None:
            print("WARNING: Dropping {} because it has no labels".format(wav_name))
            wav_files.remove(w)

    if 0 < reduced_dataset <= len(wav_files):
        wav_files = wav_files[0:reduced_dataset]

    # Get duration of sample files, extra because this needs some time too
    audio_dur = 0
    for file_name in tqdm.tqdm(wav_files):
        audio_dur += get_duration(os.path.join(file_name))
    msg = "The benchmark contains {} files with {} hours of audio"
    print(msg.format(len(wav_files), utils.seconds_to_hours(audio_dur)))

    if use_whisper:
        # Warmup whisper
        whisper_model.transcribe(wav_files[-1], fp16=False)
        whisper_model.transcribe(wav_files[-2], fp16=False)
        whisper_model.transcribe(wav_files[-3], fp16=False)

    # Set correct language for char replacements
    convert_intents.language = language
    convert_intents.load_char_replacers()  # pylint: disable=no-member

    run_benchmark(dataset, wav_files)


# ==================================================================================================

if __name__ == "__main__":
    main()
