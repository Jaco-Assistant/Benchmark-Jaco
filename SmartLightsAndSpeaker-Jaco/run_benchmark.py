import argparse
import json
import math
import multiprocessing as mp
import os
import re
import sys
import time

import numpy as np
import soundfile as sf
import tqdm

import convert_intents
import utils

tflite_enabled = False
tf_enabled = False
try:
    import tensorflow as tf

    tf_enabled = True
    tf_model: tf.keras.Model

except ModuleNotFoundError:
    import tflite_runtime.interpreter as tflite

    tflite_enabled = True
    tfl_model: tflite.Interpreter

sys.path.insert(1, "/spoken-language-understanding-research-datasets/")
from dataset_handler import (  # noqa: E402 pylint: disable=wrong-import-position
    CrossValDataset,
    TrainTestDataset,
)

sys.path.insert(1, "/slungt/swig/")
import slungt  # noqa: E402 pylint: disable=wrong-import-position

sys.path.insert(1, "/slungt/tests/")
import test_interface  # noqa: E402 pylint: disable=wrong-import-position

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
language = "en"
datasetname = "smartlights"
# datasetname = "smartspeaker"
use_stt = True
beam_size = 128
# beam_size = 64

if datasetname == "smartlights":
    data_dir = file_path + "../datasets/snips_slu/smart-lights-en-close-field/"
elif datasetname == "smartspeaker":
    data_dir = file_path + "../datasets/snips_slu/smart-speaker-{}-close-field/"
    data_dir = data_dir.format(language)
else:
    raise ValueError

synonym_replacers = {
    "pink": "red",
    "lounge": "living room",
    "bed room": "bedroom",
    "toilet": "toilets",
    "loo": "toilets",
    "facilities": "toilets",
    "kids room": "kids bedroom",
    "cella": "cellar",
    "entire house": "house",
    "entire flat": "flat",
    "child's bedroom": "child bedroom",
    "children bedroom": "child bedroom",
    "children's bedroom": "child bedroom",
}

extra_label_replacers = {
    "toilets room": "toilets",
    "toilet room": "toilets",
    "facilities room": "toilets",
    "cella room": "cellar",
    "cellar room": "cellar",
    "children room": "child bedroom",
    "flat room": "flat",
    "house room": "room",
    "parking room": "parking",
    "cublicle": "cubicle",
    "entire appartment": "apartment",
    "appartment": "apartment",
    "fifty in": "fifty",
}

stt_model_dir = file_path + "../sttmodels/{}-conformerctc-large-new/"
# stt_model_dir = file_path + "../sttmodels/{}-conformerctc-large-cb4/"
stt_model_dir = stt_model_dir.format(language)
if tf_enabled:
    stt_model_path = stt_model_dir + "pb/"
else:
    stt_model_path = stt_model_dir + "model_float16.tflite"

alphabet_path = stt_model_dir + "alphabet.json"
with open(alphabet_path, "r", encoding="utf-8") as afile:
    alphabet = json.load(afile)

datapath = file_path + "moduldata/sludata/"
vocabs, intents, lms = test_interface.load_data(datapath)

slu_decoder = slungt.Decoder(
    list(alphabet),
    beam_size,
    vocabs,
    intents,
    lms,
    lm_alpha=1.0,
    # lm_alpha=1.5,
    token_min_logprob=-10.0,
)


# ==================================================================================================


def get_duration(filename):
    """Get duration of the wav file"""
    length = sf.info(filename).duration
    return length


# ==================================================================================================


def load_audio(wav_path):
    audio, _ = sf.read(wav_path, dtype="int16")
    audio = audio / (np.iinfo(np.int16).max + 1)
    audio = np.expand_dims(audio, axis=0)
    audio = audio.astype(np.float32)
    return audio


# ==================================================================================================


def predict_signal_tf(audio):
    global tf_model

    output_data = tf_model.predict(audio, steps=1)
    output_data = output_data[0]
    return output_data


def predict_signal_tflite(audio):
    global tfl_model

    interpreter = tfl_model  # pylint: disable=used-before-assignment
    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()

    # Enable dynamic shape inputs
    interpreter.resize_tensor_input(input_details[0]["index"], audio.shape)
    interpreter.allocate_tensors()

    interpreter.set_tensor(input_details[0]["index"], audio)
    interpreter.invoke()

    output_data = interpreter.get_tensor(output_details[0]["index"])
    output_data = output_data[0]
    return output_data


# ==================================================================================================


def get_intent_from_audio(audio_path):
    """Get intent from audio file"""

    intent = {}
    audio = load_audio(audio_path)
    intent["audio_dur"] = get_duration(audio_path)

    stime = time.time()
    if tf_enabled:
        prediction = predict_signal_tf(audio)
    else:
        prediction = predict_signal_tflite(audio)
    probs = prediction.tolist()
    tf_dur = time.time() - stime
    intent["tf_dur"] = tf_dur

    stime = time.time()
    results = slu_decoder.decode(probs)
    result = results[0]
    slungt_intent = slu_decoder.extract_text2intent(result)
    intent.update(json.loads(str(slungt_intent)))
    slu_dur = time.time() - stime
    intent["slu_dur"] = slu_dur

    intent["result"] = str(result)
    intent["text_slu"] = intent["text"]
    intent.pop("text")

    stime = time.time()
    intent["text_greedy"] = slu_decoder.decode_greedy_text(probs)
    gd_dur = time.time() - stime
    intent["greedy_dur"] = gd_dur

    return intent


# ==================================================================================================


def seq_to_tokens(sequence, base_alphabet):
    full_alphabet = base_alphabet + ["-"]

    # Define base score and main value
    base_score = 0.01
    main_val = 5.0

    # Create CTC tokens
    ctc_tokens = [
        [base_score for _ in range(len(full_alphabet))] for _ in range(len(sequence))
    ]

    for i in range(len(sequence)):
        for j in range(len(full_alphabet)):
            if sequence[i] == full_alphabet[j]:
                ctc_tokens[i][j] = main_val
                break

    # Softmax calculation
    for i in range(len(ctc_tokens)):
        max_val = max(ctc_tokens[i])

        sum_exp = 0.0
        for j in range(len(ctc_tokens[i])):
            ctc_tokens[i][j] = math.exp(ctc_tokens[i][j] - max_val)
            sum_exp += ctc_tokens[i][j]

        for j in range(len(ctc_tokens[i])):
            ctc_tokens[i][j] /= sum_exp

    return ctc_tokens


def get_intent_from_text(dataset, audio_path):
    if max((len(t) for t in alphabet)) > 1:
        raise ValueError(
            "Textual inputs are only supported with character-based alphabets!"
        )

    wav_name = os.path.basename(audio_path)
    text = dataset.get_transcript(wav_name)
    text = convert_intents.convert_sample(text)  # pylint: disable=no-member
    text = text.replace("-", " ")
    text = "".join([t if t in alphabet else "" for t in text])

    # Convert text to single characters
    # Insert a blank at every repeated character
    seq = []
    last_char = ""
    for t in list(text):
        if t == last_char:
            seq.append("-")
        seq.append(t)
        last_char = t
    probs = seq_to_tokens(seq, alphabet)

    results = slu_decoder.decode(probs)
    result = results[0]
    slungt_intent = slu_decoder.extract_text2intent(result)
    intent = json.loads(str(slungt_intent))

    intent["result"] = str(result)
    intent["text_slu"] = intent["text"]
    intent.pop("text")
    intent["text_greedy"] = slu_decoder.decode_greedy_text(probs)

    return intent


# ==================================================================================================


def convert_intent(intent):
    """Convert to label intent format"""

    intent["intent"] = intent["name"].replace("smartlightsandspeaker_jaco-", "")
    intent["slots"] = {}

    if "entities" in intent:
        for e in intent["entities"]:
            entity = e["name"].replace("smartlightsandspeaker_jaco-", "")
            if entity not in intent["slots"]:
                intent["slots"][entity] = e["value"]

    return intent


# ==================================================================================================


def compare_intents(intent, label):
    if intent is None or "intent" not in intent:
        return False, "intent missing"

    if label["intent"].lower() != intent["intent"]:
        return False, "intent wrong"

    for slot in label["slots"]:
        entity_name = slot["entity"].lower()
        if entity_name.startswith("snips/"):
            entity_name = entity_name.replace("snips/", "")

        if entity_name not in intent["slots"]:
            return False, "slot missing: {}".format(slot)

        label_value = slot["text"].strip()
        detection_value = intent["slots"][entity_name].strip()

        # Remove dashes (some numbers/artists in the labels have dashes, but most not)
        label_value = label_value.replace("-", " ")
        # Remove special sentence signs
        if datasetname == "smartspeaker":
            label_value = convert_intents.convert_sample(  # pylint: disable=no-member
                label_value
            )
        else:
            label_value = label_value.replace("?", "")
        label_value = label_value.replace(".", "")
        detection_value = detection_value.replace(".", "")

        if datasetname == "smartlights":
            # Replace synonyms, this is not handled correctly in the labels dataset
            for sr in synonym_replacers:
                if label_value == sr:
                    label_value = synonym_replacers[sr]
            # Replace wrong labels that are not handled correctly in the dataset
            for sr in extra_label_replacers:
                if label_value == sr:
                    label_value = extra_label_replacers[sr]
                if detection_value == sr:
                    detection_value = extra_label_replacers[sr]

        if detection_value != label_value:
            return False, "value wrong: {} - {}".format(detection_value, label_value)

    return True, ""


# ==================================================================================================


def calc_wer(text, target):
    """Calculate WER between two texts"""

    text = text.lower()
    target = target.lower()
    text = re.sub(r"[^a-z ]", " ", text).split()
    target = re.sub(r"[^a-z ]", " ", target).split()
    wer = utils.levenshtein(text, target) / len(target)
    return wer


# ==================================================================================================


def run_benchmark(dataset, wav_files):
    print("\nRunning benchmark ...")
    start_time = time.time()

    results = []
    for filename in tqdm.tqdm(wav_files):
        if use_stt:
            intent = get_intent_from_audio(filename)
        else:
            intent = get_intent_from_text(dataset, filename)

        convint = convert_intent(intent)
        wav_name = os.path.basename(filename)
        label = dataset.get_labels_from_wav(wav_name)
        same, error_reason = compare_intents(convint, label)
        convint["same"] = same
        convint["error_reason"] = error_reason

        label_text = label["text"].lower()
        label_text = convert_intents.convert_sample(  # pylint: disable=no-member
            label_text
        )
        for k in list(convint.keys()):
            if "text_" in k:
                convint["wer_" + k.replace("text_", "")] = calc_wer(
                    convint[k], label_text
                )

        convint["filename"] = filename
        results.append(convint)

        if not same:
            # Print wrong detections for debugging
            print("\n")
            print("ERROR", error_reason)
            print("Label:", label)
            print("Detected:", convint)

    conv_dur = time.time() - start_time
    msg = " The benchmark took {}"
    print(msg.format(utils.seconds_to_hours(conv_dur)))

    # Count wrong and correct instead of using list length for debugging purposes
    # (allows to end the loop midways)
    num_correct = sum((1 for r in results if r["same"]))
    acc = num_correct / len(results)
    msg = " Correctly converted {}/{}. The accuracy is {:.4f}."
    print(msg.format(num_correct, len(results), acc))

    wers = {}
    for k in results[0]:
        if "wer_" in k:
            wers[k] = sum((r[k] for r in results)) / len(results)
    print(" Average WERs: {}".format(wers))

    durs = {}
    for k in results[0]:
        if "_dur" in k:
            durs[k] = sum((r[k] for r in results)) / len(results)
    print(" Average durations: {}".format(durs))


# ==================================================================================================


def main():
    global tf_model, tfl_model

    parser = argparse.ArgumentParser(description="Run benchmark with Jaco")
    parser.add_argument(
        "--wav_files_txt",
        required=True,
        help="Path to text file with list of wav files to use",
    )
    parser.add_argument(
        "--reduced_dataset",
        default=0,
        type=int,
        help="Test with fewer files",
    )
    args = parser.parse_args()

    reduced_dataset = args.reduced_dataset

    # Load stt model and run some warmup predictions
    if use_stt:
        stime = time.time()
        if tf_enabled:
            tf_model = tf.keras.models.load_model(stt_model_path)
            tf_model.summary()
            pred_func = predict_signal_tf
        else:
            tfl_model = tflite.Interpreter(
                model_path=stt_model_path, num_threads=mp.cpu_count()
            )
            pred_func = predict_signal_tflite
        pred_func(np.random.rand(1, 16000).astype(np.float32))
        pred_func(np.random.rand(1, 48000).astype(np.float32))
        pred_func(np.random.rand(1, 32000).astype(np.float32))
        print("Time to load stt model:", time.time() - stime)

    # Load dataset
    if "slc" in args.wav_files_txt:
        dataset = CrossValDataset.from_dir(data_dir)
    else:
        dataset = TrainTestDataset.from_dir(data_dir)
    with open(args.wav_files_txt, "r", encoding="utf-8") as file:
        wav_files = file.readlines()
        wav_files = [w.strip() for w in wav_files if w.strip() != ""]
    wav_files = sorted(wav_files)

    for w in list(wav_files):
        wav_name = os.path.basename(w)
        label = dataset.get_labels_from_wav(wav_name)
        if label is None:
            print("WARNING: Dropping {} because it has no labels".format(wav_name))
            wav_files.remove(w)

    if 0 < reduced_dataset <= len(wav_files):
        wav_files = wav_files[0:reduced_dataset]

    # Get duration of sample files, extra because this needs some time too
    audio_dur = 0
    for file_name in tqdm.tqdm(wav_files):
        audio_dur += get_duration(os.path.join(file_name))
    msg = "The benchmark contains {} files with {} hours of audio"
    print(msg.format(len(wav_files), utils.seconds_to_hours(audio_dur)))

    # Set correct language for char replacements
    convert_intents.language = language
    convert_intents.load_char_replacers()  # pylint: disable=no-member

    run_benchmark(dataset, wav_files)


# ==================================================================================================

if __name__ == "__main__":
    main()
