import copy
import json
import os
import random
import re
import shutil
import sys

import num2words

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
sys.path.insert(1, "/spoken-language-understanding-research-datasets/")
from dataset_handler import (  # noqa: E402 pylint: disable=wrong-import-position
    CrossValDataset,
    TrainTestDataset,
)

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
language = "en"
datasetname = "smartlights"
# datasetname = "smartspeaker"
use_full_french_alphabet = True

if datasetname == "smartlights":
    data_dir = file_path + "../datasets/snips_slu/smart-lights-en-close-field/"
    nlu_save_path = file_path + "dialog/nlu_slc_{}/en/"
elif datasetname == "smartspeaker":
    data_dir = file_path + "../datasets/snips_slu/smart-speaker-{}-close-field/"
    data_dir = data_dir.format(language)
    nlu_save_path = file_path + "dialog/nlu_ssc/{}/".format(language)
else:
    raise ValueError

snips_builtin = r"\[([^\[]+)\]\((snips\/(musicalbum|musicartist|musictrack).txt)\)"
number_pattern = (
    r"(?:[\\s][+-])?[0-9]+(?:(?:[.][0-9]{3}(?:(?=[^0-9])))+)?(?:[,][0-9]+)?"
)
number_pattern = re.compile(number_pattern)

# Correct spelling errors
extra_replacers = {
    "cublicle": "cubicle",
    "appartment": "apartment",
}

# Fixing room labeling errors
extra_synonyms = {
    "parking": {"parking room"},
    "toilets": {"toilets room", "toilet room", "facilities room"},
    "cellar": {"cella room", "cellar room"},
    "flat": {"flat room"},
    "room": {"house room"},
    "child bedroom": {"children room"},
}

char_replacers: dict = {}
all_char_replacers = {
    "en": {
        "àáâãåāăąǟǡǻȁȃȧ": "a",
        "æǣǽä": "ae",
        "çćĉċč": "c",
        "èéêëēĕėęěȅȇȩε": "e",
        "ìíîïĩīĭįıȉȋ": "i",
        "ñńņňŉŋǹ": "n",
        "òóôõøōŏőǫǭǿȍȏðο": "o",
        "ứùúûũūŭůűųȕȗ": "u",
        "ýÿŷ": "y",
        "ďđ": "d",
        "ĝğġģǥǧǵ̇g": "g",
        "ĥħȟ": "h",
        "ĵǰ": "j",
        "ķĸǩǩκ": "k",
        "ĺļľŀł": "l",
        "œö": "oe",
        "ŕŗřȑȓ": "r",
        "śŝşšș": "s",
        "ţťŧț": "t",
        "ŵ": "w",
        "źżžȥ": "z",
        "м": "m",
        "ü": "ue",
        '®/:,"\\*\\!': " ",
        "’": "'",
        "$": "s",
    },
    "fr": {
        "àáâãåāăąǟǡǻȁȃȧ": "a",
        "æǣǽä": "ae",
        "çćĉċč": "c",
        "èéêëēĕėęěȅȇȩε": "e",
        "ìíîïĩīĭįıȉȋ": "i",
        "ñńņňŉŋǹ": "n",
        "òóôõøōŏőǫǭǿȍȏðο": "o",
        "ứùúûũūŭůűųȕȗ": "u",
        "ýÿŷ": "y",
        "ďđ": "d",
        "ĝğġģǥǧǵ̇g": "g",
        "ĥħȟ": "h",
        "ĵǰ": "j",
        "ķĸǩǩκ": "k",
        "ĺļľŀł": "l",
        "œö": "oe",
        "ŕŗřȑȓ": "r",
        "śŝşšș": "s",
        "ţťŧț": "t",
        "ŵ": "w",
        "źżžȥ": "z",
        "м": "m",
        "ü": "ue",
        '®/:,"\\*\\!': " ",
        "’": "'",
        "$": "s",
    },
    "fr-full": {
        "áãåāăąǟǡǻȁȃȧ": "a",
        "ǣǽä": "æ",
        "ćĉċč": "c",
        "ēĕėęěȅȇȩε": "e",
        "ìíĩīĭįıȉȋ": "i",
        "ñńņňŉŋǹ": "n",
        "òóõøōŏőǫǭǿȍȏðο": "o",
        "ứúũūŭůűųȕȗ": "u",
        "ýŷ": "y",
        "ďđ": "d",
        "ĝğġģǥǧǵ̇g": "g",
        "ĥħȟ": "h",
        "ĵǰ": "j",
        "ķĸǩǩκ": "k",
        "ĺļľŀł": "l",
        "ö": "œ",
        "ŕŗřȑȓ": "r",
        "śŝşšș": "s",
        "ţťŧț": "t",
        "ŵ": "w",
        "źżžȥ": "z",
        "м": "m",
        '®/:,"\\*\\!': " ",
        "’": "'",
        "$": "s",
    },
}

special_replacers = {
    "en": {
        "&": "and",
        "°": "degrees",
        "º": "degrees",
        "α": "alpha",
        "β": "beta",
        "γ": "gamma",
        "δ": "delta",
        "@": "at",
        "+": "and",
        "%": "percent",
        " ' ": " ",
    },
    "fr": {
        "&": "et",
        "°": "grad",
        "º": "grad",
        "α": "alpha",
        "β": "beta",
        "γ": "gamma",
        "δ": "delta",
        "@": "at",
        "+": "et",
        "%": "pourcent",
        " ' ": " ",
    },
}


# ==================================================================================================


def switch_char_dict(input_dict):
    replacers = {}

    if use_full_french_alphabet and language == "fr" and "fr-full" in input_dict:
        items = input_dict["fr-full"].items()
    else:
        items = input_dict[language].items()

    for rep, replacement in items:
        # Switch keys and value
        for to_replace in rep:
            replacers[to_replace] = replacement
    return replacers


# ==================================================================================================


def load_char_replacers():
    global char_replacers
    char_replacers = switch_char_dict(all_char_replacers)


load_char_replacers()

# ==================================================================================================


def convert_entities(dataset):
    entities = {}

    for entity in dataset["entities"]:
        if entity.startswith("snips/"):
            print("WARNING: No entity data of builtin type:", entity)
            continue

        if entity in ["VolumeAbsolute1-100_EN", "VolumePercentage1-100_EN"]:
            # We want to use Jaco's default numbers instead
            continue

        entities[entity] = {"values": [], "synonyms": {}}

        for entry in dataset["entities"][entity]["data"]:
            if entry["value"] == "":
                continue

            value = entry["value"]
            entities[entity]["values"].append(value)

            if len(entry["synonyms"]) > 0:
                syns = [s.strip() for s in entry["synonyms"]]
                syns = [s for s in syns if not s == ""]
            else:
                syns = []
            if entry["value"] in extra_synonyms:
                syns = list(set(syns).union(extra_synonyms[entry["value"]]))
            if len(syns) > 0:
                entities[entity]["synonyms"][entry["value"].lower()] = syns

    return entities


# ==================================================================================================


def check_entity_completeness(dataset, entities):
    for intent in dataset["intents"]:
        for utt in dataset["intents"][intent]["utterances"]:
            for item in utt["data"]:
                if "entity" in item:
                    if item["entity"] == "snips/number":
                        continue

                    included = list(entities[item["entity"]]["values"])
                    for syns in entities[item["entity"]]["synonyms"].values():
                        included += syns
                    included = set(included)

                    value = item["text"]
                    value = value.replace(".", "")
                    if value not in included and value not in extra_replacers:
                        msg = "WARNING: '{}' not included in '{}'"
                        print(msg.format(value, item["entity"]))


# ==================================================================================================


def writeable_entities(conv_ents):
    lookup = {}
    text = ""

    for entity in conv_ents:
        lookup[entity] = ""
        lookup_text = "## lookup:{arg1}\n{arg2}.txt\n\n"
        text = text + lookup_text.format(arg1=entity, arg2=entity)

        for item in sorted(conv_ents[entity]["values"]):
            if item not in conv_ents[entity]["synonyms"]:
                lookup[entity] += item + "\n"
            else:
                syns = conv_ents[entity]["synonyms"][item]
                syns = set(syns).union({item})
                lookup[entity] += "({})->{}\n".format("|".join(syns), item)

    text = text + "## lookup:number\nnumber.txt\n\n"
    return text, lookup


# ==================================================================================================


def convert_intents_from_wavs(dataset, train_wavs):
    intents = {}
    for intent in dataset.training_dataset["intents"]:
        intents[intent] = []

    for file in train_wavs:
        label = dataset.get_labels_from_wav(os.path.basename(file))
        convint = "- " + label["text"]

        for slot in label["slots"]:
            if slot["entity"] == "snips/number":
                entity = "number"
            else:
                entity = slot["entity"]

            value = slot["text"].lower().strip()
            # Remove dashes (some numbers/artists have dashes, but most not)
            value = value.replace("-", " ")
            entity_label = " [{}]({}.txt) ".format(value, entity)
            convint = convint.replace(slot["text"], entity_label)

        convint = " ".join(convint.split())
        intents[label["intent"]].append(convint)

    return intents


# ==================================================================================================


def convert_intents_from_dataset(dataset):
    intents = {}
    for intent in dataset.training_dataset["intents"]:
        intents[intent] = []

        for ut in dataset.training_dataset["intents"][intent]["utterances"]:
            convint = "- "

            for part in ut["data"]:
                if "entity" not in part:
                    convint += part["text"].lower()
                else:
                    if part["entity"] in [
                        "VolumeAbsolute1-100_EN",
                        "VolumePercentage1-100_EN",
                        "snips/percentage",
                    ]:
                        entity = "number"
                    else:
                        entity = part["entity"]

                    value = part["text"].lower().strip()
                    # Remove dashes (some numbers/artists have dashes, but most not)
                    value = value.replace("-", " ")
                    convint += " [{}]({}.txt) ".format(value, entity)

            convint = " ".join(convint.split())
            intents[intent].append(convint)
    return intents


# ==================================================================================================


def writeable_intents(conv_ints):
    text = ""
    for intent in conv_ints:
        text = text + "## intent:{}\n".format(intent)
        text = text + "\n".join(conv_ints[intent]) + "\n\n"

    return text


# ==================================================================================================


def convert_sample(sample):
    sample = sample.lower()

    matches = number_pattern.findall(sample)
    for match in matches:
        num = match.replace(".", "")
        num = num.replace(",", ".")
        num_word = num2words.num2words(float(num), lang=language)
        sample = sample.replace(match, " {} ".format(num_word))

    for to_replace, replacement in char_replacers.items():
        sample = sample.replace(to_replace, replacement)

    for to_replace, replacement in special_replacers[language].items():
        sample = sample.replace(to_replace, " {} ".format(replacement))

    for to_replace, replacement in extra_replacers.items():
        sample = sample.replace(to_replace, replacement)

    sample = sample.replace("  ]", "]")
    sample = sample.replace("[  ", "[")
    sample = sample.replace(" ]", "]")
    sample = sample.replace("[ ", "[")

    # Remove all dots except those of the entity link
    sample = re.sub(r"\.(?!txt)", "", sample)

    # Remove all dashes except those in the entity names and at the beginning
    if sample.startswith("- "):
        sample = "- " + re.sub(r"-(?![^(]*\))", " ", sample[2:])
    else:
        sample = re.sub(r"-(?![^(]*\))", " ", sample)

    sample = sample.lower()
    sample = " ".join(sample.split())
    return sample


# ==================================================================================================


def clean_intents_and_entities(conv_ints, conv_ents):
    new_ints = {}
    new_ents = copy.deepcopy(conv_ents)

    for intent in conv_ints:
        new_ints[intent] = []

        for i, sample in enumerate(conv_ints[intent]):
            sample = sample.replace("snips/", "")
            sample = convert_sample(sample)

            if (
                datasetname == "smartspeaker"
                and len(re.findall(r"\[.*\[.*\[", sample)) > 0
            ):
                # Drop intents with 3 or more entities -> need to much computation power
                msg = "WARNING: Dropping intent because it has to much slots: {}"
                print(msg.format(sample))
                continue

            new_ints[intent].append(sample)

    for entity in conv_ents:
        for i, sample in enumerate(conv_ents[entity]["values"]):
            sample = convert_sample(sample)
            sample = sample.replace("(", "")
            sample = sample.replace(")", "")
            new_ents[entity]["values"][i] = sample

    for entity in conv_ents:
        for s in conv_ents[entity]["synonyms"]:
            for i, sample in enumerate(conv_ents[entity]["synonyms"][s]):
                sample = convert_sample(sample)
                new_ents[entity]["synonyms"][s][i] = sample

    return new_ints, new_ents


# ==================================================================================================


def collect_builtin_entities(conv_ints):
    """Find snips builtin types in nlu data"""

    snips_entities = {
        "musicalbum": {"values": [], "synonyms": {}},
        "musicartist": {"values": [], "synonyms": {}},
        "musictrack": {"values": [], "synonyms": {}},
    }
    for intent in conv_ints:
        for sample in conv_ints[intent]:
            sample = sample.lower()
            matches = re.findall(snips_builtin, sample)
            if len(matches) > 0:
                snips_entities[matches[0][2]]["values"].append(matches[0][0])

    # Load snips builtin types from artists file
    with open(data_dir + "../artist_list.json", "r", encoding="utf-8") as file:
        artist = json.load(file)
    for t in artist:
        snips_entities["musicartist"]["values"].extend(artist[t])

    # Remove duplicates
    for k in snips_entities:
        snips_entities[k]["values"] = list(set(snips_entities[k]["values"]))

    return snips_entities


# ==================================================================================================


def write_dataset(nlu_dir, dataset, conv_ents, test_wavs, train_wavs=None):
    if os.path.exists(nlu_dir):
        shutil.rmtree(nlu_dir, ignore_errors=True)
    os.makedirs(nlu_dir)

    if isinstance(dataset, CrossValDataset):
        train_ints = convert_intents_from_wavs(dataset, train_wavs)
    else:
        train_ints = convert_intents_from_dataset(dataset)
        for intent in train_ints:
            # Replace percentages in slot with percentage outside slots
            for i, sample in enumerate(train_ints[intent]):
                if language == "fr":
                    sample = sample.replace("%](number.txt)", "](number.txt) pourcent")
                    sample = sample.replace(
                        " pourcent](number.txt)", "](number.txt) pourcent"
                    )
                else:
                    sample = sample.replace("%](number.txt)", "](number.txt) percent")
                    sample = sample.replace(
                        " percent](number.txt)", "](number.txt) percent"
                    )
                train_ints[intent][i] = sample

        snips_entities = collect_builtin_entities(train_ints)
        conv_ents.update(snips_entities)

    # Replace special signs because speech-to-text doesn't understand them
    train_ints, conv_ents = clean_intents_and_entities(train_ints, conv_ents)

    text, lookups = writeable_entities(conv_ents)
    with open(nlu_dir + "nlu.md", "w+", encoding="utf-8") as file:
        text += writeable_intents(train_ints)
        text = text.lower()
        file.write(text)

    for lookup in lookups:
        with open(
            nlu_dir + "{}.txt".format(lookup.lower()), "w+", encoding="utf-8"
        ) as file:
            text = lookups[lookup].lower()
            file.write(text)

    path = file_path + "dialog/number_{}.txt".format(language)
    shutil.copyfile(path, nlu_dir + "number.txt")

    path = nlu_dir + "../test_wavs_{}.txt".format(language)
    with open(path, "w+", encoding="utf-8") as file:
        text = "\n".join(test_wavs) + "\n"
        file.write(text)


# ==================================================================================================


def create_smart_lights():
    dataset = CrossValDataset.from_dir(data_dir)
    print("")

    conv_ents = convert_entities(dataset.training_dataset)
    check_entity_completeness(dataset.training_dataset, conv_ents)

    files = dataset.audio_corpus
    wav_files = [files[e] for e in files]
    random.Random(1234).shuffle(wav_files)

    # Create splits for 5-fold cross-validation
    for i in range(5):
        nlu_dir = nlu_save_path.format(i + 1)

        start = int(len(wav_files) / 5 * i)
        stop = int(len(wav_files) / 5 * (i + 1))
        train_wavs = [c for i, c in enumerate(wav_files) if (i < start or i >= stop)]
        test_wavs = [c for i, c in enumerate(wav_files) if not (i < start or i >= stop)]

        write_dataset(nlu_dir, dataset, conv_ents, test_wavs, train_wavs=train_wavs)


# ==================================================================================================


def create_smart_speaker():
    dataset = TrainTestDataset.from_dir(data_dir)
    print("")

    conv_ents = convert_entities(dataset.training_dataset)
    files = dataset.audio_corpus
    wav_files = [files[e] for e in files]
    random.shuffle(wav_files)

    write_dataset(nlu_save_path, dataset, conv_ents, wav_files, train_wavs=None)


# ==================================================================================================


def main():
    if datasetname == "smartlights":
        create_smart_lights()
    else:
        create_smart_speaker()


# ==================================================================================================

if __name__ == "__main__":
    main()
