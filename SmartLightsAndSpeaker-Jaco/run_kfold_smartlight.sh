#! /bin/bash

for i in {1..5}; do
  echo ""
  echo "================================================================================"
  echo "Fold: ${i}"
  echo "================================================================================"
  echo ""

  export NLU_DIR=nlu_slc_${i}
  docker run --network host --rm \
    --volume "$(pwd)"/Jaco-Master/skills/collect_data.py:/Jaco-Master/skills/collect_data.py:ro \
    --volume "$(pwd)"/Jaco-Master/skills/generate_data.py:/Jaco-Master/skills/generate_data.py:ro \
    --volume "$(pwd)"/Jaco-Master/skills/map_intents_slots.py:/Jaco-Master/skills/map_intents_slots.py:ro \
    --volume "$(pwd)"/Jaco-Master/skills/sentence_tools.py:/Jaco-Master/skills/sentence_tools.py:ro \
    --volume "$(pwd)"/Jaco-Master/skills/text_tools.py:/Jaco-Master/skills/text_tools.py:ro \
    --volume "$(pwd)"/Jaco-Master/skills/basic_normalizer.py:/Jaco-Master/skills/basic_normalizer.py:ro \
    --volume "$(pwd)"/Jaco-Master/slu-parser/moduldata/langdicts.json:/Jaco-Master/slu-parser/moduldata/langdicts.json:ro \
    --volume "$(pwd)"/Jaco-Master/jacolib/:/jacolib/:ro \
    --volume "$(pwd)"/Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/dialog/${NLU_DIR}/:/Jaco-Master/skills/skills/SmartLightsAndSpeaker-Jaco/dialog/nlu/:ro \
    --volume "$(pwd)"/Jaco-Master/userdata/config/:/Jaco-Master/userdata/config/:ro \
    --volume "$(pwd)"/Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/userdata/skill_topic_keys.json:/Jaco-Master/userdata/skill_topic_keys.json:ro \
    --volume "$(pwd)"/Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/moduldata/collected/:/Jaco-Master/skills/collected/ \
    --volume "$(pwd)"/Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/moduldata/generated/:/Jaco-Master/skills/generated/ \
    master_base_image_amd64 /bin/bash -c \
      'python3 /Jaco-Master/skills/collect_data.py && python3 /Jaco-Master/skills/map_intents_slots.py && python3 /Jaco-Master/skills/generate_data.py'

  docker run --network host --rm \
    --volume "$(pwd)"/Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/:/Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/ \
    --volume "$(pwd)"/slungt/:/slungt/ \
    --volume "$(pwd)"/Benchmark-Jaco/sttmodels/:/Benchmark-Jaco/sttmodels/ \
    smartlightsandspeaker_jaco \
      python3 /slungt/preprocess/fullbuild_slu.py \
        --workdir /Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/moduldata/sludata/ \
        --nlufile_path /Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/moduldata/generated/nlu/nlu.json \
        --alphabet_path /Benchmark-Jaco/sttmodels/en-conformerctc-large-new/alphabet.json
  
  docker run --network host --rm \
    --volume "$(pwd)"/Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/:/Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/ \
    --volume "$(pwd)"/slungt/:/slungt/ \
    --volume "$(pwd)"/Benchmark-Jaco/datasets/:/Benchmark-Jaco/datasets/ \
    --volume "$(pwd)"/Benchmark-Jaco/sttmodels/:/Benchmark-Jaco/sttmodels/ \
    smartlightsandspeaker_jaco \
        /bin/bash -c "cd /slungt/swig/; make all; cd /; python3 /Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/run_benchmark.py \
        --wav_files_txt /Benchmark-Jaco/SmartLightsAndSpeaker-Jaco/dialog/${NLU_DIR}/test_wavs_en.txt"

done

echo ""
echo "FINISHED BENCHMARK"
